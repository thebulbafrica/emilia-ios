//
//  Paystack.swift
//  Emillia
//
//  Created by Anthony Odu on 24/03/2021.
//

import UIKit
import Paystack
import MHLoadingButton
import RxSwift

class Paystacks: UIViewController {

    let card : PSTCKCard = PSTCKCard()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chargeCardButton.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
  

    // MARK: Properties
      @IBOutlet weak var cardDetailsForm: PSTCKPaymentCardTextField!
      @IBOutlet weak var chargeCardButton: UIButton!
  

}
