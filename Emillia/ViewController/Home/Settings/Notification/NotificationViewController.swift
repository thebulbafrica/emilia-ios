//
//  NotificationViewController.swift
//  Emillia
//
//  Created by apple on 01/09/2021.
//

import UIKit
import RxSwift
import RxCocoa


class NotificationViewController: UIViewController {

    @IBOutlet weak var emptyViewStack: UIStackView!
    
    @IBOutlet weak var tableView: UITableView!
    
    let allNotification:PublishSubject<[Notifications]> = PublishSubject()
    
    let viewModel = NotificationRoute()
    let disposeBag = DisposeBag()
    let child = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        setupBinding()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getNotification(token: Utils.getToken(), unUsed: nil)
    }
    

    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

    
    private func configure(){
        viewModel.loading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            DispatchQueue.main.async {

                if loading{
                    this.showSpinnerView(child: this.child)
                }else{
                    this.removeSpinnerView(child: this.child)
                }
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.allNotification.observe(on: MainScheduler.instance).bind(to: allNotification).disposed(by: disposeBag)
        
        viewModel.errors.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
    }
    
    private func setupBinding(){
        tableView.register(UINib(nibName: "notification", bundle: nil), forCellReuseIdentifier: String(describing: SingleNotificationCell.self))
        
        allNotification.map{$0.isEmpty}.subscribe(onNext: {[weak self] value in
            
            if value{
                self?.emptyViewStack.isHidden = false
            }else{
                self?.emptyViewStack.isHidden = true
            }
        } ).disposed(by: disposeBag)
        allNotification.bind(to: tableView.rx.items(cellIdentifier: "notification",cellType: SingleNotificationCell.self)){
            (row,notification,cell) in
            cell.notification = notification
        }.disposed(by: disposeBag)
    }
}



class SingleNotificationCell:UITableViewCell{
    
    
    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var descriptonTitle: UILabel!
    @IBOutlet weak var dateTitle: UILabel!
    
    public var notification: Notifications! {
        didSet {
            //Utils.timeZoneForWallet(date: allData.transdate)
            nameTitle.text = notification.title
            descriptonTitle.text = notification.msg
            if let date = notification.createddate{
                dateTitle.text = Utils.justDate(date: date)
            }
           
            
        }
    }
    
    override class func awakeFromNib() {
        
    }
}

