//
//  SettingViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 15/03/2021.
//

import Foundation
import RxSwift

public class SettingViewModel{
    
 
    let dataMapper = SettingDataMapper()

 
    func updateProfile(token:String,userId:Int,data:UpdateProfile){
        ProfileRoute.updateProfile(token: token, userId: userId, data: data, serviceResponse: dataMapper)
    }

    
    func getUserProfile(token:String,userId:Int)-> Observable<UserProfileResponse>{
        return ProfileRoute.getUserProfile(token: token, userId: userId, serviceResponse: dataMapper)
    }
    
    func changePassword(token:String,userPassword:ChangePassword,userId:Int){
        PasswordRecoverRoute.changePassword(token: token, user: userPassword, id: userId, serviceResponse: dataMapper)
    }
    
    func getAllSetting(token:String)-> Observable<SettingsResponse>{
        return ProfileRoute.getallSettings(token: token)
    }
    
}
