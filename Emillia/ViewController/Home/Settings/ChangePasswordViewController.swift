//
//  ChangePasswordViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 15/03/2021.
//

import UIKit
import MHLoadingButton

class ChangePasswordViewController: BaseViewController {

    
    @IBOutlet weak var oldPass: UITextField!
    
    @IBOutlet weak var newPass: UITextField!
    
    @IBOutlet weak var confirmPass: UITextField!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    
    @IBOutlet weak var loadingBtn: LoadingButton!
    let token = Utils.getToken()
    let userId = Utils.getUserId()
    
    let viewModel = SettingViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        addBoader(textField: oldPass,newPass,confirmPass)
        viewModel.dataMapper.delegate = self
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        setDelegate(textField: oldPass,newPass,confirmPass)
        oldPass.addRightImageToTextField2(using: passwordImage)
        newPass.addRightImageToTextField2(using: passwordImage)
        confirmPass.addRightImageToTextField2(using: passwordImage)
        loadingBtn.indicator = MaterialLoadingIndicator(color: .white)
   
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func changePassword(_ sender: LoadingButton) {
        if validate() {
            let data = ChangePassword(oldPassword: oldPass.text!, newPassword: confirmPass.text!)
            viewModel.changePassword(token: token, userPassword: data, userId: userId)
        }
    }
    
  
    
    func validate()-> Bool{
        var value = true
        if oldPass.text!.isEmpty {
            setupErrorMessage(error: "Old Password is Required", errorMessage: errorMessage1, isHidden: false, textField: oldPass)
            oldPass.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if newPass.text!.isEmpty {
            setupErrorMessage(error: "New Password is Required", errorMessage: errorMessage2, isHidden: false, textField: newPass)
            newPass.layer.borderColor = UIColor.red.cgColor
            value = false
        }
//        else if !newPass.text!.isEmpty{
//            do {
//                let _ = try self.newPass.validatedText(validationType: ValidatorType.password)
//                value = true
//            } catch(let error) {
//                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage2, isHidden: false, textField: newPass)
//                value = false
//            }
//
//        }
        else{
            if newPass.text!.count <= 5 {
                setupErrorMessage(error: "Password must be greater than 5 character.", errorMessage: errorMessage2, isHidden: false, textField: newPass)
                newPass.layer.borderColor = UIColor.red.cgColor
                value = false
            }else{
                if isPasswordValid(newPass.text!){
                   value = true
                }else{
                    setupErrorMessage(error: "Password should contain numbers,alphabet and symbols only.", errorMessage: errorMessage2, isHidden: false, textField: newPass)
                    newPass.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
           
        }
        if confirmPass.text!.isEmpty {
            setupErrorMessage(error: "Confirm Password is Required", errorMessage: errorMessage3, isHidden: false, textField: confirmPass)
            confirmPass.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        if newPass.text! != confirmPass.text! {
            setupErrorMessage(error: "password Mis-match", errorMessage: errorMessage3, isHidden: false, textField: confirmPass)
            confirmPass.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        return value
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        if password.count < 6 {
            return false
        }else{
            
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=[^a-z]*[a-z])(?=[^0-9]*[0-9])[a-zA-Z0-9!@#$%^&*]{8,}")
            return passwordTest.evaluate(with: password)
        }
 
    }
    
}
extension ChangePasswordViewController:ResponseDisplay{
    
    func result<T>(success: Bool, data: T?) {
        if success{
            if let result = data as? ForgotPasswordResponse{
                guard let status = result.status else { return }
                if status {
                    showAlerAndPopCloseViewController(message: "Password Changed Successfully")
                }else{
                    showAlert(message: result.statusMessage ?? "")
                }
            }
        }else{
            
        }
    }
    
    
    func loading(loading: Bool) {
        if loading {
            loadingBtn.showLoader(userInteraction: true)
        }else{
            loadingBtn.hideLoader{
                
            }
        }
    }
    
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3)
        clearBorder(textField:oldPass,newPass,confirmPass)
      
    }
    
    
    
}
