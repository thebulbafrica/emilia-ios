//
//  SecurityViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 15/03/2021.
//

import UIKit

class SecurityViewController: BaseViewController {

    @IBOutlet weak var faceId: UIView!
    @IBOutlet weak var changePassword: UIView!
    @IBOutlet weak var biometricSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        biometricSwitch.isOn = Utils().getBiometric()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        changePassword.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "changepassword")
    }

    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func biometricTrigger(_ sender: UISwitch) {
        Utils().saveBiometrics(allow: sender.isOn)
    }
    
    func moveToVc(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(nextVc, animated: true)
        //tabViewController
    }
}
//
