//
//  SettingsViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 15/03/2021.
//

import UIKit
import RxSwift

class SettingsViewController: BaseViewController {

    
    @IBOutlet weak var personalInfo: UIView!
    
    @IBOutlet weak var security: UIView!
    
    @IBOutlet weak var notification: UIView!
    
    @IBOutlet weak var annoucement: UIView!
    
    @IBOutlet weak var tutorVideo: UIView!
    
    @IBOutlet weak var support: UIView!
    
    @IBOutlet weak var notificatinEnablerButtton: UISwitch!
    
    @IBOutlet weak var logout: UIView!
    
    @IBOutlet weak var numberOfNotification: UILabel!
    
    var videoUrl = ""
    let viewModel = NotificationRoute()
    let viewModel2 = SettingViewModel()
    let viewModel3 = LoginViewModel()
    let disposeBag = DisposeBag()
    let child = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        personalInfo.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap2(_:)))
        security.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap3(_:)))
        logout.addGestureRecognizer(tap3)
        
//        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap4(_:)))
//        notification.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap5(_:)))
        annoucement.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap6(_:)))
        tutorVideo.addGestureRecognizer(tap6)
        
        let tap7 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap7(_:)))
        support.addGestureRecognizer(tap7)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getNotification(token: Utils.getToken(), unUsed: nil)
        navigationController?.isNavigationBarHidden = true
    }
   
    @IBAction func enableNotification(_ sender: UISwitch) {
        
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "personal")
    }
    
    @objc func handleTap2(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "security")
    }
    

    @objc func handleTap3(_ sender: UITapGestureRecognizer? = nil) {
        logoutAlert()
        //navigationController?.popToRootViewController(animated: true)
    }
    
 
    
    @objc func handleTap5(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "notification")
    }
    
    @objc func handleTap6(_ sender: UITapGestureRecognizer? = nil) {
        moveToWebView(url: videoUrl)
    }
    
    @objc func handleTap7(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "support")
    }
    
    func moveToWebView(url:String){
       if let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsAndConditionViewController")
        as? TermsAndConditionViewController {
        nextVc.urlToLoad = url
        self.navigationController?.pushViewController(nextVc, animated: true)
        
        }
        //tabViewController
    }
    

    func moveToVc(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(nextVc, animated: true)
        //tabViewController
    }
    
    func getAllSetting(){
        showSpinnerView(child: child)
        viewModel2.getAllSetting(token: Utils.getToken()).subscribe {[weak self] result in
            self?.removeSpinnerView(child: self!.child)
            self?.videoUrl = result.data?.tutorialVideo ?? ""
        } onError: {error in
            self.removeSpinnerView(child: self.child)
            self.showAlert(message: "Unable to Load all Data")
        } onCompleted: {
            self.removeSpinnerView(child: self.child)
        } onDisposed: {
            self.removeSpinnerView(child: self.child)
        }.disposed(by: disposeBag)

    }
    

    func logoutAlert(){
        
        // create the alert
        let alert = UIAlertController(title: "LogOut", message: "Are you sure you want to LogOut?", preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {[weak self] action in
            self?.viewModel.logoutUser(userID: Utils.getUserId(), token: Utils.getToken(), user: nil)
            
        }))
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    private func configure(){
        viewModel.loading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            DispatchQueue.main.async {

                if loading{
                    this.showSpinnerView(child: this.child)
                }else{
                    this.removeSpinnerView(child: this.child)
                }
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.allNotification.observe(on: MainScheduler.instance).subscribe(onNext:{[weak self]result in
            DispatchQueue.main.async {
                self?.numberOfNotification.text = "\(result.count)"
            }
            
        } ).disposed(by: disposeBag)
        
        viewModel.errors.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.logOutDevice.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (result) in
            guard let self = self else { return }
            UserDefaults.clear()
            for obj in (self.navigationController?.viewControllers)! {
                
                if obj is LoginViewController {
                    let controller = obj as! LoginViewController
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
                
            }
        })
        .disposed(by: disposeBag)
    }
    

}
