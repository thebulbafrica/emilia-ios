//
//  ProfileViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import UIKit
import SKCountryPicker
import MHLoadingButton
import RxSwift
import FlagPhoneNumber

class ProfileViewController: BaseViewController {

    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var emailAddress: UITextField!
    
    // @IBOutlet weak var countryCodeText: UILabel!
    
   // @IBOutlet weak var countryFlag: UIImageView!
    
    @IBOutlet weak var phoneNumber: FPNTextField!
    
    @IBOutlet weak var phoneStackView: UIStackView!
    
    //@IBOutlet weak var countryCodes: UIStackView!
    
    @IBOutlet weak var updateBtn: LoadingButton!
    
    @IBOutlet weak var chagePix: UIButton!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    
    let viewModel = SettingViewModel()
    let userId = Utils.getUserId()
    let phoneUserNumber = Utils.getUserUserdetails()?.phone
    let token = Utils.getToken()
    let dispose = DisposeBag()
    
    var isValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chagePix.isHidden = true
        viewModel.dataMapper.delegate = self
        updateBtn.indicator = MaterialLoadingIndicator(color: .white)
        
        addBoader(textField: firstName,lastName,userName,emailAddress)
        setDelegate(textField: firstName,lastName,userName,emailAddress)
        stackViewBoader(stackView: phoneStackView)
        let _ = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
       // countryCodes.addGestureRecognizer(tap)
        fetchData(token: token, id: userId)
        configurePhoneNumber()
    }
    
    func configurePhoneNumber(){
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.setFlag(countryCode: .NG)
        phoneNumber.hasPhoneNumberExample = true
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.displayMode = .list
        
        listController.setup(repository: phoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.phoneNumber.setFlag(countryCode: country.code)
        }

        phoneNumber.delegate = self
        setDelegate(textField: phoneNumber)
        
        phoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        phoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
          // handling code
          let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
             // self.countryCodeText.text = country.dialingCode
           // self.countryFlag.image = country.flag
          }
       
          countryController.detailColor = UIColor.red

      }
    
    func fetchData(token:String,id:Int){
        viewModel.getUserProfile(token: token, userId: id).subscribe { [weak self](result) in
            self?.firstName.text = result.data.firstname
            self?.lastName.text = result.data.lastname
            self?.userName.text = result.data.address
            self?.emailAddress.text = result.data.email
            //self?.phoneNumber.text = result.data.phone
            if let no = result.data.phone{
                self?.phoneNumber.set(phoneNumber:no)
            }
           
        } onError: { (error) in
            print(error)
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)

    }

    @IBAction func subscribeBtn(_ sender: LoadingButton) {
        if validate(){
            let data = UpdateProfile(title: nil, firstName: firstName.text, lastName: lastName.text, otherName: nil, address: userName.text, address2: nil, city: nil, state: nil, country: nil, company: nil, rcno: nil)
            viewModel.updateProfile(token: token, userId: userId, data: data)
        }
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func validate()-> Bool{
        var value = true
        if firstName.text!.isEmpty {
            setupErrorMessage(error: "First Name is Empty", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            firstName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if firstName.text!.count < 3{
            setupErrorMessage(error: "Invalid First Name", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }

        if lastName.text!.isEmpty {
            setupErrorMessage(error: "Last Name is Empty", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if lastName.text!.count < 3{
            setupErrorMessage(error: "Invalid Last Name", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
      
        if emailAddress.text!.isEmpty {
            setupErrorMessage(error: "Email is Empty", errorMessage: errorMessage4, isHidden: false, textField: emailAddress)
            emailAddress.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            do {
                let _ = try self.emailAddress.validatedText(validationType: ValidatorType.email)
                value = true
            } catch(let error) {
                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage4, isHidden: false, textField: emailAddress)
                value = false
            }
            
        }
        if userName.text!.isEmpty {
            setupErrorMessage(error: "Address is Empty", errorMessage: errorMessage5, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if userName.text!.count < 2{
            setupErrorMessage(error: "Invalid Address", errorMessage: errorMessage3, isHidden: false, textField: userName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
        
        if phoneNumber.text!.isEmpty {
            setupErrorMessage(error: "Phone Number is Required", errorMessage: errorMessage3, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        else if !isValid{
            print("value",isValid)
            setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage3, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }

       
        return value
    }
    
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success{
            if let result = data as? UpdateProfileResponse{
                if result.status{
                    showAlerAndPopCloseViewController(message: "Profile Updated Successfully")
                }else{
                    showAlert(message: result.statusMessage)
                }
            }
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
               
            }
        }
    }
    
    
    
    func loading(loading: Bool) {
        if loading {
            updateBtn.showLoader(userInteraction: true)
        }else{
            updateBtn.hideLoader{

            }
        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:firstName,lastName,phoneNumber, emailAddress,userName)
        
    }
    
    
    
}

extension ProfileViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:firstName,lastName,phoneNumber, emailAddress,userName)
//        print(
//            isValid,
//            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
//            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
//            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
//            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
//            textField.getRawPhoneNumber() ?? "Raw: nil"
//        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}
