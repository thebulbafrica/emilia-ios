//
//  SupportViewController.swift
//  Emillia
//
//  Created by apple on 25/08/2021.
//

import UIKit
import RxSwift

class SupportViewController: UIViewController {

    @IBOutlet weak var chatButton: UIStackView!
    
    @IBOutlet weak var callSupportButton: UIStackView!
    
  //  @IBOutlet weak var sendEmail: UIStackView!
    
    @IBOutlet weak var privacyButton: UIStackView!
    
    @IBOutlet weak var faqButton: UIStackView!
    
    @IBOutlet weak var termsAndConditionButton: UIStackView!
    
    @IBOutlet weak var cookiePolicyButton: UIStackView!
    
    let viewModel = SettingViewModel()
    let child = SpinnerViewController()
    let disposeBag = DisposeBag()
    
    var allSetting:AllSettings? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAllSetting()
        tapGestures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func tapGestures(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        chatButton.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap2(_:)))
        callSupportButton.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap3(_:)))
        privacyButton.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap4(_:)))
        faqButton.addGestureRecognizer(tap4)
        
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap5(_:)))
        termsAndConditionButton.addGestureRecognizer(tap5)
        
        let tap6 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap6(_:)))
        cookiePolicyButton.addGestureRecognizer(tap6)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        moveToVc(identifier: "ChatViewController")
    }
    
    @objc func handleTap2(_ sender: UITapGestureRecognizer? = nil) {
        moveToContact(identifier: "CallSupportViewController")
    }
    

    @objc func handleTap3(_ sender: UITapGestureRecognizer? = nil) {
        if let result = allSetting{
            moveToWebView(identifier: result.privacy ?? "")
        }
       // moveToWebView(identifier: "privacy")
    }
    
    
    
    @objc func handleTap4(_ sender: UITapGestureRecognizer? = nil) {
        if let result = allSetting{
            moveToWebView(identifier: result.faq ?? "")
        }
       // moveToWebView(identifier: "faq")
    }
    
    @objc func handleTap5(_ sender: UITapGestureRecognizer? = nil) {
        if let result = allSetting{
            moveToWebView(identifier: result.termandCondition ?? "")
        }
        //moveToWebView(identifier: "terms")
    }
    
    @objc func handleTap6(_ sender: UITapGestureRecognizer? = nil) {
        if let result = allSetting{
            moveToWebView(identifier: result.cookie ?? "")
        }
        //moveToWebView(identifier: "policy")
    }
    
    
    func moveToVc(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(nextVc, animated: true)
       
    }
    
    func moveToContact(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier) as! CallSupportViewController
        nextVc.contact = allSetting?.contacts
        self.navigationController?.pushViewController(nextVc, animated: true)
       
    }
    
    func moveToWebView(identifier:String){
       if let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TermsAndConditionViewController")
        as? TermsAndConditionViewController {
        nextVc.urlToLoad = identifier
        self.navigationController?.pushViewController(nextVc, animated: true)
        
        }
        //tabViewController
    }
    
    func getAllSetting(){
        showSpinnerView(child: child)
        viewModel.getAllSetting(token: Utils.getToken()).subscribe {[weak self] result in
            self?.removeSpinnerView(child: self!.child)
            self?.allSetting = result.data
        } onError: {error in
            self.removeSpinnerView(child: self.child)
            self.showAlert(message: "Unable to Load all Data")
        } onCompleted: {
            self.removeSpinnerView(child: self.child)
        } onDisposed: {
            self.removeSpinnerView(child: self.child)
        }.disposed(by: disposeBag)

    }
}
