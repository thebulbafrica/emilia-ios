//
//  TermsAndConditionViewController.swift
//  Emillia
//
//  Created by apple on 05/09/2021.
//

import UIKit
import WebKit

class TermsAndConditionViewController: UIViewController {
    
    var urlToLoad = ""
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(urlToLoad)
        navigationController?.isNavigationBarHidden = false
        if let url = URL(string: urlToLoad){
            webView.load(URLRequest(url: url))
        }
    }
    
}
