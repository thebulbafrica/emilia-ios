//
//  CallSupportViewController.swift
//  Emillia
//
//  Created by apple on 05/09/2021.
//

import UIKit

class CallSupportViewController: UIViewController {

    
    @IBOutlet weak var headOfficeLabel: UILabel!
    
    @IBOutlet weak var callNoLabel: UILabel!
    
    
    @IBOutlet weak var enquireLabel: UILabel!
    
    var contact:Contacts? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let contact = contact {
            headOfficeLabel.text = contact.address
            
            callNoLabel.text = contact.phone
            
            enquireLabel.text = contact.emials
        }
   
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func chatIconBtn(_ sender: UIButton) {
        moveToVc(identifier: "ChatViewController")
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func moveToVc(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(nextVc, animated: true)
       
    }
}
