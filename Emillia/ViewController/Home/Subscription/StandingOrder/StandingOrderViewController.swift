//
//  StandingOrderViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import UIKit

class StandingOrderViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    class func instantiateFromStoryboard() -> StandingOrderViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! StandingOrderViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        createImage(tableView: tableView, height: 200, width: 200, firstText: "No Standing Order", secondText: "You currently do not have any Standing Order")
        // Do any additional setup after loading the view.
    }
    


}
extension StandingOrderViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subhistroycell", for: indexPath) as! SubHistroyTableViewCell
      
        return cell
    }
    
    
}
