//
//  SubHistroyViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import UIKit
import ShimmerSwift
import RxSwift
import MHLoadingButton

class SubHistroyViewController: BaseViewController,SelectedDelegate, DateDelegate {
    
    func update(selectedParameter: String, download: Bool) {
        if selectedParameter == "date"{
            opView.isHidden = true
            removebottomSheet()
            datepopUp(parentVC: self, download: download)
        }else if selectedParameter == "2"{
            opView.isHidden = true
            removebottomSheet()
            let chn = Utils.getUserChn()
            let value = SubFilter(chnno: chn, endDate: nil, startDate: nil, userID: userId, filterWithChn: nil, status: nil, brokerName: nil, instrument: nil, sendToMail: nil)
            filter(filter: value)
        }else{
            opView.isHidden = true
            removebottomSheet()
            let chn = Utils.getUserChn()
            let value = SubFilter(chnno: chn, endDate: nil, startDate: nil, userID: userId, filterWithChn: nil, status: selectedParameter, brokerName: nil, instrument: nil, sendToMail: nil)
            filter(filter: value)
           // filterByParameter(value: selectedParameter, token: token)
        }
    }
    
    func update(start: String, endDate: String) {
        let chn = Utils.getUserChn()
        let value = SubFilter(chnno: chn, endDate: endDate, startDate: start, userID: userId, filterWithChn: nil, status: nil, brokerName: nil, instrument: nil, sendToMail: nil)
        filter(filter: value)
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var chNumber: UITextField!
    
    @IBOutlet weak var continueBtn: LoadingButton!
    
    let child = SpinnerViewController()
    
    
    class func instantiateFromStoryboard() -> SubHistroyViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SubHistroyViewController
    }
    var data = [AllSubData]()
    let viewModel = SubscriptionViewModel()
    let dispose = DisposeBag()
    let token = Utils.getToken()
    let userId = Utils.getUserId()
    
    let opView =  UIView(frame: UIScreen.main.bounds)
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(opView)
        opView.isHidden = true
        stackView.isHidden = false
        tableView.isHidden = true
        chNumber.autocapitalizationType = .allCharacters
        viewModel.dataMapper.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
    }
    
    @objc func showSpinningWheel(_ notification: NSNotification) {
        if let selected = notification.userInfo?["selected"] as? Int {
            if selected == 1{
                if tableView.isHidden{
                    showAlert(message: "Enter CHN and Search before Filtering")
                }else{
                    removebottomSheet()
                    addBottomSheetView()
                }
                
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.isHidden = true
        stackView.isHidden = false
        removebottomSheet()
    }
    
    
    func loadData(token:String) {
        viewModel.getSubscription(token: token, userId: userId).subscribe {[weak self] (instrument) in
            guard let data = instrument.data else {
                self?.data.removeAll()
                self?.tableView.reloadData()
                self?.showAlert(message: instrument.statusMessage)
                
                return
            }
            
            self?.stackView.isHidden = true
            self?.tableView.isHidden = false
            
            if data.isEmpty{
                self?.data.removeAll()
                self?.tableView.reloadData()
                self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No Subscription Histroy", secondText: "You currently do not have any subcriptions, go to the instrument list to subscribe.")
            }else{
                if let viewWithTag = self?.tableView.viewWithTag(100) {
                       viewWithTag.removeFromSuperview()
                   }
                if let self = self{
                    self.data.removeAll()
                    self.data.append(contentsOf: data)
                    self.tableView.reloadData()
//                    self.tableView.showLoader()
//                    Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SubHistroyViewController.removeLoader), userInfo: nil, repeats: false)
                }
            }
        } onError: { (error) in
            print("error: ",error)
        } onCompleted: {
            print("completed: ","completed")
        } onDisposed: {
            print("disposed: ","disposed")
        }.disposed(by: dispose)
        
    }
    
    @objc func removeLoader()
    {
        self.tableView.hideLoader()
    }
    
    
    @IBAction func conBtn(_ sender: LoadingButton) {
        if chNumber.text!.isEmpty{
            showAlert(message: "Enter CHN")
        }else{
            Utils.saveUserChnn(chn: chNumber.text!)
            let value = SubFilter(chnno: chNumber.text!, endDate: nil, startDate: nil, userID: userId, filterWithChn: nil, status: nil, brokerName: nil, instrument: nil, sendToMail: nil)
            filter(filter: value)
        }
       
    }
    
    
    func filter(filter: SubFilter){
        continueBtn.showLoader(userInteraction: true)
        viewModel.filterSubscription(token: Utils.getToken(), filter: filter).subscribe {[weak self] instrument in
            self?.continueBtn.hideLoader{}
            
            guard let data = instrument.data else {
                self?.data.removeAll()
                self?.tableView.reloadData()
                self?.showAlert(message: instrument.statusMessage)

                return
                
            }
            
            self?.stackView.isHidden = true
            self?.tableView.isHidden = false
            if data.isEmpty{
                self?.data.removeAll()
                self?.tableView.reloadData()
                self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No Subscription Histroy", secondText: "You currently do not have any subcriptions, go to the instrument list to subscribe.")
            }else{
                if let viewWithTag = self?.tableView.viewWithTag(100) {
                       viewWithTag.removeFromSuperview()
                   }
                if let self = self{
                    self.data.removeAll()
                    self.data.append(contentsOf: data)
                    self.tableView.reloadData()
                    self.tableView.showLoader()
                    Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SubHistroyViewController.removeLoader), userInfo: nil, repeats: false)
                }
            }
            
        } onError: { (error) in
            print("error: ",error)
            self.continueBtn.hideLoader{}
        } onCompleted: {
            print("completed: ","completed")
            self.continueBtn.hideLoader{}
        } onDisposed: {
            print("disposed: ","disposed")
            self.continueBtn.hideLoader{}
        }.disposed(by: dispose)
    }
    
    
    func addBottomSheetView() {
        opView.isHidden = false
        opView.backgroundColor = .black
        opView.layer.opacity = 0.5
        
        
        if let bottomSheetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomFilterTransactionViewController") as? BottomFilterTransactionViewController {
            // 2- Add bottomSheetVC as a child view
            bottomSheetVC.sub = true
            bottomSheetVC.delegate = self
            self.addChild(bottomSheetVC)
            self.view.addSubview(bottomSheetVC.view)
            bottomSheetVC.didMove(toParent: self)
            
            // 3- Adjust bottomSheet frame and initial position.
            
            let height = view.frame.height
               let width  = view.frame.width
            bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        }
    }
    
    func removebottomSheet(){
        if self.children.count > 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                if viewContoller is BottomFilterTransactionViewController{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
                break
            }
        }
    }
    
    func datepopUp(parentVC: UIViewController,download:Bool){
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DateModalViewController") as? DateModalViewController {
            popupViewController.modalPresentationStyle = .custom
            popupViewController.modalTransitionStyle = .crossDissolve
            popupViewController.delegate = self
            popupViewController.download = download
            popupViewController.sub = true
            parentVC.present(popupViewController, animated: true)
        }
    }
}


extension SubHistroyViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subhistroycell", for: indexPath) as! SubHistroyTableViewCell
        let allData = data[indexPath.row]
        cell.applicationId.text = allData.applicationid
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let totalAmountSpent = numberFormatter.string(from: NSNumber(value:allData.amount ))
        cell.amount.text = "₦" + String(totalAmountSpent!)
        cell.applicationName.text = allData.name
        cell.dueDate.text =  allData.code
        
        
        cell.viewMore.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        cell.viewMore.tag = indexPath.row
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        moveToVc(data:item)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        let data = data[buttonTag]
        
        getLastSub(subId: data.id)
        //moveToVc(data:item)
    }
    
    func moveToVc(data:AllSubData){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "viewmoresubscription") as? SingleSubscriptionViewController
        if let viewController = nextVc{
            viewController.subHistroy = data
            self.navigationController?.pushViewController(viewController, animated: true)
        }
  
    }
    
    func moveToReb(data:ResubscribeData){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "instrumentdetails") as? InstrumentDetailsViewController
        if let viewController = nextVc{
           viewController.reSub = data
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func getLastSub(subId:Int){
        //let userID = Utils.getUserId()
        showSpinnerView(child:child)
        viewModel.ReSubscripe(token: token, subID: subId).subscribe { (data) in
            self.removeSpinnerView(child: self.child)
            if data.statusCode == 200{
                if let data = data.data{
                    self.moveToReb(data: data)
                   // Utils.saveLastSubscriptionDetails(user: data)
                }
            }else{
                self.showAlert(message: data.statusMessage)
            }
        } onError: { (error) in
            self.showAlert(message: "Unable to Continue at the momment")
            self.removeSpinnerView(child: self.child)
        } onCompleted: {
            print("completed: ","completed")
            self.removeSpinnerView(child: self.child)
        } onDisposed: {
            print("disposed: ","disposed")
            self.removeSpinnerView(child: self.child)
        }.disposed(by: dispose)
    }
    
    
    
    
}

extension SubHistroyViewController:ResponseDisplay{
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        
    }
    
    
}
