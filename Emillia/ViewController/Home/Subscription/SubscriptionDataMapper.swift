//
//  SubscriptionDataMapper.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation

class SubscriptionDataMapper: ServiceResponder {
   
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T) {
        delegate?.result(success: true, data: response)

    }
    
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    

    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
    }
    
    
}
