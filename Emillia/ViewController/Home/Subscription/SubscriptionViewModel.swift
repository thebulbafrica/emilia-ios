//
//  SubscriptionViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
import RxSwift
import Alamofire


public class SubscriptionViewModel{
    
    private let apiClient = APIClient()
    let dataMapper = SubscriptionDataMapper()
    
    let dispose = DisposeBag()
    
    let loading: PublishSubject<Bool> = PublishSubject()
    let error : PublishSubject<CustomError> = PublishSubject()
    let nonTradingNumber: PublishSubject<String> = PublishSubject()
    let validateBVNResponse: PublishSubject<BVNValidateResponse> = PublishSubject()
    let validateBankResponse: PublishSubject<String> = PublishSubject()
    let subscriptionValue: PublishSubject<String> = PublishSubject()
    let callbackResponse: PublishSubject<ResponseData> = PublishSubject()
    private let baseUrl = Utils.baseUrl
 
    func getInstrument(token:String) -> Observable<InstrumentResponse> {
        return  SubscriptionRoute.getInstrument(token: token, serviceResponse: dataMapper)
    }
    
    func getSubscription(token:String,userId:Int) -> Observable<AllSubDataResponse> {
        return  SubscriptionRoute.getSubscription(token: token, userId: userId, serviceResponse: dataMapper)
    }
    
    func getBroker(token:String) -> Observable<BrokersResponse>{
        return  SubscriptionRoute.getBrokers(token: token, serviceResponse: dataMapper)
    }
    
    func validateCHN(token:String,validate:ValidateChN){
        return  SubscriptionRoute.validateCHN(token:token,user: validate, serviceResponse: dataMapper)
    }
    
    func validateBank(token:String,userAccount:AccountData){
        return  SubscriptionRoute.valideAccountNumber(token: token, user: userAccount, serviceResponse: dataMapper)
    }
    
    func validateBvn(token:String,userAccount:BVNData){
        return  SubscriptionRoute.validateBVN(token: token, user: userAccount, serviceResponse: dataMapper)
    }
    
    func convertIntToWords(input:String)->String{
        let convertString = Int(input)
        if let result = convertString{
            return result.asWord!
        }else{
            return ""
        }
    }
    
    func getBanks() -> [Bank]?{
        return  SubscriptionRoute.getBanks()
    }
 
    func submitSub(token:String,userId:Int,data:SubscriptionSubmissionData){
        SubscriptionRoute.submitSubscribtion(token: token, userId: userId, data: data, serviceResponse: dataMapper)
    }

    func getPayment(token:String,transRef:String)->Observable<GetPaymentResponse>{
        return SubscriptionRoute.getPayment(token: token, transRef: transRef, serviceResponse: dataMapper)
    }
 
    func getExistingSub(token:String,userId:Int)->Observable<GetExistingSubResponse>{
       return SubscriptionRoute.getExistingSub(token: token, userId: userId, serviceResponse: dataMapper)
        
    }
    
    func ReSubscripe(token:String,subID:Int)->Observable<ResubscribeResponse>{
       return SubscriptionRoute.reSubscribe(token: token, subID:subID, serviceResponse: dataMapper)
        
    }
    
    func getAllCountry(token:String)->Observable<allcountry>{
       return AllCountriesAndState.getAllCountry(token: token, serviceResponse: dataMapper)
    }
    
    func getAllState(token:String,countryId:Int)->Observable<allState>{
        return AllCountriesAndState.getAllState(token: token, countryId: countryId, serviceResponse: dataMapper)
    }
    
    func getAllCity(token:String,stateId:Int)->Observable<allCity>{
        return AllCountriesAndState.getAllCity(token: token, stateId: stateId, serviceResponse: dataMapper)
    }
    
    
    func filter(token:String,filter:SearchList){
        SubscriptionRoute.filter(token: token, filter: filter,serviceResponse: dataMapper)
    }
    
    
    func filterSubscription(token:String,filter:SubFilter)->Observable<AllSubDataResponse>{
        SubscriptionRoute.filterSubscription(token: token, filter: filter, serviceResponse: dataMapper)
    }
    
  

    func validateBVN(token:String,user:BVNData?){
       
        self.loading.onNext(true)
        
        let result = apiClient.send(path: "/api/cscs/validate/bvn/record", method: .post, token: token, parameters: user) as Observable<BVNValidateResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            self.validateBVNResponse.onNext(result)
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.error.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.error.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.error.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: dispose)
       //return result
    }
    
    func validateBankDetails(token:String,userAccount:AccountData?){
       
        self.loading.onNext(true)
        
        do {
            
            let jsonBody = try JSONEncoder().encode(userAccount)
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        
        let result = apiClient.send(path: "/api/cscs/verify/account", method: .post, token: token, parameters: userAccount) as Observable<String>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            self.validateBankResponse.onNext(result)
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.error.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.error.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.error.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: dispose)
       //return result
    }
    
    
    func nonTradingAccount(token:String,submit:SubscriptionSubmissionData?){
      
        self.loading.onNext(true)
        
        let parameter = NonTradingSendData(bankName: submit?.bankname ?? "", bankAccNo: submit?.bankacctno ?? "", bopDate: submit?.bankOpeningDate ?? "", nxPhone: submit?.nextkinphone ?? "", gender: submit?.gender ?? "", madianName: submit?.mmaidenname ?? "", cpName: submit?.companyname ?? "", cpPhone: submit?.phone ?? "", bvn: submit?.bvnno ?? "", state: submit?.state ?? "", taxID: submit?.taxId ?? "", userID: Utils.getUserId())
       
        let result = apiClient.send(path: "/api/subscription/NonTradingAccount", method: .post, token: token, parameters: parameter) as Observable<NoTradingResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            if result.status{
                if let res = result.data{
                    self.nonTradingNumber.onNext(res)
                }else{
                    self.error.onNext(.serverMessage(result.statusMessage))
                }
            }else{
                self.error.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.error.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.error.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.error.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: dispose)
       //return result
    }
    
    
    func subscribe(token:String,userId:Int,data:SubmitSubscriptionData?){
       
        self.loading.onNext(true)
        
        do {
            
            let jsonBody = try JSONEncoder().encode(data)
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        
        let result = apiClient.send(path: "/api/subscription/subscribe/\(userId)", method: .post, token: token, parameters: data) as Observable<SubscriptionSubmissionDataResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            if result.status{
                if let res = result.data{
                    self.subscriptionValue.onNext(res)
                }else{
                    self.error.onNext(.serverMessage(result.statusMessage))
                }
            }else{
                self.error.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.error.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.error.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.error.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: dispose)
       //return result
    }
    
    func getPaymentLink(token:String, data:GetCallbackBody?){
       
        self.loading.onNext(true)
        
        do {
            
            let jsonBody = try JSONEncoder().encode(data)
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        
        let result = apiClient.send(path: "/api/transactions/GeneratePaymentLink", method: .post, token: token, parameters: data) as Observable<GetCallbackResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            if result.status ?? false{
                if let res = result.data?.data{
                    self.callbackResponse.onNext(res)
                }else{
                    self.error.onNext(.serverMessage(result.statusMessage ?? ""))
                }
            }else{
                self.error.onNext(.serverMessage(result.statusMessage ?? ""))
            }
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.error.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.error.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.error.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: dispose)
       //return result
    }
}


//static func filter(token:String,filter:SearchList, completion: @escaping (_ success: Bool, _ error: Error?, _ result:InstrumentResponse?)-> Void){
//
//
//    
//    var generatedToken:String{
//        if !token.contains("Bearer"){
//            return "Bearer \(token)"
//        }else{
//            return token
//        }
//    }
//    guard let url = URL(string:"\(baseUrl)/api/subscription/search/offers") else {return}
//    var request = URLRequest(url: url)
//    request.httpMethod = "POST"
//    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//    request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
//    do {
//        let jsonBody = try JSONEncoder().encode(filter)
//        request.httpBody = jsonBody
//        print("jsonBody:",jsonBody)
//        let jsonBodyString = String(data: jsonBody, encoding: .utf8)
//        print("JSON String : ", jsonBodyString!)
//    } catch let err  {
//        print("jsonBody Error: ",err)
//    }
//    let session = URLSession.shared
//    let task = session.dataTask(with: request){ (data,response,err) in
//        
//        guard let data = data else {return}
//        
//        do{
//            let results = try JSONDecoder().decode(InstrumentResponse.self, from: data)
//            DispatchQueue.main.async {
//                completion(true,nil,results)
//              //  serviceResponse.success(response: results)
//              //  serviceResponse.loading(loading: false)
//            }
//            print("DATA:\(data)")
//            print("DATA:\(results)")
//        }catch let err{
//            DispatchQueue.main.async {
//                completion(false,err,nil)
//               // serviceResponse.failed(error: .failedRequest)
//               // serviceResponse.loading(loading: false)
//            }
//            
//            print("Session Error: ",err)
//        }
//    }
//    task.resume()
//}
