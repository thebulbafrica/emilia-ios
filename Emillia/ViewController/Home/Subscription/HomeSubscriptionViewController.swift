//
//  HomeSubscriptionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import Foundation
import UIKit
import PagingMenuController

class HomeSubscriptionViewController: BaseViewController {
 
    @IBOutlet weak var titleAppBar: UILabel!

    @IBOutlet weak var backButton: UIButton!

    
    @IBOutlet weak var filterBtn: UIButton!
    
    var viewControl:Any? = nil
    
    var currentPage = 0
    
    var selectedPage = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let options = PagingMenuOptions()
        
        
         

        let pagingMenuController = PagingMenuController(options: options)
        pagingMenuController.view.frame.origin.y += 120
        pagingMenuController.view.frame.size.height -= 120

        addChild(pagingMenuController)
        view.addSubview(pagingMenuController.view)
        pagingMenuController.didMove(toParent: self)
        getViewOn()
    }

 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
       
        
    }
    
    @IBAction func filterTypeSelection(_ sender: UIButton) {
       
        
        if selectedPage == 0{
            
            let dataDict:[String: Int] = ["selected": 0]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: dataDict)
            
        }else if selectedPage == 1{
           
            let dataDict:[String: Int] = ["selected": 1]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: dataDict)
        }else{
            let dataDict:[String: Int] = ["selected": 2]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notificationName"), object: nil, userInfo: dataDict)
        }
    }
    
    
    func getViewOn(){
        if self.children.count > 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                if viewContoller is PagingMenuController{
                    let viewCont = viewContoller as! PagingMenuController
                    currentPage = viewCont.currentPage
                    viewCont.onMove = { state in
                        switch state {
                        case let .willMoveController(menuController, _):
                           
                            if menuController is InstrumentListViewController{
                                self.filterBtn.isHidden = false
                                self.selectedPage = 0
                                //self.viewControl = InstrumentListViewController()
                            }else if menuController is SubHistroyViewController{
                                self.filterBtn.isHidden = false
                                self.selectedPage = 1
                                //self.viewControl = SubHistroyViewController
                            }else{
                                self.filterBtn.isHidden = true
                                self.selectedPage = 2
                                //self.viewControl = StandingOrderViewController()
                            }
                        case .didMoveController(_, _):
                           // print(previousMenuController)
                            print("")
                        case .willMoveItem(_, _):
                           // print(previousMenuItemView)
                            print("")
                        case .didMoveItem(_, _):
                           // print(previousMenuItemView)
                            print("")
                        case .didScrollStart:
                            print("Scroll start")
                        case .didScrollEnd:
                            print("Scroll end")
                        }
                    }
                }
            }
        }
    }
    
   
}

