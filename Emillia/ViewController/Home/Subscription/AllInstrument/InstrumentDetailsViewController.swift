//
//  InstrumentDetailsViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import RxSwift

class InstrumentDetailsViewController: BaseViewController {

    
    @IBOutlet weak var instrumentName: UITextField!
    
    @IBOutlet weak var facevalue: UITextField!
    
    @IBOutlet weak var subscriptionAmount: UITextField!
    
    @IBOutlet weak var subscriptionUnit: UITextField!
    
    
    @IBOutlet weak var subUnitTex: UILabel!
    
    @IBOutlet weak var subUnitTextSmall: UILabel!
    
    private let errorMessage1 = UILabel()
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    
    var reSub:ResubscribeData? = nil
    var viewModel = SubscriptionViewModel()
    var child = SpinnerViewController()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        updateData()
        instrumentName.text = subscribtionData?.instrument.name
        if let fValue = subscribtionData?.offer.minval{
            facevalue.text = String(fValue)
            //Subscription Unit (Multiple of N1000)
            //The cost of one (1) unit is 1000 Naira.
            subUnitTex.text = "Subscription Unit (Multiple of N\(fValue))"
            subUnitTextSmall.text = "The cost of one (1) unit is \(fValue) Naira."
        }
        setDelegate(textField:instrumentName,facevalue,subscriptionAmount,subscriptionUnit)
        addBoader(textField:instrumentName,facevalue,subscriptionAmount,subscriptionUnit)
        // Do any additional setup after loading the view.
        subscriptionUnit.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    func updateData(){
        if let reSubData = reSub{
            subscribtionData = AllInstrument(offer: reSubData.offer.offer, instrument: reSubData.offer.instrument)
            submistionData = SubscriptionSubmissionData(offerid: reSubData.offer.offer.id, amount: 0, subamount: 0, totalamount: 0, firstname: reSubData.subscriptionData.firstname, lastname: reSubData.subscriptionData.lastname, title: reSubData.subscriptionData.title, occupation: reSubData.subscriptionData.occupation, phone: reSubData.subscriptionData.phone, nextkinfname: reSubData.subscriptionData.nextkinfname, nextkinlname: reSubData.subscriptionData.nextkinlname, address: reSubData.subscriptionData.address, address2: reSubData.subscriptionData.address2, passportno: reSubData.subscriptionData.passportno, dob: reSubData.subscriptionData.dob, mmaidenname: reSubData.subscriptionData.mmaidenname, email: reSubData.subscriptionData.email, bankname: reSubData.subscriptionData.bankname, bankacctno: reSubData.subscriptionData.bankacctno, bvnno: reSubData.subscriptionData.bvnno, companyname: reSubData.subscriptionData.companyname, businesstype: reSubData.subscriptionData.businesstype, rcno: reSubData.subscriptionData.rcno, contactperson: reSubData.subscriptionData.contactperson, cscsno: reSubData.subscriptionData.cscsno, chnno: reSubData.subscriptionData.chnno, brokername: reSubData.subscriptionData.brokername, gender: reSubData.subscriptionData.gender, nextkinphone: reSubData.subscriptionData.nextkinphone, nextkinemail: reSubData.subscriptionData.nextkinemail, nextkinrelation: reSubData.subscriptionData.nextkinrelation, city: reSubData.subscriptionData.city, state: reSubData.subscriptionData.state, postcode: reSubData.subscriptionData.postcode, country: reSubData.subscriptionData.country, brokercode: reSubData.subscriptionData.brokercode, nextkinothername: reSubData.subscriptionData.nextkinothername)
        }
    }
    
    @objc final private func textFieldDidChange(textField: UITextField){
        if let field = textField.text{
            if field.isEmpty{
                facevalue.text = String((subscribtionData?.offer.minval)!)
                subscriptionAmount.text = ""
            }
            let value = Int(field)
            if let resultValue = value{
                if let fValue = subscribtionData?.offer.minval{
                    let userFaceValue = resultValue * fValue
                    facevalue.text = String(userFaceValue)
                    subscriptionAmount.text = Utils.convert(number: userFaceValue as NSNumber).uppercased()
                }
               
                
            }
        }
    }
    
    
    func validateField()-> Bool{
        var value = true
        if subscriptionUnit.text!.isEmpty {
            setupErrorMessage(error: "Subscription Unit is Empty", errorMessage: errorMessage1, isHidden: false, textField: subscriptionUnit)
            subscriptionUnit.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }
    
    
    @IBAction func submit(_ sender: UIButton){
        if validateField(){

            if let sub = submistionData?.isChn{
                if sub{
                    // submistionData?.unit = Int(subscriptionUnit.text!)!
                     submistionData?.amount = Int(facevalue.text!)!
                     //submistionData?.amount = subscribtionData?.offer.minval
                    moveToNext()
                }else{
                    submistionData?.amount = Int(facevalue.text!)!
                    viewModel.nonTradingAccount(token: Utils.getToken(), submit: submistionData)
                }
            }
        }
    }
    
    func getSubDetails(){
        if let _ =  Utils.getLastSubscriptionDetails(){
            
        }
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "confirmation") as? ConfirmationViewController
        if let registerVC = nextVc{
            registerVC.unit = subscriptionUnit.text!
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }

}

extension InstrumentDetailsViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1)
        clearBorder(textField: subscriptionUnit)
    }
    
    private func configure(){
        viewModel.loading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            DispatchQueue.main.async {

                if loading{
                    this.showSpinnerView(child: this.child)
                }else{
                    this.removeSpinnerView(child: this.child)
                }
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.nonTradingNumber.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] result in
            guard let this = self else{return}
            DispatchQueue.main.async {
                this.submistionData?.chnno = result
                this.moveToNext()
                
            }
        }).disposed(by: disposeBag)

        
        viewModel.error.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
    }
}
