//
//  InstrumentListViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import UIKit
import ShimmerSwift
import RxSwift
import ListPlaceholder

class InstrumentListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    class func instantiateFromStoryboard() -> InstrumentListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InstrumentListViewController
    }
    
    var selectedIndex = 0

    let dispose = DisposeBag()
    let dispose2 = DisposeBag()
    var allInstrumentData = [AllInstrument]()
    let viewModel = SubscriptionViewModel()
    let token = Utils.getToken()
    var allData = SubscriptionSubmissionData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        loadData(token:token)
       // getLastSub()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(_:)), name: NSNotification.Name(rawValue: "notificationName"), object: nil)
    }

    @objc func showSpinningWheel(_ notification: NSNotification) {
        if let selected = notification.userInfo?["selected"] as? Int {
            if selected == 0{
                popUpMenu()
            }
        }
    }
   

    @IBAction func search(_ sender: UIButton) {
       
    }
    
    func popUpMenu(){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Show open instrument", style: .default) { [weak self] _ in
            guard let this = self else {return }
            this.filterTableForOpened()
            
        })

        
        alert.addAction(UIAlertAction(title: "Show close instrument", style: .default) { [weak self] _ in
            guard let this = self else {return }
            this.filterTableForClosed()
        })
        
        alert.addAction(UIAlertAction(title: "Show all instrument", style: .default) { [weak self] _ in
            guard let this = self else {return }
            this.loadData(token:this.token)
        })
        
        alert.addAction(UIAlertAction(title: "Close", style: .cancel) { _ in
           
        })

        present(alert, animated: true)
    }
    
     func filterTableForOpened(){
         let searchParam = SearchList(status: 1)
         viewModel.filter(token: token, filter: searchParam)

     }
    
    func filterTableForClosed(){
        let searchParam = SearchList(status: 0)
        viewModel.filter(token: token, filter: searchParam)
    }
    
    func loadData(token:String) {
        viewModel.getInstrument(token: token).subscribe {[weak self] (instrument) in
            if instrument.data.isEmpty{
                self?.allInstrumentData.removeAll()
                self?.tableView.reloadData()
                self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No Instrument Available", secondText: "You currently do not have any subcriptions, go to the instrument list to subscribe.")
            }else{
                if let self = self{
                self.allInstrumentData.removeAll()
                    self.allInstrumentData.append(contentsOf: instrument.data)
                self.tableView.reloadData()
                self.tableView.showLoader()
                Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(InstrumentListViewController.removeLoader), userInfo: nil, repeats: false)
            }
            }
           
        } onError: { (error) in
            print("error: ",error)
        } onCompleted: {
            print("completed: ","completed")
        } onDisposed: {
           
            print("disposed: ","disposed")
        }.disposed(by: dispose)
        
    }
    
    
    @objc func removeLoader()
    {
        self.tableView.hideLoader()
    }
    
    
    
    func moveToVc(data:AllInstrument){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "accountselection") as? AccountSelectionViewController
        if let viewController = nextVc{
            viewController.subscribtionData = data
            self.navigationController?.pushViewController(viewController, animated: true)
        }
  
    }
    
   
    
}


extension InstrumentListViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        allInstrumentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InstrumentListTableViewCell
        let allData = allInstrumentData[indexPath.row]
        cell.dueDate.text = Utils.timeZoneForWallet2(date: allData.offer.enddate)
        cell.tenure.text = String(allData.offer.tenor)
        cell.applicationName.text  = allData.instrument.name
        
        if allData.offer.status == 0{
            cell.status.text  = "Closed"
            cell.status.textColor = UIColor(red: 0.78, green: 0.18, blue: 0.15, alpha: 1.00)
            cell.statusView.backgroundColor = UIColor(red: 0.96, green: 0.79, blue: 0.78, alpha: 1.00)
            cell.subscribe.setBackgroundImage((UIImage(named: "Rectangle 83")), for: .normal)
            
            cell.subscribe.isUserInteractionEnabled = false
        }else{
            cell.status.text  = "Open"
            cell.status.textColor =  UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
            cell.statusView.backgroundColor =  UIColor(red: 0.94, green: 0.98, blue: 0.95, alpha: 1.00)
            cell.subscribe.setBackgroundImage((UIImage(named: "Rectangle 85")), for: .normal)
            cell.subscribe.isUserInteractionEnabled = true
            
        }

        
        cell.subscribe.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        cell.subscribe.tag = indexPath.row
        
        
        return cell
    }
    
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        let item = allInstrumentData[buttonTag]
        getLastSub(item:item)
        
    }
    
    func getLastSub(item:AllInstrument){
        let userID = Utils.getUserId()
        viewModel.getExistingSub(token: token, userId: userID).subscribe { (data) in
            if data.statusCode == 200{
                if let data = data.data{
                   Utils.saveLastSubscriptionDetails(user: data)
                }
                self.moveToVc(data:item)
            }else{
                self.showAlert(message: data.statusMessage)
            }
        } onError: { (error) in
            print("error: ",error)
        } onCompleted: {
            print("completed: ","completed")
        } onDisposed: {
            
            print("disposed: ","disposed")
        }.disposed(by: dispose)
    }
}

extension InstrumentListViewController:ResponseDisplay{
    
    func result<T>(success: Bool, data: T?) {
        if let result = data as? InstrumentResponse{
            if result.status{
                if result.data.isEmpty{
                    createImage(tableView: (tableView)!, height: 200, width: 200, firstText: "No Instrument Found", secondText: "")
                }else{
                    allInstrumentData.removeAll()
                    allInstrumentData.append(contentsOf: result.data)
                    tableView.reloadData()
                }
            }
        }
    }
    
    
    func loading(loading: Bool) {
        if loading {
           tableView.showLoader()
        }else{
           self.tableView.hideLoader()
        }
    }
    
    
}
