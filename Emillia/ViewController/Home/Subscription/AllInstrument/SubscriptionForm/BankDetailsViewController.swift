//
//  BankDetailsViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import iOSDropDown
import RxSwift

class BankDetailsViewController: BaseViewController {
    
    
    @IBOutlet weak var accountNumber: UITextField!
    
    @IBOutlet weak var bankName: DropDown!
    
    @IBOutlet weak var accountName: UITextField!
    
    @IBOutlet weak var bvn: UITextField!
    
    @IBOutlet weak var bvnName: UITextField!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    
    let viewModel = SubscriptionViewModel()
    let token = Utils.getToken()
    var boxView = UIView()
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
 
    var allBanks = [Bank]()
    let dispose = DisposeBag()
    var bankCode = ""
    let progressHUD = ProgressHUD(text: "Fetching Data")
    let userdetails = Utils.getUserUserdetails()
    
    @IBOutlet weak var acctNo: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var bvnLabel: UILabel!
    
    @IBOutlet weak var instruction: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
                
        getBanks()
       // getSubDetails()
        bankName.isSearchEnable = true
        addAsterix(label: acctNo, text: "Account Number")
        addAsterix(label: bankLabel, text: "Bank")
        addAsterix(label: bvnLabel, text: "BVN")
       // viewModel.dataMapper.delegate = self
        configure()
        if submistionData?.businesstype == 1{
            
            if let fName = submistionData?.firstname, let lName = submistionData?.lastname{
                instruction.text = "Note that the certificate will be made payable in your name \(fName.uppercased() + " " + lName.uppercased()). If the name does not match that on your bank account, the bank may not honor payment."
            }
            
        }else{
            instruction.text = "Note that the certificate will be made payable in your name \(submistionData!.companyname?.uppercased() ?? ""). If the name does not match that on your bank account, the bank may not honor payment."
        }
        
       
        
        setDelegate(textField:accountName,accountNumber,bvn,bvnName,bankName)
        addBoader(textField:accountName,accountNumber,bvn,bvnName,bankName )
        bvn.addTarget(self, action: #selector(bvntextFieldDidChange), for: .editingChanged)
        accountNumber.addTarget(self, action: #selector(accountNumbertextFieldDidChange), for: .editingChanged)
        
        bankName.didSelect { [self] name, _, _ in
            for bank in allBanks{
                if name == bank.name{
                    bankCode = bank.code
                }
            }
            if accountNumber.text!.isEmpty{
                self.showAlert(message: "Enter Account Number")
            }else{
                let userData = AccountData(accountNumber: accountNumber.text!, bankCode: bankCode)
                viewModel.validateBankDetails(token: token, userAccount: userData)
            }
        }
        
    }
    
    
    @objc final private func bvntextFieldDidChange(textField: UITextField){
        if let field = textField.text{
            if field.count == 11 {
                clearError(uiLabel:errorMessage4)
                let userData = BVNData(bvn: field, firstName: submistionData!.firstname!, lastName: submistionData!.lastname!, middleName: "", accountNumber: accountNumber.text!, bankCode: bankCode)
                viewModel.validateBVN(token: token, user: userData)
                //viewModel.validateBvn(token: token, userAccount: userData)
            }else{
                setupErrorMessage(error: "BVN must be 11 digit", errorMessage: errorMessage4, isHidden: false, textField: bvn)
            }
        }
    }
    
  
    
    @objc final private func accountNumbertextFieldDidChange(textField: UITextField){
        if let field = textField.text{
                if field.count == 10{
                    clearError(uiLabel: errorMessage1)
                    if !bankName.text!.isEmpty{
                        clearError(uiLabel: errorMessage1,errorMessage2)
                        let userData = AccountData(accountNumber: accountNumber.text!, bankCode: bankCode)
                        viewModel.validateBankDetails(token: token, userAccount: userData)
                    }else{
                        
                        setupErrorMessage(error: "Select Bank", errorMessage: errorMessage2, isHidden: false, textField: bankName)
                        //self.showAlert(message: "Select bank")
                    }
                }else{
                    setupErrorMessage(error: "Account Number must be 10", errorMessage: errorMessage1, isHidden: false, textField: accountNumber)
                }
        }
    }
    
    
   
    
    
    func getBanks(){
        let banks = viewModel.getBanks()
        guard let givenBanks = banks else {return}
        self.allBanks = givenBanks
        var allBanks:[String] = []
        for bank in givenBanks{
            allBanks.append(bank.name)
        }
        bankName.optionArray = allBanks
        //allBanks.append(contentsOf: banks)
    }
    
 
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submit(_ sender: UIButton){
        if validateField(){
            submistionData?.bankacctno = accountNumber.text
            submistionData?.bankname = bankName.text
            submistionData?.bvnno = bvn.text
            
            moveToNext()
        }
    }
    
    
    
    func validateField()-> Bool{
        var value = true
        if accountNumber.text!.isEmpty {
            setupErrorMessage(error: "Account Number is Empty", errorMessage: errorMessage1, isHidden: false, textField: accountNumber)
            accountNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if bankName.text!.isEmpty {
            setupErrorMessage(error: "Select Bank", errorMessage: errorMessage2, isHidden: false, textField: bankName)
            bankName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        if bvn.text!.isEmpty {
            setupErrorMessage(error: "BVN is Empty", errorMessage: errorMessage4, isHidden: false, textField: bvn)
            bvn.layer.borderColor = UIColor.red.cgColor
            value = false
        }

        if bvnName.text!.isEmpty {
            setupErrorMessage(error: "BVN Name is Empty", errorMessage: errorMessage5, isHidden: false, textField: bvnName)
            bvnName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if accountName.text!.isEmpty {
            setupErrorMessage(error: "Account Name is Empty", errorMessage: errorMessage3, isHidden: false, textField: accountName)
            accountName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if accountName.text!.lowercased().contains("invalid"){
            setupErrorMessage(error: "Account is Invalid", errorMessage: errorMessage3, isHidden: false, textField: accountName)
            accountName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "instrumentdetails") as? InstrumentDetailsViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    
    func getSubDetails(){
        if let details =  Utils.getLastSubscriptionDetails(){
            for bank in allBanks{
                if details.bankname == bank.name{
                    bankCode = bank.code
                }
            }
            let userData = AccountData(accountNumber: details.bankacctno ?? "", bankCode: bankCode)
            viewModel.validateBankDetails(token: token, userAccount: userData)
        
            let userbvn = BVNData(bvn: details.bvnno ?? "", firstName: details.firstname ?? "", lastName: details.lastname ?? "", middleName: "", accountNumber: details.bankacctno ?? "", bankCode: bankCode)
           // viewModel.validateBvn(token: token, userAccount: userbvn)
            viewModel.validateBVN(token: token, user: userbvn)
            //accountName.text = details.bankacctno
          bvnName.text = Utils.getBVNName()
        accountName.text =  Utils.getAccountName()
        bankName.text  = details.bankname
        accountNumber.text  = details.bankacctno
        bvn.text  = details.bvnno
        //bvnName.text  = details.b
        }
    }
    
    private func configure(){
        viewModel.loading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            DispatchQueue.main.async {

                if loading {
                    this.view.addSubview(this.progressHUD)
                    
                }else{
                    this.progressHUD.removeFromSuperview()
                }
            }
        })
        .disposed(by: dispose)
        
        viewModel.validateBVNResponse.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] result in
            guard let this = self else{return}
            DispatchQueue.main.async {
                if (result.responseCode == "00") {
                    if result.firstName?.lowercased() == "valid" && result.lastName?.lowercased() == "valid"{
 
                        if let firstName = result.firstName , let lastname = result.lastName{
                            this.bvnName.text = firstName.lowercased() + " " + lastname.lowercased()
                            Utils.saveBVNName(bvnName:firstName.lowercased() + " " + lastname.lowercased())
                        }
                        
                        
                    }else{
                        if result.firstName?.lowercased() != "valid"{
                            this.setupErrorMessage(error: "BVN does Not match User's First Name", errorMessage: this.errorMessage4, isHidden: false, textField: this.bvn)
                            this.bvn.layer.borderColor = UIColor.red.cgColor
                        }
                        else if result.lastName?.lowercased() != "valid"{
                            this.setupErrorMessage(error: "BVN does Not match User's Last Name", errorMessage: this.errorMessage4, isHidden: false, textField: this.bvn)
                            this.bvn.layer.borderColor = UIColor.red.cgColor
                        }
                    }
                    
                }else{
                    this.setupErrorMessage(error: "Invalid BVN", errorMessage: this.errorMessage4, isHidden: false, textField: this.bvn)
                    this.bvn.layer.borderColor = UIColor.red.cgColor
                }
            }
        }).disposed(by: dispose)
        
        
        viewModel.validateBankResponse.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] result in
            guard let this = self else{return}
            DispatchQueue.main.async {
               
                this.accountName.text = result.replacingOccurrences(of: "\"", with: "")
                Utils.saveAccountName(accountName:result.replacingOccurrences(of: "\"", with: ""))
            }
        }).disposed(by: dispose)

        
        viewModel.error.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: dispose)
    }
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:accountName,accountNumber,bvn,bvnName,bankName)
    }
}


