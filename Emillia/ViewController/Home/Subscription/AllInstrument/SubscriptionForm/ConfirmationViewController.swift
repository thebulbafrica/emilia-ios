//
//  ConfirmationViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit

class ConfirmationViewController: BaseViewController {

    
    @IBOutlet weak var firstNmae: UILabel!
    
    @IBOutlet weak var lastName: UILabel!
    
    @IBOutlet weak var emailAddress: UILabel!
    
    @IBOutlet weak var phoneNumber: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var address2: UILabel!
    @IBOutlet weak var postCode: UILabel!
    
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var bank: UILabel!
    
    @IBOutlet weak var accountNumber: UILabel!
    
    @IBOutlet weak var numberOfUnit: UILabel!
    
    @IBOutlet weak var totalAmount: UILabel!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var secondTitle: UILabel!
    
    
    var unit = ""
    
    var submistionData: SubscriptionSubmissionData?
    var subscribtionData: AllInstrument?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if submistionData?.businesstype == 2{
            titleLabel.text = "Company Name"
            secondTitle.text = "RC Number"
            firstNmae.text = submistionData?.companyname
            lastName.text = submistionData?.rcno
        }else{
            titleLabel.text = "First Name"
            secondTitle.text = "Last Name"
            firstNmae.text = submistionData?.firstname
            lastName.text = submistionData?.lastname
        }
        
        emailAddress.text = submistionData?.email
        phoneNumber.text = submistionData?.phone
        address.text = submistionData?.address
        address2.text =  submistionData?.address2
        postCode.text = submistionData?.postcode
        country.text = submistionData?.country
        bank.text = submistionData?.bankname
        accountNumber.text = submistionData?.bankacctno
        numberOfUnit.text = unit
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let transactionAmount = numberFormatter.string(from: NSNumber(value: (submistionData?.amount)!))
        totalAmount.text = "₦" + String(transactionAmount!)
//        submistionData?.subamount = 0
//        submistionData?.totalamount = 0
//        submistionData?.title = ""
//        submistionData?.rcno = ""
//        submistionData?.contactperson = ""
//        submistionData?.companyname = ""
//        submistionData?.nextkinothername = ""
        
        

    }
    
    
    @IBAction func submit(_ sender: UIButton){
        moveToNext()
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "payment") as? PaymentDetailsViewController
        if let registerVC = nextVc{
            Utils.saveSubscriptionDetails(user: submistionData!)
            registerVC.subscribtionData = subscribtionData
            registerVC.submistionData = submistionData
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
   

}
