//
//  PaymentDetailsViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import RxSwift

class PaymentDetailsViewController: BaseViewController {

    var subscribtionData:AllInstrument?
    var submistionData:SubscriptionSubmissionData? = nil
    let progressHUD = ProgressHUD(text: "Fetching Data")
    let viewModel = SubscriptionViewModel()
    let userID = Utils.getUserId()
    let token = Utils.getToken()
    
    @IBOutlet weak var flutterWave: CardView!
    
    @IBOutlet weak var interSwitch: CardView!
    
    @IBOutlet weak var eTransact: CardView!
    
    let disposeBag = DisposeBag()
    let child = SpinnerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        flutterWave.isHidden = true
        interSwitch.isHidden = true
        eTransact.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func flutterWave(_ sender: UIButton) {
        
    }
    
    
    
    @IBAction func payStack(_ sender: UIButton) {
        let data: SubmitSubscriptionData? = SubmitSubscriptionData(offerid: subscribtionData?.offer.id, amount: submistionData?.amount, subamount: submistionData?.subamount, totalamount: submistionData?.totalamount, firstname: submistionData?.firstname, lastname: submistionData?.lastname, title: submistionData?.title, occupation: submistionData?.occupation, phone: submistionData?.phone, phoneUseForSub: submistionData?.phone, nextkinfname: submistionData?.nextkinfname, nextkinlname: submistionData?.nextkinlname, address: submistionData?.address, address2: submistionData?.address2, passportno: submistionData?.passportno, dob: submistionData?.dob, mmaidenname: submistionData?.mmaidenname, email: submistionData?.email, bankname: submistionData?.bankname, bankacctno: submistionData?.bankacctno, bvnno: submistionData?.bvnno, companyname: submistionData?.companyname, businesstype: submistionData?.businesstype, rcno: submistionData?.rcno, contactperson: submistionData?.contactperson, cscsno: submistionData?.cscsno, chnno: submistionData?.chnno, brokername: submistionData?.brokername, gender: submistionData?.gender, nextkinphone: submistionData?.nextkinphone, nextkinemail: submistionData?.nextkinemail, nextkinrelation: submistionData?.nextkinrelation, city: submistionData?.city, state: submistionData?.state, postcode: submistionData?.postcode, country: submistionData?.country, brokercode: submistionData?.brokercode, nextkinothername: submistionData?.nextkinothername)
        viewModel.subscribe(token: token, userId: userID, data: data)
    }
    
    
    @IBAction func interSwitch(_ sender: UIButton) {
    }
    
    @IBAction func etransact(_ sender: UIButton) {
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func moveToNext(refrence:String){
        print(Utils.getUserEmail())
        guard let amount = submistionData?.amount else { return }
        let callbackBody = GetCallbackBody(email: Utils.getUserEmail(), amount: "\(amount * 100)", currency: "NGN", reference: refrence, callbackURL: "http://thebulbafricaemilliauiapi-test.us-east-1.elasticbeanstalk.com/api/transactions/paystackwebhook")
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "card") as? PaymentViewController
        if let registerVC = nextVc{
            registerVC.subscribtionData = subscribtionData
            registerVC.submistionData = submistionData
            registerVC.refrence = refrence
            registerVC.callbackBody = callbackBody
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    private func configure(){
        viewModel.loading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            DispatchQueue.main.async {
                if loading{
                    this.showSpinnerView(child: this.child)
                }else{
                    this.removeSpinnerView(child: this.child)
                }
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.subscriptionValue.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] result in
            guard let this = self else{return}
            DispatchQueue.main.async {
                this.moveToNext(refrence: result)
            }
        }).disposed(by: disposeBag)

        
        viewModel.error.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
    }
}
