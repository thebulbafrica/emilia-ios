//
//  CHNValidateViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import MHLoadingButton

class CHNValidateViewController: BaseViewController {

    
    @IBOutlet weak var chnValue: UITextField!
    
    @IBOutlet weak var validate: LoadingButton!
    
    private let errorMessage1 = UILabel()
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    let viewModel = SubscriptionViewModel()
    let token = Utils.getToken()
    let lastSubData = Utils.getLastSubscriptionDetails()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        validate.indicator = MaterialLoadingIndicator(color: .white)
        // Do any additional setup after loading the view.
        
        let _ = Utils.getUserCscs()
        //chnValue.text = Utils.getUserChn() //lastSubData?.chnno //Utils.getUserChn()
    }
    
    @IBAction func validateBtn(_ sender: LoadingButton) {
        if validateField(){
            let chnNumber = chnValue.text!
            let dtatSubmit = ValidateChN(chn: chnNumber)
            viewModel.validateCHN(token: token, validate: dtatSubmit)
        }
    }
    
    func adjustView(){
        setDelegate(textField: chnValue)
        addBoader(textField: chnValue)
        chnValue.setLeftPaddingPoints(10)
        chnValue.setRightPaddingPoints(10)

    }

    func validateField()-> Bool{
        var value = true
        if chnValue.text!.isEmpty {
            setupErrorMessage(error: "CHN is Empty", errorMessage: errorMessage1, isHidden: false, textField: chnValue)
            chnValue.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        return value
    }
    
    
    func moveToNext(cscs:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "broker") as? AccountDetailsViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            submistionData?.chnno = chnValue.text!
            submistionData?.cscsno = cscs
            submistionData?.offerid = subscribtionData?.offer.id
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1)
        clearBorder(textField: chnValue)
    }
}

extension CHNValidateViewController:ResponseDisplay{
    func result<T>(success: Bool, data: T?) {
        if success {
           
            if let result = data as? ValidateChNResponse{
                if result.data.isEmpty{
                    self.showAlert(message: "Details not Available now")
                }else if result.data[0].response_code == 200{
                    guard let chn = result.data[0].chn, let cscs = result.data[0].cscs else {
                        moveToNext(cscs: "")
                        return
                    }
                    Utils.saveUserChnn(chn: chn)
                    Utils.saveUserCscs(cscs: cscs)
                    moveToNext(cscs: cscs)
                }else if result.data[0].response_code == 500{
                    self.showAlert(message: "Details not Available now")
                }else if result.data[0].response_message != nil {
                    self.showAlert(message: result.data[0].response_message!)
                }else{
                    self.showAlert(message: "Details not Available now")
                }
            }
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            validate.showLoader(userInteraction: true)
        }else{
            validate.hideLoader{
                
            }
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
