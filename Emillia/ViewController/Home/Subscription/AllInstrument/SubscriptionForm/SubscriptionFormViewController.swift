//
//  SubscriptionFormViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 02/03/2021.
//

import UIKit
import iOSDropDown
import SKCountryPicker
import FlagPhoneNumber

class SubscriptionFormViewController: BaseViewController {
    
    @IBOutlet var firstName:UITextField!
    @IBOutlet var LastName:UITextField!
    @IBOutlet var chn:UITextField!
    @IBOutlet var cscs:UITextField!
    @IBOutlet var dateOfBirth:UITextField!
    @IBOutlet var motherMadianName:UITextField!
    @IBOutlet var emailAddress:UITextField!
    @IBOutlet var phoneNumber:FPNTextField!
    @IBOutlet var gender:DropDown!
    @IBOutlet var occupation:UITextField!
    @IBOutlet var idCardNumber:UITextField!
   // @IBOutlet weak var countryCodes: UIStackView!
  //  @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var phoneStackView: UIStackView!
  //  @IBOutlet weak var countryCodeText: UILabel!
    
    @IBOutlet var taxIdCardNumber:UITextField!
    @IBOutlet var dateOfAccountOpen:UITextField!
    
    @IBOutlet weak var fnameLabel: UILabel!
    @IBOutlet weak var lNameLabel: UILabel!
    @IBOutlet weak var chnLabel: UILabel!
    @IBOutlet weak var dob: UILabel!
    @IBOutlet weak var mmadianName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    @IBOutlet weak var idNo: UILabel!
    
    @IBOutlet weak var taxIdLabel: UILabel!
    @IBOutlet weak var dteOfbankOpening: UILabel!
    
    @IBOutlet weak var taxIdStack: UIStackView!
    
    @IBOutlet weak var accountOpeningStack: UIStackView!
    
    @IBOutlet weak var chnStack: UIStackView!
    
    @IBOutlet weak var cscStack: UIStackView!
    

    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    private let errorMessage6 = UILabel()
    private let errorMessage7 = UILabel()
    private let errorMessage8 = UILabel()
    private let errorMessage9 = UILabel()
   
    
    let datePicker = UIDatePicker()
    let datePicker2 = UIDatePicker()
    var requiredGender = ["Male","Female"]
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    let userDetails = Utils.getUserUserdetails()
    
    var isValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
  

    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let sub = submistionData?.isChn{
            if sub{
                taxIdStack.isHidden = true
                accountOpeningStack.isHidden = true
                chnStack.isHidden = false
                cscStack.isHidden = false
            }else{
                
                taxIdStack.isHidden = false
                accountOpeningStack.isHidden = false
                chnStack.isHidden = true
                cscStack.isHidden = true
                //Utils.saveUserChnn(chn: "")
                //Utils.saveUserCscs(cscs: "")
            }
        }
        
        gender.optionArray = requiredGender
        addAsterix(label: fnameLabel, text: "First Name")
        addAsterix(label: lNameLabel, text: "Last Name")
        addAsterix(label: chnLabel, text: "CHN")
        addAsterix(label: dob, text: "Date of Birth")
        addAsterix(label: mmadianName, text: "Mothers Maiden Name")
        addAsterix(label: email, text: "Email Address")
        addAsterix(label: genderLabel, text: "Gender")
        addAsterix(label: occupationLabel, text: "Occupation")
        addAsterix(label: idNo, text: "Passport/Drivers License/National ID No")
        addAsterix(label: phone, text: "Phone")
        addAsterix(label: dteOfbankOpening, text: "Date of Bank Account creation")
        addAsterix(label: taxIdLabel, text: "Tax Identification Number")
        showDatePicker()
        showDatePicker2()
        getSubDetails()
        setDelegate(textField:firstName,LastName,emailAddress,phoneNumber,cscs,dateOfBirth,motherMadianName,emailAddress,gender,occupation,idCardNumber,taxIdCardNumber,dateOfAccountOpen)
        addBoader(textField:firstName,LastName,emailAddress,cscs,dateOfBirth,motherMadianName,emailAddress,gender,occupation,idCardNumber,taxIdCardNumber,dateOfAccountOpen)
        
        stackViewBoader(stackView: phoneStackView)
        let _ = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
       // countryCodes.addGestureRecognizer(tap)
        configurePhoneNumber()
    }
    
    func configurePhoneNumber(){
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.setFlag(countryCode: .NG)
        phoneNumber.hasPhoneNumberExample = true
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.displayMode = .list
        
        listController.setup(repository: phoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.phoneNumber.setFlag(countryCode: country.code)
        }

        phoneNumber.delegate = self
        setDelegate(textField: phoneNumber)
        
        phoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        phoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
          // handling code
          let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
             // self.countryCodeText.text = country.dialingCode
           // self.countryFlag.image = country.flag
          }
       
          countryController.detailColor = UIColor.red

      }
    
    func getSubDetails(){
        if let details =  Utils.getLastSubscriptionDetails(){
            dateOfBirth.text = details.dob
            motherMadianName.text  = details.mmaidenname
           // phoneNumber.text  = details.phone
            phoneNumber.set(phoneNumber: details.phone ?? "")
            gender.text  = details.gender
            occupation.text  = details.occupation
            idCardNumber.text  = details.passportno
            firstName.text = details.firstname
            LastName.text = details.lastname
            
            if let sub = submistionData?.isChn{
                if sub{
                    chn.text = details.chnno
                    cscs.text = details.cscsno
                }else{
                    chn.text = ""
                    cscs.text = ""
                }
            }
            emailAddress.text = details.email
        }else{
            firstName.text = userDetails?.firstname
            LastName.text = userDetails?.lastname
            if let sub = submistionData?.isChn{
                if sub{
                    chn.text = Utils.getUserChn()
                    cscs.text = Utils.getUserCscs()
                }else{
                    chn.text = ""
                    cscs.text = ""
                }
            }
            emailAddress.text = userDetails?.email
            if let no = userDetails?.phone{
                phoneNumber.set(phoneNumber: no)
            }
        }
        //Utils.getSubscriptionDetails()
        //guard let details = userDetails else{return}

    }

    
    @IBAction func continueBtn(_ sender: UIButton) {
        if validateField(){
            submistionData?.firstname = firstName.text!
            submistionData?.lastname = LastName.text!
            submistionData?.email = emailAddress.text!
            submistionData?.phone = phoneNumber.getFormattedPhoneNumber(format: .E164) ?? "" 
            submistionData?.chnno = chn.text!
            submistionData?.cscsno = cscs.text!
            submistionData?.dob = dateOfBirth.text!
            submistionData?.mmaidenname = motherMadianName.text!
            submistionData?.gender = gender.text!
            submistionData?.occupation = occupation.text!
            submistionData?.passportno = idCardNumber.text!
            
            
            submistionData?.bankOpeningDate = dateOfAccountOpen.text!
            submistionData?.taxId = taxIdCardNumber.text!
            moveToNext()
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validateField()-> Bool{
        var value = true
        
        if let sub = submistionData?.isChn{
            if sub{
                
                if dateOfBirth.text!.isEmpty {
                    setupErrorMessage(error: "Select Date of Birth", errorMessage: errorMessage1, isHidden: false, textField: dateOfBirth)
                    dateOfBirth.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if motherMadianName.text!.isEmpty {
                    setupErrorMessage(error: "Mother Maiden Name is Empty", errorMessage: errorMessage2, isHidden: false, textField: motherMadianName)
                    motherMadianName.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if emailAddress.text!.isEmpty {
                    setupErrorMessage(error: "Email Address is Empty", errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                    emailAddress.layer.borderColor = UIColor.red.cgColor
                    value = false
                }else{
                    do {
                        let _ = try self.emailAddress.validatedText(validationType: ValidatorType.email)
                        value = true
                    } catch(let error) {
                        setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                        value = false
                    }
                }
                
               
                
                if gender.text!.isEmpty {
                    setupErrorMessage(error: "Select Gender", errorMessage: errorMessage5, isHidden: false, textField: gender)
                    gender.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                

                if occupation.text!.isEmpty {
                    setupErrorMessage(error: "Occupation is Empty", errorMessage: errorMessage6, isHidden: false, textField: occupation)
                    occupation.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if idCardNumber.text!.isEmpty {
                    setupErrorMessage(error: "Enter Any Goverment ID Card Number", errorMessage: errorMessage7, isHidden: false, textField: idCardNumber)
                    idCardNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                else if !isValid{
                    print("value",isValid)
                    setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
            else{
                if dateOfBirth.text!.isEmpty {
                    setupErrorMessage(error: "Select Date of Birth", errorMessage: errorMessage1, isHidden: false, textField: dateOfBirth)
                    dateOfBirth.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if motherMadianName.text!.isEmpty {
                    setupErrorMessage(error: "Mother Maiden Name is Empty", errorMessage: errorMessage2, isHidden: false, textField: motherMadianName)
                    motherMadianName.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if emailAddress.text!.isEmpty {
                    setupErrorMessage(error: "Email Address is Empty", errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                    emailAddress.layer.borderColor = UIColor.red.cgColor
                    value = false
                }else{
                    do {
                        let _ = try self.emailAddress.validatedText(validationType: ValidatorType.email)
                        value = true
                    } catch(let error) {
                        setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                        value = false
                    }
                }
                
               
                
                if gender.text!.isEmpty {
                    setupErrorMessage(error: "Select Gender", errorMessage: errorMessage5, isHidden: false, textField: gender)
                    gender.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                

                if occupation.text!.isEmpty {
                    setupErrorMessage(error: "Occupation is Empty", errorMessage: errorMessage6, isHidden: false, textField: occupation)
                    occupation.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if idCardNumber.text!.isEmpty {
                    setupErrorMessage(error: "Enter Any Goverment ID Card Number", errorMessage: errorMessage7, isHidden: false, textField: idCardNumber)
                    idCardNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                else if !isValid{
                    print("value",isValid)
                    setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                if taxIdCardNumber.text!.isEmpty {
                    setupErrorMessage(error: "Tax ID is Empty", errorMessage: errorMessage8, isHidden: false, textField: taxIdCardNumber)
                    taxIdCardNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if dateOfAccountOpen.text!.isEmpty {
                    setupErrorMessage(error: "Select Date Bank Account was Opened", errorMessage: errorMessage9, isHidden: false, textField: dateOfAccountOpen)
                    dateOfAccountOpen.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
        }
        
       
        return value
       
    }
  
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "address") as? AddressSubscriptionViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
  
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5,errorMessage6,errorMessage7,errorMessage8,errorMessage9)
        clearBorder(textField: firstName,LastName,emailAddress,phoneNumber,cscs,dateOfBirth,motherMadianName,emailAddress,gender,occupation,idCardNumber,taxIdCardNumber,dateOfAccountOpen)
    }
    
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton =  UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePicker.maximumDate = Date()
        // add toolbar to textField
        dateOfBirth.inputAccessoryView = toolbar
        // add datepicker to textField
        dateOfBirth.inputView = datePicker
        
    }
    
    
    func showDatePicker2(){
        //Formate Date
        datePicker2.datePickerMode = .date
        datePicker2.preferredDatePickerStyle = .wheels
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donedatePicker2))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton =  UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePicker2.maximumDate = Date()
        // add toolbar to textField
        dateOfAccountOpen.inputAccessoryView = toolbar
        // add datepicker to textField
        dateOfAccountOpen.inputView = datePicker
        
    }
 
 
 
 
 @objc func donedatePicker(){
  //For date formate
   let formatter = DateFormatter()
   formatter.dateFormat = "yyyy-MM-dd"
   dateOfBirth.text = formatter.string(from: datePicker.date)
   //dismiss date picker dialog
   self.view.endEditing(true)
    }
    
    @objc func donedatePicker2(){
     //For date formate
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
    dateOfAccountOpen.text = formatter.string(from: datePicker2.date)
      //dismiss date picker dialog
      self.view.endEditing(true)
       }
 
 @objc func cancelDatePicker(){
      //cancel button dismiss datepicker dialog
       self.view.endEditing(true)
     }
    
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
    
}



extension SubscriptionFormViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5,errorMessage6,errorMessage7,errorMessage8,errorMessage9)
        clearBorder(textField: firstName,LastName,emailAddress,phoneNumber,cscs,dateOfBirth,motherMadianName,emailAddress,gender,occupation,idCardNumber,taxIdCardNumber,dateOfAccountOpen)
     
//        print(
//            isValid,
//            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
//            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
//            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
//            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
//            textField.getRawPhoneNumber() ?? "Raw: nil"
//        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}
