//
//  AccountDetailsViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import RxSwift
import ListPlaceholder


class AccountDetailsViewController: BaseViewController{
    
    @IBOutlet weak var tableView: UITableView!
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    var viewModel = SubscriptionViewModel()
    let dispose = DisposeBag()
    var data = [BrokersData]()
    var updatedData = [NewBrokersData]()
    var brokerName = ""
    var brokerCode = ""
    var token = Utils.getToken()

    var rowsWhichAreChecked = [IndexPath]()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        getBroker(token: token)
       
    }
    
    
    func getBroker(token:String){
        viewModel.getBroker(token: token)
            .subscribe {[weak self] (brokerResponse) in
                
                if brokerResponse.data.isEmpty{
                    self?.data.removeAll()
                    self?.tableView.reloadData()
                    self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No Instrument Available", secondText: "You currently do not have any subcriptions, go to the instrument list to subscribe.")
                }else{
                    if let self = self{
                    self.data.removeAll()
                        brokerResponse.data.forEach { (brkData) in
                            let newOpt = NewBrokersData(id: brkData.id, name: brkData.name, code: brkData.code, check: false)
                            self.updatedData.append(newOpt)
                        }
                        self.data.append(contentsOf: brokerResponse.data)
                    self.tableView.reloadData()
                    self.tableView.showLoader()
                    Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(AccountDetailsViewController.removeLoader), userInfo: nil, repeats: false)
                }
                }
            } onError: { (error) in
                
            } onCompleted: {
                
            } onDisposed: {
                
            }.disposed(by: dispose)

    }
    
    @objc func removeLoader()
    {
        self.tableView.hideLoader()
    }
    

    @IBAction func yesBtn(_ sender: SSRadioButton) {
        
    }
    
    @IBAction func continueBtn(_ sender: UIButton) {
        if brokerName.isEmpty{
            showToast(message: "Select Broker", font: .systemFont(ofSize: 10))
        }else{
            submistionData?.brokername = brokerName
            submistionData?.brokercode = brokerCode
            moveToNext()
        }
    }
    
    func moveToNext(){
        
        if submistionData?.businesstype == 1{
            let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "personalInfo") as? SubscriptionFormViewController
            if let registerVC = nextVc{
                registerVC.submistionData = submistionData
                registerVC.subscribtionData = subscribtionData
                self.navigationController?.pushViewController(registerVC, animated: true)
            }
        }else if submistionData?.businesstype == 2{
            let nextVc2 = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CompanyInfo") as? CompanySubscriptionFormViewController
            if let registerVC = nextVc2{
                registerVC.submistionData = submistionData
                registerVC.subscribtionData = subscribtionData
                self.navigationController?.pushViewController(registerVC, animated: true)
            }
        }
        
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}


extension AccountDetailsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "brokercell", for: indexPath) as! IssuringHouseTableViewCell
        let allData = updatedData[indexPath.row]
        cell.title.text = allData.name + "|" + allData.code
        cell.checkBoxBtn.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        cell.checkBoxBtn.tag = indexPath.row
  
        if allData.check!{
            cell.checkBoxBtn.isChecked = true
        }else{
            cell.checkBoxBtn.isChecked = false
        }
        return cell
    }
    
    
    @objc func connected(sender: UIButton){
      
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        print("selected")
        //let cell = tableView.cellForRow(at: indexPath) as! SegmentTableViewCell
        let allOptions = updatedData[indexPath.row]
        
     
        var i = 0
        while i < updatedData.count {
            if(updatedData[i].check!){
                updatedData[i].check = false
            }
            i += 1
        }
        
        
        var j = 0
        while j < data.count {
            if(data[j].id == allOptions.id){
                updatedData[j].check =  !updatedData[j].check!
                // check = true
                // cell.isShown = true
            }
            j += 1
        }
        brokerName = allOptions.name
        brokerCode = allOptions.code
        print(allOptions)
        tableView.reloadData()
    }
    
}

extension AccountDetailsViewController:ResponseDisplay{
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        
    }
    
    
}
