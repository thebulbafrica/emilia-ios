//
//  NextofKinViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//

import UIKit
import SKCountryPicker
import FlagPhoneNumber

class NextofKinViewController: BaseViewController {

    @IBOutlet var nextOfKinFirstName:UITextField!
    @IBOutlet var nextOfKinLasttName:UITextField!
    @IBOutlet var nextOfKinRelationship:UITextField!
    @IBOutlet var nextOfKinPhoneNumber:FPNTextField!
    @IBOutlet var nextOfKinEmailAddress:UITextField!
   // @IBOutlet weak var countryCodes: UIStackView!
   // @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var phoneStackView: UIStackView!
   // @IBOutlet weak var countryCodeText: UILabel!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
  
    
    
    @IBOutlet weak var fname: UILabel!
    @IBOutlet weak var lName: UILabel!
    @IBOutlet weak var relationship: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    var isValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSubDetails()
        addAsterix(label: fname, text: "First Name")
        addAsterix(label: lName, text: "Last Name")
        addAsterix(label: relationship, text: "Relationship")
        addAsterix(label: phoneLabel, text: "Phone")
        addAsterix(label: emailLabel, text: "Email Address")
        getSubDetails()
          setDelegate(textField:nextOfKinFirstName ,nextOfKinLasttName,nextOfKinRelationship,nextOfKinPhoneNumber,nextOfKinEmailAddress)
          addBoader(textField:nextOfKinFirstName ,nextOfKinLasttName,nextOfKinRelationship,nextOfKinEmailAddress)
        stackViewBoader(stackView: phoneStackView)
        let _ = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
       // countryCodes.addGestureRecognizer(tap)
    }
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }
    func configurePhoneNumber(){
        nextOfKinPhoneNumber.borderStyle = .roundedRect
        nextOfKinPhoneNumber.setFlag(countryCode: .NG)
        nextOfKinPhoneNumber.hasPhoneNumberExample = true
        nextOfKinPhoneNumber.placeholder = "Phone Number"
        nextOfKinPhoneNumber.displayMode = .list
        
        listController.setup(repository: nextOfKinPhoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.nextOfKinPhoneNumber.setFlag(countryCode: country.code)
        }

        nextOfKinPhoneNumber.delegate = self
        setDelegate(textField: nextOfKinPhoneNumber)
        
        nextOfKinPhoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        nextOfKinPhoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

@objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
      // handling code
      let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
         // self.countryCodeText.text = country.dialingCode
       // self.countryFlag.image = country.flag
      }
   
      countryController.detailColor = UIColor.red

  }
    
    @IBAction func backbutton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continueBtn(_ sender: UIButton) {
        if validateField(){
            submistionData?.nextkinfname = nextOfKinFirstName.text!
            submistionData?.nextkinlname = nextOfKinLasttName.text!
            submistionData?.nextkinemail = nextOfKinEmailAddress.text!
            submistionData?.nextkinphone = nextOfKinPhoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
            submistionData?.nextkinrelation = nextOfKinRelationship.text!
            moveToNext()
        }
    }
    
    func getSubDetails(){
        if let details =  Utils.getLastSubscriptionDetails(){
        nextOfKinFirstName.text = details.nextkinfname
        nextOfKinLasttName.text = details.nextkinlname
        nextOfKinRelationship.text = details.nextkinrelation
        nextOfKinPhoneNumber.text = details.nextkinphone
            nextOfKinPhoneNumber.set(phoneNumber: details.phone ?? "")
        nextOfKinEmailAddress.text = details.nextkinemail
        }
    }
    
    
    func validateField()-> Bool{
        var value = true
        if nextOfKinFirstName.text!.isEmpty {
            setupErrorMessage(error: "Next of kin First Name Empty", errorMessage: errorMessage1, isHidden: false, textField: nextOfKinFirstName)
            nextOfKinFirstName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if nextOfKinLasttName.text!.isEmpty {
            setupErrorMessage(error: "Next of kin Last Name Empty", errorMessage: errorMessage2, isHidden: false, textField: nextOfKinLasttName)
            nextOfKinLasttName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if nextOfKinEmailAddress.text!.isEmpty {
            setupErrorMessage(error: "Email Address is Empty", errorMessage: errorMessage3, isHidden: false, textField: nextOfKinEmailAddress)
            nextOfKinEmailAddress.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            do {
                let _ = try self.nextOfKinEmailAddress.validatedText(validationType: ValidatorType.email)
                value = true
            } catch(let error) {
                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage3, isHidden: false, textField: nextOfKinEmailAddress)
                value = false
            }
        }
        
        if nextOfKinPhoneNumber.text!.isEmpty {
            setupErrorMessage(error: "Next of Kin Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: nextOfKinPhoneNumber)
            nextOfKinPhoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if nextOfKinRelationship.text!.isEmpty {
            setupErrorMessage(error: "Relationship with Next of Kin is Empty", errorMessage: errorMessage5, isHidden: false, textField: nextOfKinRelationship)
            nextOfKinRelationship.layer.borderColor = UIColor.red.cgColor
            value = false
        }
    
        return value
    }
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "bankdetails") as? BankDetailsViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField: nextOfKinFirstName ,nextOfKinLasttName,nextOfKinRelationship,nextOfKinPhoneNumber,nextOfKinEmailAddress)
    }
}


extension NextofKinViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField: nextOfKinFirstName ,nextOfKinLasttName,nextOfKinRelationship,nextOfKinPhoneNumber,nextOfKinEmailAddress)
//        print(
//            isValid,
//            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
//            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
//            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
//            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
//            textField.getRawPhoneNumber() ?? "Raw: nil"
//        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}
