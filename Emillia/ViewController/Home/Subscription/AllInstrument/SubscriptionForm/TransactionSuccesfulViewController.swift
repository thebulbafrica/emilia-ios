//
//  TransactionSuccesfulViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//

import UIKit

class TransactionSuccesfulViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func goToDashBoard(_ sender: UIButton) {
        moveToVc()
    }

    func moveToVc(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "tabViewController")
        self.navigationController?.pushViewController(nextVc, animated: true)
       
        //tabViewController
    }

}
