//
//  AddressSubscriptionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//

import UIKit
import iOSDropDown
import RxSwift

class AddressSubscriptionViewController: BaseViewController {

    
    @IBOutlet var address1:UITextField!
    @IBOutlet var address2:UITextField!
    @IBOutlet var city:DropDown!
    @IBOutlet var state:DropDown!
    @IBOutlet var postcode:UITextField!
    @IBOutlet var country:DropDown!
    
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
 
    
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    
    let viewModel = SubscriptionViewModel()
    let token = Utils.getToken()
    let dispose = DisposeBag()
    
    var countryID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(submistionData)
        
        
        city.isSearchEnable = true
        state.isSearchEnable = true
        country.isSearchEnable = true
        getSubDetails()
        getCountry()
        addAsterix(label: address, text: "Address 1")
        addAsterix(label: cityLabel, text: "City")
        addAsterix(label: stateLabel, text: "State")
        addAsterix(label: postLabel, text: "Postcode")
        addAsterix(label: countryLabel, text: "Country")
    
        
        setDelegate(textField:address1,address2,city,state,postcode,country)
          addBoader(textField:address1,address2,city,state,postcode,country)

       
        country.didSelect { [self] (country, _, _) in
            getStatesFromCountry(selectedCountry: country)
        }
        
        state.didSelect { [self] (state, _, _) in
            getCityFromState(selectedCity: state, countryId: countryID)
        }

        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
 
    @IBAction func aontinueBtn(_ sender: UIButton) {
        if validateField(){
            submistionData?.address = address1.text!
            submistionData?.address2 = address2.text!
            submistionData?.city = city.text!
            submistionData?.state = state.text!
            submistionData?.postcode = postcode.text!
            submistionData?.country = country.text!
            moveToNext()
        }
    }
    
    func validateField()-> Bool{
        var value = true
        if address1.text!.isEmpty {
            setupErrorMessage(error: "Enter Address", errorMessage: errorMessage1, isHidden: false, textField: address1)
            address1.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if city.text!.isEmpty {
            setupErrorMessage(error: "Select City", errorMessage: errorMessage2, isHidden: false, textField: city)
            city.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if state.text!.isEmpty {
            setupErrorMessage(error: "Select State", errorMessage: errorMessage3, isHidden: false, textField: state)
            state.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if postcode.text!.isEmpty {
            setupErrorMessage(error: "Postcode is Empty", errorMessage: errorMessage4, isHidden: false, textField: postcode)
            postcode.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if country.text!.isEmpty {
            setupErrorMessage(error: "Select Countru", errorMessage: errorMessage5, isHidden: false, textField: country)
            country.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "nextofkin") as? NextofKinViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
 
    
    func getCountry() {
        viewModel.getAllCountry(token: token).subscribe {[weak self] (allCountry) in
           // self?.country.optionArray = allCountry
            var countryList = [String]()
            allCountry.forEach { (countries) in
                countryList.append(countries.name)
            }
            self?.country.optionArray = countryList
        } onError: { (error) in
            print(error)
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)

    }
    
    
    func getStatesFromCountry(selectedCountry:String){
        viewModel.getAllCountry(token: token).subscribe {[weak self] (allCountry) in
            allCountry.forEach { (countries) in
                if countries.name == selectedCountry{
                    self?.getState(id: countries.id)
                    self?.countryID = countries.id
                }
            }
        } onError: { (error) in
            print(error)
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)
    }
    
    
    func getState(id:Int){
        viewModel.getAllState(token: token, countryId: id).subscribe {[weak self] (allState) in
            var stateList = [String]()
            allState.forEach { (states) in
                stateList.append(states.name)
            }
            self?.state.optionArray = stateList
        } onError: { (error) in
            
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)

    }
    
    
    func getCityFromState(selectedCity:String,countryId:Int){
        
        viewModel.getAllState(token: token, countryId: countryId).subscribe {[weak self] (allState) in
            
            allState.forEach { (states) in
                if states.name == selectedCity{
                    self?.getCity(stateId: states.id)
                }
            }
            
        } onError: { (error) in
            
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)
    }
    
    func getCity(stateId:Int){
        viewModel.getAllCity(token: token, stateId: stateId).subscribe {[weak self](allCity) in
            var cityList = [String]()
            allCity.forEach { (cities) in
                cityList.append(cities.name)
            }
            self?.city.optionArray = cityList
        } onError: { (error) in
            
        } onCompleted: {
            
        } onDisposed: {
            
        }.disposed(by: dispose)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField: address1,address2,city,state,postcode,country)
    }
    
    func getSubDetails(){
        if let details =  Utils.getLastSubscriptionDetails(){
            address1.text = details.address
            address2.text = details.address2
            country.text = details.country
            state.text = details.state
            city.text = details.city
            postcode.text = details.postcode
        }
    }
    
    //
    //    func getState(selectedCountry:String)-> [String]{
    //        let allCountries = AllCountriesAndState.allcountries()
    //        var stateList = [String]()
    //        if let country = allCountries{
    //            country.forEach { count in
    //                if count.countryName == selectedCountry{
    //                    count.states.forEach { st in
    //                        stateList.append(st.stateName)
    //                    }
    //                }
    //            }
    //        }
    //        return stateList
    //    }
    //
    //
    //    func getCountry() -> [String]{
    //        let allCountries = AllCountriesAndState.allcountries()
    //        var countryList = [String]()
    //        if let country = allCountries{
    //            country.forEach { count in
    //                countryList.append(count.countryName)
    //            }
    //        }
    //        return countryList
    //    }
    //    func getCity(selectedCountry:String,selectedState:String)-> [String]{
    //        let allCountries = AllCountriesAndState.allcountries()
    //        var stateCity = [String]()
    //        if let country = allCountries{
    //            country.forEach { count in
    //                if count.countryName == selectedCountry{
    //                    count.states.forEach { st in
    //                        if st.stateName == selectedState{
    //                            count.states.forEach { st in
    //                                stateCity.append(contentsOf: st.cities)
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        return stateCity
    //    }
    //
    //
    
    
}
