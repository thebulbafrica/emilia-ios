//
//  PaymentViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//

import UIKit
import Paystack
import MHLoadingButton
import RxSwift
import WebKit

class PaymentViewController: BaseViewController, PSTCKPaymentCardTextFieldDelegate,UITextViewDelegate  {
    
    //@IBOutlet weak var cardNumber: UITextField!
    
    //  @IBOutlet weak var expiringDate: UITextField!
    
    
    @IBOutlet weak var cardNo:UITextField!
    @IBOutlet weak var expiringDate:UITextField!
    @IBOutlet weak var cvv:UITextField!
    @IBOutlet weak var paystackStackView: UIStackView!
    
    private var previousTextFieldContent: String?
    @IBOutlet weak var webView: WKWebView!
    private var previousSelection: UITextRange?
    
    // @IBOutlet weak var cvv: UITextField!
    @IBOutlet weak var proceedButton: UIButton!
    
    @IBOutlet weak var lblTerms: UILabel!
    var subscribtionData:AllInstrument?
    var submistionData:SubscriptionSubmissionData? = nil
    var callbackBody: GetCallbackBody?
    
    var refrence = ""
    let dispose = DisposeBag()
    let callbackUrl = "http://thebulbafricaemilliauiapi-test.us-east-1.elasticbeanstalk.com/api/transactions/paystackwebhook"
    
    let paymentTextField = PSTCKPaymentCardTextField()
    
    let viewModel = SubscriptionViewModel()
    let userID = Utils.getUserId()
    let token = Utils.getToken()
    // let paymentTextField = PSTCKPaymentCardTextField()
    var btnTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paystackStackView.isHidden = true
        expiringDate.delegate = self
        proceedButton.alpha = 0.4
        proceedButton.isUserInteractionEnabled = false
        viewModel.dataMapper.delegate = self
        configure()
        getPaymentLink()
        //proceedButton.indicator = MaterialLoadingIndicator(color: .white)
        // Do any additional setup after loading the view.
        cardNo.addTarget(self, action: #selector(reformatAsCardNumber), for: .editingChanged)
        
        cvv.addTarget(self, action: #selector(checkCvv), for: .editingChanged)
        let amount = submistionData?.amount
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        let transactionAmount = numberFormatter.string(from: NSNumber(value:amount ?? Int(0.0)))
        btnTitle = "Pay NGN \(transactionAmount!)"
        
        proceedButton.setTitle(btnTitle, for: .normal)
        
        let text = "By selecting the button below, you agree to the Terms & Conditions and agreement of the Debt Management Office."
        let linkTextWithColor = "Terms & Conditions"
        
        let range = (text as NSString).range(of: linkTextWithColor)
        
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)
        
        lblTerms.attributedText = attributedString
        
        
        lblTerms.isUserInteractionEnabled = true
        lblTerms.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        lblTerms.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = lblTerms.text else { return }
        let numberRange = (text as NSString).range(of: "Terms & Conditions")
        print(numberRange)
        let emailRange = (text as NSString).range(of: "Terms & Conditions")
        if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: numberRange) {
            print("Email tapped")
            guard let url = URL(string:  "http://www.subscribedmo.herokuapp.com/terms") else {
                return //be safe
            }
            
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            
        } else if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: emailRange) {
            print("Email tappedsss")
        }else{
            print("yea ")
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
    
    
    func paymentCardTextFieldDidChange(_ textField: PSTCKPaymentCardTextField) {
        if textField.isValid{
            
        }
        // Toggle navigation, for example
        // saveButton.enabled = textField.isValid
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "success") as? TransactionSuccesfulViewController
        if let registerVC = nextVc{
            
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    
    @IBAction func proceed(_ sender: LoadingButton) {
        performTransaction()
        //performTransaction()
    }
    
    func getPaymentLink() {
        guard let callbackBody = callbackBody else {
            return
        }
        viewModel.getPaymentLink(token: Utils.getToken(), data: callbackBody)
    }
    
    func configure() {
        viewModel.callbackResponse.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] result in
            let urlRequest = URLRequest.init(url: URL.init(string: result.authorizationURL ?? "")!)
            self?.webView.load(urlRequest)
            self?.webView.navigationDelegate = self
        }).disposed(by: dispose)
    }
    
    func performTransaction(){
        if(validateField()){
            
            let num = cardNo.text?.replacingOccurrences(of: " ", with: "")
            let date = expiringDate.text!
            let cardCvv = cvv.text!
            let newDate = date.split(separator: "/")
            let month = UInt(newDate[0])
            let year = UInt(newDate[1])
            
            let amount = (submistionData?.amount)! * 100
            
            loadingIndicator(load: true, btnTitle: btnTitle, btn: proceedButton)
            
            let cardParams = paymentTextField.cardParams as PSTCKCardParams
            cardParams.number = num
            cardParams.cvc = cardCvv
            cardParams.expYear = year!
            cardParams.expMonth = month!
            
            let transactionParams = PSTCKTransactionParams.init();
            
            // building new Paystack Transaction
            
            transactionParams.amount = UInt(amount);
            if let email = submistionData?.email{
                transactionParams.email = email
            }
            
            transactionParams.reference = refrence
            
            
            PSTCKAPIClient.shared().chargeCard(cardParams, forTransaction: transactionParams, on: self,didEndWithError: { (error, reference) -> Void in
                //Alert Error
                self.loadingIndicator(load: false, btnTitle: self.btnTitle, btn: self.proceedButton)
                
                
                self.showAlert(message: "Transaction Failed")
                print(error.localizedDescription)
                
            }, didRequestValidation: { (reference) -> Void in
                print("didRequestValidation::\(reference)")
                print("didRequestValidationpassRef::\(self.refrence)")
                // an OTP was requested, transaction has not yet succeeded
            }, didTransactionSuccess: { [self] (reference) -> Void in
                print("successRef::\(reference)")
                print("passRef::\(self.refrence)")
                viewModel.getPayment(token: token, transRef: self.refrence).subscribe { result in
                    if result.status{
                        moveToNext()
                    }
                } onError: { (error) in
                    print(error)
                    loadingIndicator(load: false, btnTitle: btnTitle, btn: proceedButton)
                } onCompleted: {
                    loadingIndicator(load: false, btnTitle: btnTitle, btn: proceedButton)
                } onDisposed: {
                    loadingIndicator(load: false, btnTitle: btnTitle, btn: proceedButton)
                }.disposed(by: dispose)
                
                //self.tabBarController?.selectedIndex = 1
            })
        }
    }
    
    
    func validateField() -> Bool{
        let num = cardNo.text!
        let date = expiringDate.text!
        let cardCvv = cvv.text!
        if(num.isEmpty && date.isEmpty && cardCvv.isEmpty){
            return false
        }else{
            
            return true
        }
        
    }
}


extension PaymentViewController{
    
    @objc func checkCvv(textField: UITextField) {
        if textField.text!.count >= 3{
            proceedButton.alpha = 1.0
            proceedButton.isUserInteractionEnabled = true
        }
    }
    
    @objc func reformatAsCardNumber(textField:UITextField){
        let formatter = CreditCardFormatter()
        let isAmex = false
        formatter.formatToCreditCardNumber(isAmex: isAmex, textField: textField, withPreviousTextContent: textField.text, andPreviousCursorPosition: textField.selectedTextRange)
        if let text = textField.text?.replacingOccurrences(of: " ", with: ""){
            let imageView = UIImageView();
            let image = CardValidator.getCardType(number:text ).icon
            imageView.image = image;
            textField.rightView = imageView
            textField.rightViewMode = .always
            let checkIsCardValid = CardValidator.luhnCheck(text)
            if checkIsCardValid{
                textField.setCornerBorder(color: .clear, cornerRadius: 0, borderWidth: 0)
            }else{
                proceedButton.alpha = 0.4
                proceedButton.isUserInteractionEnabled = false
                textField.setCornerBorder(color: .red, cornerRadius: 0, borderWidth: 1)
            }
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
    }
    
    
}

extension PaymentViewController:ResponseDisplay {
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        self.loadingIndicator(load: loading, btnTitle: btnTitle, btn: self.proceedButton)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.length > 0 {
            return true
        }
        if string == "" {
            return false
        }
        if range.location > 4 {
            return false
        }
        var originalText = textField.text
        let replacementText = string.replacingOccurrences(of: " ", with: "")
        
        //Verify entered text is a numeric value
        if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
            return false
        }
        
        //Put / after 2 digit
        if range.location == 2 {
            originalText?.append("/")
            textField.text = originalText
        }
        return true
    }
    
    
}


extension String {
    
    func containsOnlyDigits() -> Bool
    {
        
        let notDigits = NSCharacterSet.decimalDigits.inverted
        
        if rangeOfCharacter(from: notDigits, options: String.CompareOptions.literal, range: nil) == nil
        {
            return true
        }
        
        return false
    }
}


var creditCardFormatter : CreditCardFormatter
{
    return CreditCardFormatter.sharedInstance
}

class CreditCardFormatter : NSObject
{
    static let sharedInstance : CreditCardFormatter = CreditCardFormatter()
    
    func formatToCreditCardNumber(isAmex: Bool, textField : UITextField, withPreviousTextContent previousTextContent : String?, andPreviousCursorPosition previousCursorSelection : UITextRange?) {
        var selectedRangeStart = textField.endOfDocument
        if textField.selectedTextRange?.start != nil {
            selectedRangeStart = (textField.selectedTextRange?.start)!
        }
        if  let textFieldText = textField.text
        {
            var targetCursorPosition : UInt = UInt(textField.offset(from:textField.beginningOfDocument, to: selectedRangeStart))
            let cardNumberWithoutSpaces : String = removeNonDigitsFromString(string: textFieldText, andPreserveCursorPosition: &targetCursorPosition)
            if cardNumberWithoutSpaces.count > 19
            {
                textField.text = previousTextContent
                textField.selectedTextRange = previousCursorSelection
                return
            }
            var cardNumberWithSpaces = ""
            if isAmex {
                cardNumberWithSpaces = insertSpacesInAmexFormat(string: cardNumberWithoutSpaces, andPreserveCursorPosition: &targetCursorPosition)
            }
            else
            {
                cardNumberWithSpaces = insertSpacesIntoEvery4DigitsIntoString(string: cardNumberWithoutSpaces, andPreserveCursorPosition: &targetCursorPosition)
            }
            textField.text = cardNumberWithSpaces
            if let finalCursorPosition = textField.position(from:textField.beginningOfDocument, offset: Int(targetCursorPosition))
            {
                textField.selectedTextRange = textField.textRange(from: finalCursorPosition, to: finalCursorPosition)
            }
        }
    }
    
    func removeNonDigitsFromString(string : String, andPreserveCursorPosition cursorPosition : inout UInt) -> String {
        var digitsOnlyString : String = ""
        for index in stride(from: 0, to: string.count, by: 1)
        {
            let charToAdd : Character = Array(string)[index]
            if isDigit(character: charToAdd)
            {
                digitsOnlyString.append(charToAdd)
            }
            else
            {
                if index < Int(cursorPosition)
                {
                    cursorPosition -= 1
                }
            }
        }
        return digitsOnlyString
    }
    
    private func isDigit(character : Character) -> Bool
    {
        return "\(character)".containsOnlyDigits()
    }
    
    func insertSpacesInAmexFormat(string : String, andPreserveCursorPosition cursorPosition : inout UInt) -> String {
        var stringWithAddedSpaces : String = ""
        for index in stride(from: 0, to: string.count, by: 1)
        {
            if index == 4
            {
                stringWithAddedSpaces += " "
                if index < Int(cursorPosition)
                {
                    cursorPosition += 1
                }
            }
            if index == 10 {
                stringWithAddedSpaces += " "
                if index < Int(cursorPosition)
                {
                    cursorPosition += 1
                }
            }
            if index < 15 {
                let characterToAdd : Character = Array(string)[index]
                stringWithAddedSpaces.append(characterToAdd)
            }
        }
        return stringWithAddedSpaces
    }
    
    
    func insertSpacesIntoEvery4DigitsIntoString(string : String, andPreserveCursorPosition cursorPosition : inout UInt) -> String {
        var stringWithAddedSpaces : String = ""
        for index in stride(from: 0, to: string.count, by: 1)
        {
            if index != 0 && index % 4 == 0 && index < 19
            {
                stringWithAddedSpaces += " "
                
                if index < Int(cursorPosition)
                {
                    cursorPosition += 1
                }
            }
            if index < 19 {
                let characterToAdd : Character = Array(string)[index]
                stringWithAddedSpaces.append(characterToAdd)
            }
        }
        return stringWithAddedSpaces
    }
    
}
extension PaymentViewController: WKNavigationDelegate {
    //This is helper to get url params
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    // This is a WKNavigationDelegate func we can use to handle redirection
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping ((WKNavigationActionPolicy) -> Void)) {
        
        if let url = navigationAction.request.url {
            
            /*
             We control here when the user wants to cancel a payment.
             By default a cancel action redirects to http://cancelurl.com/.
             Based on our workflow we can for example remove the webview or push
             another view to the user.
             */
            if url.absoluteString == "http://cancelurl.com/"{
                decisionHandler(.cancel)
                self.showAlert(message: "Payment cancelled")
                navigationController?.popViewController(animated: true)
            }
            else{
                decisionHandler(.allow)
            }
            
            //After a successful transaction we can check if the current url is the callback url
            //and do what makes sense for our workflow. We can get the transaction reference for example.
            
            if url.absoluteString.hasPrefix(callbackUrl){
                let reference = getQueryStringParameter(url: url.absoluteString, param: "reference")
                viewModel.getPayment(token: token, transRef: reference ?? "").subscribe { result in
                    if result.status{
                        self.moveToNext()
                    }
                } onError: { [self] (error) in
                    print(error)
                    self.loadingIndicator(load: false, btnTitle: btnTitle, btn: proceedButton)
                } onCompleted: {
                    self.loadingIndicator(load: false, btnTitle: self.btnTitle, btn: self.proceedButton)
                } onDisposed: {
                    self.loadingIndicator(load: false, btnTitle: self.btnTitle, btn: self.proceedButton)
                }.disposed(by: dispose)
            }
        }
    }
}
