//
//  AccountSelectionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit

class AccountSelectionViewController: BaseViewController, SSRadioButtonControllerDelegate {
    func didSelectButton(selectedButton: UIButton?) {
        
    }
    
    var accounttype:Int?
    var senderBool = false
    var senderBool2 = false
    var allData = SubscriptionSubmissionData()
    var subscribtionData:AllInstrument?
    @IBOutlet weak var jointAcct: SSRadioButton!
    
    @IBOutlet weak var individyualAccount: SSRadioButton!
    
    var radioButtonController: SSRadioButtonsController?
    
    
    
    @IBOutlet weak var jointAccountView: CardView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //jointAccountView.isHidden = true
        radioButtonController = SSRadioButtonsController(buttons: individyualAccount, jointAcct)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        
    }
    
    @IBAction func individual(_ sender: SSRadioButton) {
        
        if !sender.isSelected{
            accounttype = 1
        }else{
            accounttype = nil
        }
  
       
    }
    
    @IBAction func jointAccount(_ sender: SSRadioButton) {
        
        if !sender.isSelected{
            accounttype = 2
        }else{
            accounttype = nil
        }

    }
    
    @IBAction func nextBtn(_ sender: UIButton) {
        if accounttype == nil{
            showToast(message: "Kindly Select Account Type", font: .systemFont(ofSize: 10))
        }else{
         allData.businesstype = accounttype
            moveToNext()
        }
    }
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "doyouhavechn") as? ClearingHouseViewController
        if let registerVC = nextVc{
            registerVC.subscribtionData = subscribtionData
            registerVC.submistionData = allData
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
