//
//  CompanySubscriptionFormViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 9/30/21.
//

import UIKit
import iOSDropDown
import SKCountryPicker
import FlagPhoneNumber

class CompanySubscriptionFormViewController: UIViewController {

    @IBOutlet var companyName:UITextField!
    @IBOutlet var rcNumber:UITextField!
    @IBOutlet var chn:UITextField!
    @IBOutlet var cscs:UITextField!
    @IBOutlet var passPorNumber:UITextField!
    @IBOutlet var dateOfAccountOpen:UITextField!
    @IBOutlet var emailAddress:UITextField!
    @IBOutlet var phoneNumber:FPNTextField!
    @IBOutlet var contactPerson:UITextField!
    @IBOutlet var taxIdCardNumber:UITextField!
    @IBOutlet weak var phoneStackView: UIStackView!

    
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var rCNumber: UILabel!
    @IBOutlet weak var chnLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var taxIdLabel: UILabel!
    @IBOutlet weak var dteOfbankOpening: UILabel!
   

    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    private let errorMessage6 = UILabel()
    private let errorMessage7 = UILabel()
    private let errorMessage8 = UILabel()
   
    @IBOutlet weak var taxIdStack: UIStackView!
    
    @IBOutlet weak var accountOpeningStack: UIStackView!
    
    @IBOutlet weak var chnStack: UIStackView!
    
    @IBOutlet weak var cscStack: UIStackView!
    
    let datePicker = UIDatePicker()
    var requiredGender = ["Male","Female"]
    
    var submistionData:SubscriptionSubmissionData? = nil
    var subscribtionData:AllInstrument?
    let userDetails = Utils.getUserUserdetails()
    
    var isValid = false
    let datePicker2 = UIDatePicker()
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let sub = submistionData?.isChn{
            if sub{
                taxIdStack.isHidden = true
                accountOpeningStack.isHidden = true
                chnStack.isHidden = false
                cscStack.isHidden = false
            }else{
                
                taxIdStack.isHidden = false
                accountOpeningStack.isHidden = false
                chnStack.isHidden = true
                cscStack.isHidden = true
                //Utils.saveUserChnn(chn: "")
                //Utils.saveUserCscs(cscs: "")
            }
        }
        
        addAsterix(label: companyNameLabel, text: "Company Name")
        addAsterix(label: rCNumber, text: "R/C Number")
        addAsterix(label: chnLabel, text: "CHN")
        addAsterix(label: passLabel, text: "Passport Number")

        addAsterix(label: email, text: "Email Address")
        addAsterix(label: dteOfbankOpening, text: "Date of Bank Account creation")
        addAsterix(label: taxIdLabel, text: "Tax Identification Number")
        addAsterix(label: phone, text: "Phone")
        showDatePicker()
     
        getSubDetails()
        setDelegate(textField:companyName,chn,cscs,phoneNumber,cscs,passPorNumber,dateOfAccountOpen,emailAddress,contactPerson,taxIdCardNumber)
        addBoader(textField:companyName,chn,cscs,phoneNumber,cscs,passPorNumber,dateOfAccountOpen,emailAddress,contactPerson,taxIdCardNumber)
        
        stackViewBoader(stackView: phoneStackView)
        let _ = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
       // countryCodes.addGestureRecognizer(tap)
        configurePhoneNumber()
    }
    

    
    func configurePhoneNumber(){
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.setFlag(countryCode: .NG)
        phoneNumber.hasPhoneNumberExample = true
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.displayMode = .list
        
        listController.setup(repository: phoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.phoneNumber.setFlag(countryCode: country.code)
        }

        phoneNumber.delegate = self
        setDelegate(textField: phoneNumber)
        
        phoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        phoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
          // handling code
          let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
             // self.countryCodeText.text = country.dialingCode
           // self.countryFlag.image = country.flag
          }
       
          countryController.detailColor = UIColor.red

      }
    
   
    
    func getSubDetails(){
        if let details =  Utils.getLastSubscriptionDetails(){
            
            companyName.text = details.companyname
            rcNumber.text = details.rcno
            passPorNumber.text = details.passportno
            chn.text = details.chnno
            cscs.text = details.cscsno
            emailAddress.text = details.email
            phoneNumber.set(phoneNumber: details.phone ?? "")
            contactPerson.text = details.contactperson
            
           
            
        }else{
            companyName.text = userDetails?.companyname
            contactPerson.text = userDetails?.contactfirstName
            rcNumber.text = userDetails?.rcno
            chn.text = Utils.getUserChn()
            cscs.text = Utils.getUserCscs()
            emailAddress.text = userDetails?.email
            if let no = userDetails?.phone{
                phoneNumber.set(phoneNumber: no)
            }
           
        }
        
        //Utils.getSubscriptionDetails()
        //guard let details = userDetails else{return}

    }

    
    @IBAction func continueBtn(_ sender: UIButton) {
        if validateField(){

            submistionData?.companyname = companyName.text!
            submistionData?.rcno = rcNumber.text!
            submistionData?.chnno = chn.text!
            submistionData?.phone = phoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
            submistionData?.cscsno = cscs.text!
            submistionData?.passportno = passPorNumber.text!
            submistionData?.bankOpeningDate = dateOfAccountOpen.text!
            submistionData?.email = emailAddress.text!
            submistionData?.contactperson = contactPerson.text!
            submistionData?.taxId = taxIdCardNumber.text!
            moveToNext()
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validateField()-> Bool{
        var value = true
        if let sub = submistionData?.isChn{
            if sub{
                if companyName.text!.isEmpty {
                    setupErrorMessage(error: "Company Name is Empty", errorMessage: errorMessage2, isHidden: false, textField: companyName)
                    companyName.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if rcNumber.text!.isEmpty {
                    setupErrorMessage(error: "Rc Number is Empty", errorMessage: errorMessage5, isHidden: false, textField: rcNumber)
                    rcNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if passPorNumber.text!.isEmpty {
                    setupErrorMessage(error: "Passport Number is Empty", errorMessage: errorMessage8, isHidden: false, textField: passPorNumber)
                    passPorNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if emailAddress.text!.isEmpty {
                    setupErrorMessage(error: "Email Address is Empty", errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                    emailAddress.layer.borderColor = UIColor.red.cgColor
                    value = false
                }else{
                    do {
                        let _ = try self.emailAddress.validatedText(validationType: ValidatorType.email)
                        value = true
                    } catch(let error) {
                        setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                        value = false
                    }
                }
                
               
                
                
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                else if !isValid{
                    print("value",isValid)
                    setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                
                
            }
            else{
                if companyName.text!.isEmpty {
                    setupErrorMessage(error: "Company Name is Empty", errorMessage: errorMessage2, isHidden: false, textField: companyName)
                    companyName.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if rcNumber.text!.isEmpty {
                    setupErrorMessage(error: "Rc Number is Empty", errorMessage: errorMessage5, isHidden: false, textField: rcNumber)
                    rcNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if emailAddress.text!.isEmpty {
                    setupErrorMessage(error: "Email Address is Empty", errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                    emailAddress.layer.borderColor = UIColor.red.cgColor
                    value = false
                }else{
                    do {
                        let _ = try self.emailAddress.validatedText(validationType: ValidatorType.email)
                        value = true
                    } catch(let error) {
                        setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage3, isHidden: false, textField: emailAddress)
                        value = false
                    }
                }
                
               
                if phoneNumber.text!.isEmpty {
                    setupErrorMessage(error: "Phone Number is Empty", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                else if !isValid{
                    print("value",isValid)
                    setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
                    phoneNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if taxIdCardNumber.text!.isEmpty {
                    setupErrorMessage(error: "Tax ID is Empty", errorMessage: errorMessage6, isHidden: false, textField: taxIdCardNumber)
                    taxIdCardNumber.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
                
                if dateOfAccountOpen.text!.isEmpty {
                    setupErrorMessage(error: "Select Date Bank Account was Opened", errorMessage: errorMessage1, isHidden: false, textField: dateOfAccountOpen)
                    dateOfAccountOpen.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
        }
       
        
       
      
        
        return value
    }
  
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "address") as? AddressSubscriptionViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
  
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5,errorMessage6,errorMessage7,errorMessage8)
        clearBorder(textField: companyName,chn,cscs,phoneNumber,cscs,passPorNumber,dateOfAccountOpen,emailAddress,contactPerson,taxIdCardNumber)
    }
    
    
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton =  UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePicker.maximumDate = Date()
        // add toolbar to textField
        dateOfAccountOpen.inputAccessoryView = toolbar
        // add datepicker to textField
        dateOfAccountOpen.inputView = datePicker
        
    }
 
    
    func showDatePicker2(){
        //Formate Date
        datePicker2.datePickerMode = .date
        datePicker2.preferredDatePickerStyle = .wheels
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donedatePicker2))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton =  UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        datePicker2.maximumDate = Date()
        // add toolbar to textField
        dateOfAccountOpen.inputAccessoryView = toolbar
        // add datepicker to textField
        dateOfAccountOpen.inputView = datePicker
        
    }
 
 
 
 
    
    @objc func donedatePicker2(){
     //For date formate
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
    dateOfAccountOpen.text = formatter.string(from: datePicker2.date)
      //dismiss date picker dialog
      self.view.endEditing(true)
       }
 
 
 @objc func donedatePicker(){
  //For date formate
   let formatter = DateFormatter()
   formatter.dateFormat = "yyyy-MM-dd"
     dateOfAccountOpen.text = formatter.string(from: datePicker.date)
   //dismiss date picker dialog
   self.view.endEditing(true)
    }
    
  
 
 @objc func cancelDatePicker(){
      //cancel button dismiss datepicker dialog
       self.view.endEditing(true)
     }
    
    @objc func dismissCountries() {
        listController.dismiss(animated: true, completion: nil)
    }

}

extension CompanySubscriptionFormViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5,errorMessage6,errorMessage7,errorMessage8)
        clearBorder(textField: companyName,chn,cscs,phoneNumber,cscs,passPorNumber,dateOfAccountOpen,emailAddress,contactPerson,taxIdCardNumber)
     
//        print(
//            isValid,
//            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
//            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
//            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
//            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
//            textField.getRawPhoneNumber() ?? "Raw: nil"
//        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}
