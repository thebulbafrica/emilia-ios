//
//  ClearingHouseViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 09/03/2021.
//

import UIKit
import MHLoadingButton
import RxSwift

class ClearingHouseViewController: BaseViewController,SSRadioButtonControllerDelegate {
    @IBOutlet weak var no: SSRadioButton!
    
    
    @IBOutlet weak var yes: SSRadioButton!
    
    @IBOutlet weak var continueBtn: LoadingButton!
    
   
    
    func didSelectButton(selectedButton: UIButton?) {
        
    }
    
    var subscribtionData:AllInstrument?
    var submistionData:SubscriptionSubmissionData? = nil
    
    var haveCHN = ""
  
  
    
    @IBOutlet weak var noView: CardView!
    
    
    
    var radioButtonController: SSRadioButtonsController?
    override func viewDidLoad() {
        super.viewDidLoad()
       // noView.isHidden = true
        //viewModel.dataMapper.delegate = self
        continueBtn.indicator = MaterialLoadingIndicator(color: .white)
        radioButtonController = SSRadioButtonsController(buttons: no, yes)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
       // configure()
    }
    

    @IBAction func yesBtn(_ sender: SSRadioButton) {
        if !sender.isSelected{
            haveCHN = "Yes"
        }else{
            haveCHN = ""
        }
    }
    
    @IBAction func noBtn(_ sender: SSRadioButton) {
        if !sender.isSelected{
            haveCHN = "No"
        }else{
            haveCHN = ""
        }
        
    }

    
    @IBAction func continueBtn(_ sender: UIButton) {
        if haveCHN.isEmpty{
            showToast(message: "Do you have CHN?", font: .systemFont(ofSize: 10))
        }else if haveCHN == "Yes"{
            submistionData?.isChn = true
            moveToNext()
        }else{
            submistionData?.isChn = false
            moveToBroker()
           // viewModel.nonTradingAccount(token: Utils.getToken())
            //showToast(message: "Requires CHN to proceed", font: .systemFont(ofSize: 10))
        }
    }
    
    
    
    func moveToNext(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "validatechn") as? CHNValidateViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    func moveToBroker(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "broker") as? AccountDetailsViewController
        if let registerVC = nextVc{
            registerVC.submistionData = submistionData
            registerVC.subscribtionData = subscribtionData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
}



