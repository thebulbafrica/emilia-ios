//
//  SingleSubscriptionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 11/03/2021.
//

import UIKit

class SingleSubscriptionViewController: BaseViewController {

    
    @IBOutlet weak var appId: UILabel!
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var instrument: UILabel!
    
    @IBOutlet weak var titleName: UILabel!
    
    @IBOutlet weak var issuer: UILabel!
    
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var chn: UILabel!
    
    @IBOutlet weak var cscsNumber: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var subscribtionDate: UILabel!
    
    var subHistroy:AllSubData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleName.text = subHistroy?.name
        appId.text = subHistroy?.applicationid
        name.text = subHistroy?.name
        instrument.text = subHistroy?.code
        issuer.text = subHistroy?.issuer
        amount.text = String(subHistroy!.amount)
        chn.text = subHistroy?.chnno
        cscsNumber.text = subHistroy?.cscsno
        if subHistroy?.status == 1{
            status.text = "Successful"
        }else{
            status.textColor = .red
            status.text = "Failed"
        }
        
        
        subscribtionDate.text = Utils.timeZoneForWallet2(date: subHistroy!.subdate)
    }
    

    @IBAction func backBtn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
