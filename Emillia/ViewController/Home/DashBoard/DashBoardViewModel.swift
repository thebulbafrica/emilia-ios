//
//  DashBoardViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
import RxSwift


public class DashBoardViewModel{
    
 
    let dataMapper = DashBoardDataMapper()

 
    func getUserDashBoard(token:String,id:Int) -> Observable<HomeResponse> {
        return  DashBoardRoute.getUserDashBoard(token: token, id: id, serviceResponse: dataMapper)
    }
    
    
    
}
