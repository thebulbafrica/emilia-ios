//
//  DashBoardViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 02/03/2021.
//

import UIKit
import ListPlaceholder
import RxSwift

class DashBoardViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var topStackView: UIStackView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var amountSpent: UILabel!
    
    @IBOutlet weak var transactionFailed: UILabel!
    
    @IBOutlet weak var transactionSuccessful: UILabel!
    
    @IBOutlet weak var subscriptionTotal: UILabel!
    
    @IBOutlet weak var notificationBtn: UIButton!
    var data = [HomeTransaction]()
    
    @IBOutlet weak var useView: UIView!
    
    
    @IBOutlet weak var firstView: CardView!
    
    @IBOutlet weak var secondView: CardView!
    
    @IBOutlet weak var thirdView: CardView!
    
    @IBOutlet weak var forthView: CardView!
    
    
    
    //var shimmerView : ShimmeringView!

    let viewModel = DashBoardViewModel()
    let dispose = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        let id = Utils.getUserId()
        let token = Utils.getToken()
        loadData(token:token,id:id)
        firstView.showLoader()
        secondView.showLoader()
        thirdView.showLoader()
        forthView.showLoader()
        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(DashBoardViewController.removeLoader2), userInfo: nil, repeats: false)
    }
    
    @IBAction func viewAllTransaction(_ sender: UIButton) {
        tabBarController?.selectedIndex = 1
    }
    
    
    @IBAction func openNotification(_ sender: UIButton) {
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "notification")
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    func loadData(token:String,id:Int) {
        viewModel.getUserDashBoard(token:token, id: id).subscribe {[weak self] (instrument) in
            if let self = self{
                
                if instrument.data.transactions.isEmpty{
                    self.createImage(tableView: (self.tableView)!, height: 100, width: 100, firstText: "No transactions yet", secondText: "")
                }else{
                    // cell.transactionAmount.text =
                    self.data.removeAll()
                    self.data.append(contentsOf: instrument.data.transactions)
                    self.tableView.reloadData()
                    self.tableView.showLoader()
                    Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(DashBoardViewController.removeLoader), userInfo: nil, repeats: false)
                    //userImage
                }
                Utils.saveUserDetails(user: instrument.data.userDetails)
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                let totalAmountSpent = numberFormatter.string(from: NSNumber(value:instrument.data.totalAmount))
                let totalSubscription = instrument.data.totalSubscriptions
                let totalSuccessfulTransaction = instrument.data.totalSuccessfulTransactions
                let totalFailedTransaction = instrument.data.totalFailedTransactions
                self.userName.text = instrument.data.userDetails.username
                self.amountSpent.text = "₦" + String(totalAmountSpent!)
                self.subscriptionTotal.text = String(totalSubscription)
                self.transactionFailed.text = String(totalFailedTransaction)
                self.transactionSuccessful.text = String(totalSuccessfulTransaction)
            }
        } onError: { (error) in
            print("error: ",error)
        } onCompleted: {
            
        } onDisposed: {
        }.disposed(by: dispose)

    }
    
    
    @objc func removeLoader()
    {
        self.tableView.hideLoader()
 
    }
    
    
    @objc func removeLoader2()
    {
        firstView.hideLoader()
        secondView.hideLoader()
        thirdView.hideLoader()
        forthView.hideLoader()
    }
    
    
    
}

extension DashBoardViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count > 2{
            return 3
        }else{
            return data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transaction", for: indexPath) as! TransactionCell
        let allData = data[indexPath.row]
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        let transactionAmount = numberFormatter.string(from: NSNumber(value:allData.amount))
        cell.transactionName.text = allData.name
        cell.transactionAmount.text =  "₦" + String(transactionAmount!)

        if allData.status == 1{
            cell.imageView?.image = UIImage(named: "Group 28")
        }else{
            cell.imageView?.image = UIImage(named: "Group 168")
        }
        cell.transactionDate.text = Utils.timeZoneForWallet(date: String(allData.transdate))
        if allData.status == 1{
            cell.transactionStatus.textColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
            cell.transactionStatus.text = "Succesfull"
        }else{
            cell.transactionStatus.textColor = .red
            cell.transactionStatus.text = "Failed"
        }
        return cell
    }
    
    func moveToNotification(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
}



extension DashBoardViewController:ResponseDisplay{
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        if loading {
            //shimmerView.isShimmering = true
        }else{
           // shimmerView.isShimmering = false
        }
    }
    
    func moveToVc(data:TransacationData){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "subtransaction") as? SingleTransactionViewController
        if let registerVC = nextVc{
            registerVC.singleTransaction = data
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
}
