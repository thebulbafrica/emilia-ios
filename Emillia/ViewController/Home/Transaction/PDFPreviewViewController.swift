//
//  PDFPreviewViewController.swift
//  Emillia
//
//  Created by apple on 02/09/2021.
//

import UIKit
import WebKit

class PDFPreviewViewController: UIViewController {
    
    var invoiceComposer: TransactionComposer!
    
    var invoiceCompose2:SubscriptionComposer!
    
    var sub = false
    
    var invoiceInfo: [String: AnyObject] = [:]
    @IBOutlet weak var webPreview:  WKWebView!
    
  

    var HTMLContent: String!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        createInvoiceAsHTML()
    }
    
    @IBAction func downloadPdf(_ sender: AnyObject) {
        exportPdf()
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func createInvoiceAsHTML() {
        if sub{
            invoiceCompose2 = SubscriptionComposer()
            if let invoiceHTML = invoiceCompose2.renderInvoice(invoiceNumber: invoiceInfo["invoiceNumber"] as! String, name: invoiceInfo["name"] as! String, start: invoiceInfo["start"] as! String, end: invoiceInfo["end"] as! String, items: invoiceInfo["items"] as! [[String: String]]){
                webPreview.loadHTMLString(invoiceHTML, baseURL: nil)
                HTMLContent = invoiceHTML
            }
        }else{
            invoiceComposer = TransactionComposer()
            if let invoiceHTML = invoiceComposer.renderInvoice(invoiceNumber: invoiceInfo["invoiceNumber"] as! String, name: invoiceInfo["name"] as! String, start: invoiceInfo["start"] as! String, end: invoiceInfo["end"] as! String, items: invoiceInfo["items"] as! [[String: String]]){
                webPreview.loadHTMLString(invoiceHTML, baseURL: nil)
                HTMLContent = invoiceHTML
            }
        }
      
       
    }
    
    
    func exportPdf(){
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(webPreview.viewPrintFormatter(), startingAtPageAt: 0)
        
        let paperRect = CGRect(x: 0, y: 0, width: 595.2, height: 841.8)
        let printableRect = paperRect.insetBy(dx: 10, dy: 10)
        
        render.setValue(NSValue(cgRect: paperRect), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printableRect), forKey: "printableRect")
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        
        for i in 0 ..< render.numberOfPages {
            UIGraphicsBeginPDFPage()
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i, in: bounds)
        }
        UIGraphicsEndPDFContext()
        //     let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        //        let filePath = "\(documentsPath)/name.pdf"
        //        pdfData.write(toFile: filePath, atomically: true)
        
        let activityViewController = UIActivityViewController(activityItems: [pdfData], applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
   

}
