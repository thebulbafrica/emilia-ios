//
//  TransactionDataMapper.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation

class TransactionDataMapper: ServiceResponder {
   
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T) {

    }
    
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    

    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
    }
    
    
}
