//
//  SingleTransactionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit

class SingleTransactionViewController: BaseViewController {

    
    
    @IBOutlet weak var transactionTitle: UILabel!
    
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var describtion: UILabel!
    
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var transactionDate: UILabel!
    
    var singleTransaction:TransacationData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindView()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func bindView(){
        if let eachTransaction = singleTransaction{
            name.text = eachTransaction.name
            transactionTitle.text = eachTransaction.name
            describtion.text = eachTransaction.description
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            
            let transactionAmount = numberFormatter.string(from: NSNumber(value: eachTransaction.amount))
            amount.text = "₦" + String(transactionAmount!)
            transactionDate.text =  Utils.timezoneMonthDayYear(date: eachTransaction.transdate)
            
            if eachTransaction.status == 1{
                status.text = "Successful"
            }else{
                status.text = "UnSuccessful"
            }
            
            
            
        }
    }
    


}
