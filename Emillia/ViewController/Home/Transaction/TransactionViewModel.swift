//
//  TransactionViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
import RxSwift

public class TransactionViewModel{
    
 
    let dataMapper = TransactionDataMapper()

 
    func getallTransaction(token:String,id:Int) -> Observable<TransacationResponse> {
        return  TransactionRoute.getallTransaction(token: token, userId: id, serviceResponse: dataMapper)
    }
    
    func filterTransaction(token:String,filter: Filter)-> Observable<TransacationResponse>{
        TransactionRoute.filterTransaction(token: token, filter: filter, serviceResponse: dataMapper)
    }
}
