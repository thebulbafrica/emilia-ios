//
//  BottomFilterTransactionViewController.swift
//  Emillia
//
//  Created by apple on 30/08/2021.
//

import UIKit
import SimpleCheckbox
protocol SelectedDelegate: AnyObject {
    func update(selectedParameter:String,download:Bool)
}

class BottomFilterTransactionViewController: UIViewController {

    weak var delegate:SelectedDelegate?
    
    @IBOutlet weak var randomFilter: Checkbox!
    
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var downloadHistroyLabel: UILabel!
    let userId = Utils.getUserId()
    
    var sub = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if sub{
            subTitle.text = "Sort Subscriptions"
            downloadHistroyLabel.text = "Download Subscription history"
        }else{
            subTitle.text = "Sort Transactions"
            downloadHistroyLabel.text = "Download Transaction history"
        }
     
        randomFilter.uncheckedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        randomFilter.borderStyle = .circle
        randomFilter.checkedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        randomFilter.checkmarkStyle = .tick
        randomFilter.checkmarkColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        randomFilter.addTarget(self, action: #selector(checkboxValueChanged(sender:)), for: .valueChanged)
    }

    @objc func checkboxValueChanged(sender: Checkbox) {
        if sender.isChecked{
            randomFilter.checkboxFillColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
            randomFilter.checkmarkColor = .white
        }else{
            randomFilter.checkboxFillColor = .clear
        }
    }
    
    @IBAction func enableRandomFt(_ sender: Checkbox) {
        if sender.isChecked{
            delegate?.update(selectedParameter: "2", download: false)
        }
       
    }
 
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.layer.opacity = 1
        if sub{
            UIView.animate(withDuration: 0.3) { [weak self] in
                let frame = self?.view.frame
                let yComponent = UIScreen.main.bounds.height - 550
                self?.view.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
            }
        }else{
            UIView.animate(withDuration: 0.3) { [weak self] in
                let frame = self?.view.frame
                let yComponent = UIScreen.main.bounds.height - 400
                self?.view.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
            }
        }
        
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        if let parent = self.parent as? TransactionViewController{
            parent.opView.isHidden = true
        }
        if let parent = self.parent as? SubHistroyViewController{
            parent.opView.isHidden = true
        }
        UIView.animate(withDuration: 0.3) { [weak self] in
            let frame = self?.view.frame
            let yComponent = UIScreen.main.bounds.height
            self?.view.frame = CGRect(x: 0, y: yComponent, width: frame!.width, height: frame!.height)
        }
    }
    
    @IBAction func sucessBtn(_ sender: UIButton) {
        delegate?.update(selectedParameter: "1",download: false)
    }
    
    @IBAction func failedBtn(_ sender: UIButton) {
        delegate?.update(selectedParameter: "0",download: false)
    }
    
    @IBAction func dateBtn(_ sender: UIButton) {
        delegate?.update(selectedParameter: "date",download: false)
    }
    
    
    @IBAction func downloadBtn(_ sender: UIButton) {
        delegate?.update(selectedParameter: "date",download: true)
    }
    
    
}
