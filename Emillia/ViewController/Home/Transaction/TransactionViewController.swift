//
//  TransactionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import UIKit
import RxSwift

class TransactionViewController: BaseViewController,SelectedDelegate, DateDelegate{
   
    func update(start: String, endDate: String) {
        filterByDate(startDate: start, endDate: endDate, token: token)
    }
    
    
    func update(selectedParameter: String, download: Bool) {
        
        if selectedParameter == "date"{
            opView.isHidden = true
            removebottomSheet()
            datepopUp(parentVC: self, download: download)
        }else if selectedParameter == "2"{
            opView.isHidden = true
            removebottomSheet()
            loadData(token: token)
        }else{
            opView.isHidden = true
            removebottomSheet()
            filterByParameter(value: selectedParameter, token: token)
        }
    }
   
    
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var opView: UIView!
    //@IBOutlet weak var sideBtn: UIButton!
    
    //@IBOutlet weak var searchField: UISearchBar!
    
    
    var data = [TransacationData]()
    let viewModel = TransactionViewModel()
    let token = Utils.getToken()
    let dispose = DisposeBag()
    var selectedIndex:[Int] = []
    let userId = Utils.getUserId()
    
    let maxDimmedAlpha: CGFloat = 0.6
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  opView.backgroundColor = .clear
      
        opView.isHidden = true
        viewModel.dataMapper.delegate = self
        loadData(token: token)
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        removebottomSheet()
    }
    
    func addBottomSheetView() {
        opView.isHidden = false
        opView.backgroundColor = .black
        opView.layer.opacity = 0.5
        
        
        if let bottomSheetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BottomFilterTransactionViewController") as? BottomFilterTransactionViewController {
            // 2- Add bottomSheetVC as a child view
            
            bottomSheetVC.delegate = self
            self.addChild(bottomSheetVC)
            self.view.addSubview(bottomSheetVC.view)
            bottomSheetVC.didMove(toParent: self)
            
            // 3- Adjust bottomSheet frame and initial position.
            
            let height = view.frame.height
               let width  = view.frame.width
            bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height) 
        }
    }
    
    func removebottomSheet(){
        if self.children.count > 0{
            let viewControllers:[UIViewController] = self.children
            for viewContoller in viewControllers{
                if viewContoller is BottomFilterTransactionViewController{
                    viewContoller.willMove(toParent: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParent()
                }
                break
            }
        }
    }
    
    func datepopUp(parentVC: UIViewController,download:Bool){
        //creating a reference for the dialogView controller
        if let popupViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DateModalViewController") as? DateModalViewController {
            popupViewController.modalPresentationStyle = .custom
            popupViewController.modalTransitionStyle = .crossDissolve
            popupViewController.delegate = self
            popupViewController.download = download
            popupViewController.sub = false
            parentVC.present(popupViewController, animated: true)
        }
    }
    
    
    @IBAction func filterBtn(_ sender: UIButton) {
        removebottomSheet()
        addBottomSheetView()
    }
    
    func filterByDate(startDate:String,endDate:String,token:String){
        let filter = Filter(endDate: endDate, startDate: startDate, userID: userId, status: nil, sendToMail: 0)
        viewModel.filterTransaction(token: token, filter: filter).subscribe(onNext: {[weak self] instrument in
            if instrument.status{
                if instrument.data.isEmpty{
                    self?.data.removeAll()
                    self?.tableView.reloadData()
                    self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No transactions ", secondText: "You currently do not have any transactions")
                }else{
                    if let viewWithTag = self?.tableView.viewWithTag(100) {
                           viewWithTag.removeFromSuperview()
                       }
                    if let self = self{
                        self.data.removeAll()
                        self.data.append(contentsOf: instrument.data)
                        self.tableView.reloadData()
                        self.tableView.showLoader()
                        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SubHistroyViewController.removeLoader), userInfo: nil, repeats: false)
                    }
                }
            }else{
                
            }
        }).disposed(by: dispose)
    }
    
    
    func filterByParameter(value:String,token:String){
        let filter = Filter(endDate: nil, startDate: nil, userID: userId, status: value, sendToMail: 0)
        viewModel.filterTransaction(token: token, filter: filter).subscribe(onNext: {[weak self] instrument in
            if instrument.status{
                if instrument.data.isEmpty{
                    self?.data.removeAll()
                    self?.tableView.reloadData()
                    self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No transactions ", secondText: "You currently do not have any transactions")
                }else{
                    if let viewWithTag = self?.tableView.viewWithTag(100) {
                           viewWithTag.removeFromSuperview()
                       }
                    if let self = self{
                        self.data.removeAll()
                        self.data.append(contentsOf: instrument.data)
                        self.tableView.reloadData()
                        self.tableView.showLoader()
                        Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SubHistroyViewController.removeLoader), userInfo: nil, repeats: false)
                    }
                }
            }else{
                
            }
        }).disposed(by: dispose)
    }
    
   
    
    
    func loadData(token:String) {
        viewModel.getallTransaction(token: token,id:userId).subscribe {[weak self] (instrument) in
            if instrument.data.isEmpty{
                self?.data.removeAll()
                self?.tableView.reloadData()
                self?.createImage(tableView: (self?.tableView)!, height: 200, width: 200, firstText: "No transactions yet", secondText: "You currently do not have any transactions, complete a subscription to see your transaction.")
            }else{
                if let viewWithTag = self?.tableView.viewWithTag(100) {
                       viewWithTag.removeFromSuperview()
                   }
                if let self = self{
                    self.data.removeAll()
                    self.data.append(contentsOf: instrument.data)
                    self.tableView.reloadData()
                    self.tableView.showLoader()
                    Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(SubHistroyViewController.removeLoader), userInfo: nil, repeats: false)
                }
            }
        } onError: { (error) in
            print("error: ",error)
            
        } onCompleted: {
            print("completed: ","completed")
        } onDisposed: {
            print("disposed: ","disposed")
        }.disposed(by: dispose)
        
    }
    
    @objc func removeLoader()
    {
        self.tableView.hideLoader()
    }
    
    
    func moveToVc(data:TransacationData){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "subtransaction") as? SingleTransactionViewController
        if let registerVC = nextVc{
            registerVC.singleTransaction = data
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    
   
    
}


extension TransactionViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionlist", for: indexPath) as! TransactionTableViewCell
        let allData = data[indexPath.row]
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        let transactionAmount = numberFormatter.string(from: NSNumber(value:allData.amount))
        cell.transactionName.text = allData.name
        cell.transactionAmount.text =  "₦" + String(transactionAmount!)
        
        if allData.status == 1{
            cell.imageView?.image = UIImage(named: "Group 28")
            cell.transactionStatus.textColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
            cell.transactionStatus.text = "Successful"
        }else{
            cell.imageView?.image = UIImage(named: "Group 168")
            cell.transactionStatus.textColor = .red
            cell.transactionStatus.text = "Failed"
        }
        cell.transactionDate.text = Utils.timeZoneForWallet(date: allData.transdate)
        
    
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex.append(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
        let item = data[indexPath.row]
        moveToVc(data: item)
        
    }
    
    
}

extension TransactionViewController:ResponseDisplay{
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        
    }
    
    
}
