//
//  DateModalViewController.swift
//  Emillia
//
//  Created by apple on 31/08/2021.
//

import UIKit
import MHLoadingButton
import RxSwift

protocol DateDelegate: AnyObject {
    func update(start:String,endDate:String)
}

class DateModalViewController: UIViewController, ResponseDisplay {
    func result<T>(success: Bool, data: T?) {
        
    }
    
    func loading(loading: Bool) {
        if loading {
            DownloadPdf.showLoader(userInteraction: true)
        }else{
            DownloadPdf.hideLoader{
                
            }
        }
    }
    

    @IBOutlet weak var startDate: UITextField!
    
    @IBOutlet weak var endDate: UITextField!
    
    @IBOutlet weak var DownloadPdf: LoadingButton!
    
    let viewModel = TransactionViewModel()
    let viewModel2 = SubscriptionViewModel()
    let userId = Utils.getUserId()
    let token = Utils.getToken()
    let userDetails = Utils.getUserUserdetails()
    //var items: [[String: AnyObject]] = []
    
    var items: [[String: String]] = []
    
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var popUpView: UIView!
    let datePicker = UIDatePicker()
    let start = ""
    let end = ""
    
    var download = false
    var sub = false
    
    var informations: [String: AnyObject] = [:]
    
    weak var delegate:DateDelegate?
    let dispose = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.dataMapper.delegate = self
        DownloadPdf.indicator = MaterialLoadingIndicator(color: .white)
        popUpView.layer.cornerRadius = 10
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        if sub{
            subTitle.text = "Subscription history"
        }else{
            subTitle.text = "Transaction history"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        items.removeAll()
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        dismiss(animated: true, completion:nil)
    }
    @IBAction func startDateSelector(_ sender: UITextField) {
        showStartDatePicker()
    }
    
    @IBAction func endDateSelector(_ sender: UITextField) {
        showEndDatePicker()
    }
    
    @IBAction func downloadPdfBtn(_ sender: LoadingButton) {
        if startDate.text!.isEmpty{
            showAlert(message: "Selecte Start Date")
        }else if endDate.text!.isEmpty{
            showAlert(message: "Selecte End Date")
        }else{
            if download{
                getData(start:startDate.text!,end:endDate.text!)
                
            }else{
                dismiss(animated: true, completion: {
                    self.delegate?.update(start: self.startDate.text!, endDate: self.endDate.text!)
                })
            }
        }
        
    }
    
    func getData(start:String,end:String){
        items.removeAll()
        DownloadPdf.showLoader(userInteraction: true)
       
        if sub{
            let chn = Utils.getUserChn()
           
           
            let filter2 = SubFilter(chnno: chn, endDate: end, startDate: start, userID: userId, filterWithChn: nil, status: nil, brokerName: nil, instrument: nil, sendToMail: nil)
            
            viewModel2.filterSubscription(token: token, filter: filter2).subscribe { [weak self]result in
               
                self?.DownloadPdf.hideLoader{
                }
                
                if result.status{
                    if let data = result.data {
                        if data.isEmpty{
                            
                        }else{
                            data.forEach { subscription in
                                
                                self?.items.append(
                                    ["transactionDate":Utils.timeZone(date: subscription.subdate),
                                     "transactionName": subscription.name,
                                     "transactionCode": subscription.code,
                                     "transactionDesc": subscription.issuer,
                                    "transactionAmount": String(subscription.amount),
                                    "transactionStatus": subscription.status == 0 ? "Failed" : "Successfull"
                                    ]
                                )
                            }
                        }
                    }
                    
                    self?.informations["invoiceNumber"] = "download" as AnyObject
                    if let userDetails = self?.userDetails{
                        self?.informations["name"] = "\(userDetails.firstname) \(userDetails.lastname)" as AnyObject
                    }
                    self?.informations["start"] = start as AnyObject
                    self?.informations["end"] = end as AnyObject
                    self?.informations["items"] = self?.items as AnyObject
                    
                    self!.dismiss(animated: true, completion:{
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PDFPreviewViewController") as! PDFPreviewViewController
                        nextViewController.invoiceInfo = self!.informations
                        nextViewController.sub = true
                        self?.present(nextViewController, animated:true, completion:nil)
                    })
                   
                }
                
                
            } onError: { [weak self] _ in
                self?.DownloadPdf.hideLoader{}
            } onCompleted: {[weak self] in
                    self?.DownloadPdf.hideLoader{}
            }.disposed(by: dispose)
        }else{
            let filter = Filter(endDate: end, startDate: start, userID: userId, status: nil, sendToMail: 0)
            viewModel.filterTransaction(token: token, filter: filter).subscribe(onNext: {[weak self]result in
                self?.DownloadPdf.hideLoader{
                    
                }
                if result.status{
                    if result.data.isEmpty{
    //                    self?.items.append(
    //                        ["transactionDate": "",
    //                         "transactionName": "",
    //                        "transactionCode": "",
    //                        "transactionDesc": "",
    //                        "transactionAmount": "",
    //                        "transactionStatus": "")]
    //                    )
                    }else{
                        result.data.forEach { transactions in
                            self?.items.append(
                                ["transactionDate":Utils.timeZone(date: transactions.transdate),
                                 "transactionName": transactions.name,
                                "transactionCode": transactions.code,
                                "transactionDesc": transactions.description,
                                "transactionAmount": String(transactions.amount),
                                "transactionStatus": transactions.status == 0 ? "Failed" : "Successfull"]
                            )
                        }
                    }
                    
                    self?.informations["invoiceNumber"] = "download" as AnyObject
                    if let userDetails = self?.userDetails{
                        self?.informations["name"] = "\(userDetails.firstname) \(userDetails.lastname)" as AnyObject
                    }
                    self?.informations["start"] = start as AnyObject
                    self?.informations["end"] = end as AnyObject
                    self?.informations["items"] = self?.items as AnyObject
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "PDFPreviewViewController") as! PDFPreviewViewController
                    nextViewController.invoiceInfo = self!.informations
                    nextViewController.sub = false
                    self?.present(nextViewController, animated:true, completion:nil)
                }
            },
            onError: {[weak self] _ in
                self?.DownloadPdf.hideLoader{}
            },
            onCompleted: {[weak self] in
                self?.DownloadPdf.hideLoader{}
            }
            ).disposed(by: dispose)
            
            
        }
      
       
        
  

        
        
       
        
    }
    
    
    func showStartDatePicker(){
       //Formate Date
       datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    
        startDate.inputAccessoryView = toolbar
        startDate.inputView = datePicker

    }

     @objc func donedatePicker(){

      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
        startDate.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
       self.view.endEditing(true)
     }
   
    func showEndDatePicker(){
       //Formate Date
       datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
      //ToolBar
      let toolbar = UIToolbar();
      toolbar.sizeToFit()
      let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker2));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
     let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        endDate.inputAccessoryView = toolbar
        endDate.inputView = datePicker

    }

     @objc func donedatePicker2(){

      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
        endDate.text = formatter.string(from: datePicker.date)
      self.view.endEditing(true)
    }
    
}
