//
//  OnBoardingImageCollectionViewCell.swift
//  Emillia
//
//  Created by Anthony Odu on 28/02/2021.
//

import UIKit

class OnBoardingImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var textView: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    class func getIdentifier() -> String {
        return "onBoardingImageCell"
    }
}
