//
//  TransactionCell.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var transactionImage: UIImageView!
    
    @IBOutlet weak var transactionName: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var transactionAmount: UILabel!
    @IBOutlet weak var transactionStatus: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
