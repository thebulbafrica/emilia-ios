//
//  SubHistroyTableViewCell.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import UIKit

class SubHistroyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var applicationId: UILabel!
    
    @IBOutlet weak var applicationName: UILabel!
    
    @IBOutlet weak var dueDate: UILabel!
    
    @IBOutlet weak var amount: UILabel!
    
    @IBOutlet weak var viewMore: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
