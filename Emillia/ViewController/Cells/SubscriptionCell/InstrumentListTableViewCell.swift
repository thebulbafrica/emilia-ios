//
//  InstrumentListTableViewCell.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import UIKit

class InstrumentListTableViewCell: UITableViewCell {

    @IBOutlet weak var tenure: UILabel!
    
    @IBOutlet weak var applicationName: UILabel!
    
    @IBOutlet weak var dueDate: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var subscribe: UIButton!
    
    @IBOutlet weak var statusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
