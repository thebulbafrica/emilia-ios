//
//  IssuringHouseTableViewCell.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import UIKit
import SimpleCheckbox

class IssuringHouseTableViewCell: UITableViewCell  {

    @IBOutlet weak var title:UILabel!
    
   
    @IBOutlet weak var checkBoxBtn: Checkbox!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBoxBtn.borderStyle = .circle
        checkBoxBtn.checkmarkStyle = .circle
        checkBoxBtn.checkmarkStyle = .circle
        checkBoxBtn.checkedBorderColor = UIColor(red: 0.00, green: 0.57, blue: 0.27, alpha: 1.00)
        checkBoxBtn.checkmarkColor  = UIColor(red: 0.00, green: 0.57, blue: 0.27, alpha: 1.00)
        checkBoxBtn.uncheckedBorderColor = .gray
    }
    
    @IBAction func checkBox(_ sender: Checkbox) {
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
