//
//  OtpViewController.swift
//  Emillia
//
//  Created by apple on 16/09/2021.
//

import UIKit
import RxSwift

class OtpViewController: UIViewController {

    @IBOutlet weak var otp: UITextField!
    @IBOutlet weak var continueBtn: UIButton!
    
    private let errorMessage1 = UILabel()
    var viewModel = LoginViewModel()
    private let disposeBag = DisposeBag()
    let child = SpinnerViewController()
    var userEmail = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        otp.addBoader()
        configureLoginServiceCallBacks()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validate()-> Bool{
        var value = true
        if otp.text!.isEmpty {
            setupErrorMessage(error: "OTP is Empty", errorMessage: errorMessage1, isHidden: false, textField: otp)
            otp.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if otp.text!.count != 4{
            setupErrorMessage(error: "Invalid OTP", errorMessage: errorMessage1, isHidden: false, textField: otp)
            otp.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }
    
    @IBAction func contBtn(_ sender: UIButton) {
        if validate(){
            let otpField = otp.text!
            let otpData = DeviceOTP(emailAddress: userEmail, oneTimePassword: otpField)
            viewModel.registerDeviceOTP(user: otpData)
           // viewModel.confirmOTP(otp: otpData)
        }
    }
    
    private func configureLoginServiceCallBacks() {
        viewModel.showLoading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            if loading{
                this.showSpinnerView(child: this.child)
            }else{
                this.removeSpinnerView(child: this.child)
            }
            
        })
        .disposed(by: disposeBag)
        
        
        viewModel.errors.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
        
        viewModel.resendOTP.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (result) in
            guard let self = self else { return }
            if result{
                self.showAlert(message: "OTP Sent")
            }
        })
        .disposed(by: disposeBag)
        
        
        viewModel.otpSuccess.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (result) in
            guard let self = self else { return }
            self.moveToLogin()
        })
        .disposed(by: disposeBag)
        
    }
    
    
    
    
    
    
    @IBAction func resendOtp(_ sender: UIButton) {
        viewModel.resendDeviceOTP(user: PasswordRecover(email: userEmail))
    }
    
    
    func moveToLogin(){
        self.navigationController?.popToRootViewController(animated: true)
//        for obj in (self.navigationController?.viewControllers)! {
//            if obj is LoginViewController {
//                let controller = obj as! LoginViewController
//                self.navigationController?.popToViewController(controller, animated: true)
//                break
//            }
//
//        }
    }
}

struct DeviceOTP:Codable {
    let emailAddress:String
    let oneTimePassword:String
}
