//
//  RegesterPhoneViewController.swift
//  Emillia
//
//  Created by apple on 16/09/2021.
//

import UIKit
import RxSwift

class RegesterPhoneViewController: UIViewController {
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    let child = SpinnerViewController()
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    
    private let disposeBag = DisposeBag()
    
    var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLoginServiceCallBacks()
    }
    
    private func configureLoginServiceCallBacks() {
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        password.addRightImageToTextField2(using: passwordImage)
        viewModel.showLoading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            if loading{
                this.showSpinnerView(child: this.child)
            }else{
                this.removeSpinnerView(child: this.child)
            }
            
        })
        .disposed(by: disposeBag)
        
        
        viewModel.errors.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
        
        
        viewModel.registerDevice.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (result) in
            guard let self = self else { return }
            self.moveToOTP()
        })
        .disposed(by: disposeBag)
        
    }
    
    func validate()-> Bool{
        var value = true
        if password.text!.isEmpty {
            setupErrorMessage(error: "Password is Empty", errorMessage: errorMessage1, isHidden: false, textField: password)
            //            setupErrorMessage(error: "Password is Empty", errorMessage: errorMessage1, isHidden: false, textField: password)
            password.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if userName.text!.isEmpty {
            setupErrorMessage(error: "Username or Email is Empty", errorMessage: errorMessage2, isHidden: false, textField: userName)
            //            setupErrorMessage(error: "Confirm Password is Empty", errorMessage: errorMessage2, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        return value
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func moveToOTP(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "otpphone") as? OtpViewController
        
        
        if let registerVC = nextVc{
            registerVC.userEmail = userName.text!
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        if validate(){
            let userEmai = userName.text!
            let userPassword = password.text!
            let userData = RegisterPhone(emailAddress: userEmai, password: userPassword, deviceID: UIDevice.current.identifierForVendor!.uuidString)
            viewModel.registerDevice(user: userData)
        }
    }
}


