//
//  OnBoardingInstructionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 28/02/2021.
//

import UIKit

class OnBoardingInstructionViewController: UIViewController {

    @IBOutlet weak var sliderCollection: UICollectionView!
    @IBOutlet weak var pageControl: CustomPageControl! //UIPageControl!
    
    
    let imageAndText:[OnBoardImageAndText] = [
        OnBoardImageAndText(imageName: "first_onboarding_image", text: "Grow your finances and manage your resources by investing now."),
        OnBoardImageAndText(imageName: "second_onboarding_image", text: "Be the boss of your finances today."),
        OnBoardImageAndText(imageName: "third_onboarding_image", text: "Get very good interests on investments after said period.")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func allcountries()-> [Country]?{
        if let path = Bundle.main.path(forResource: "countries", ofType: "json")  {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(CountryDataResponse.self, from: data)
               return  jsonData.countries
            }
            catch {
                // handle error
            }
        }
        return nil
    }
    
    @IBAction func registerBtn(_ sender: UIButton) {
        goToRegisterViewController()
    }
    
    @IBAction func loginBtn(_ sender: UIButton) {
        goToLoginViewController()
    }
    

    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    
    
    func goToRegisterViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createAccountIntroductionViewController") as? CreateAccountIntroductionViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    
}

extension OnBoardingInstructionViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = imageAndText.count
        pageControl.numberOfPages = count
        return count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: OnBoardingImageCollectionViewCell.getIdentifier(), for: indexPath) as! OnBoardingImageCollectionViewCell
        
        let values = imageAndText[indexPath.row]
        cell.imageView.image = UIImage(named: values.imageName)
        cell.textView.text = values.text
        
        return cell
        
    }
 
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let width = scrollView.frame.size.width
        let currentPage = Int((scrollView.contentOffset.x + (0.5 * width)) / width)
        pageControl?.currentPage = currentPage
     
    }

    
}


