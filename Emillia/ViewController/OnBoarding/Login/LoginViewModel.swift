//
//  LoginViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation
import RxSwift


public class LoginViewModel{
    private let apiClient = APIClient()
    private let disposeBag = DisposeBag()
    let showLoading: PublishSubject<Bool> = PublishSubject()
    let errors : PublishSubject<CustomError> = PublishSubject()
    let data: PublishSubject<LoginData> = PublishSubject()
    let registerDevice:PublishSubject<Bool> = PublishSubject()
    let otpSuccess:PublishSubject<Bool> = PublishSubject()
    let resendOTP:PublishSubject<Bool> = PublishSubject()
  
    let dataMapper = LogiDataMapper()
    
    func loginUser(user:Login){
        showLoading.onNext(true)
        let result = apiClient.send(path: "/api/login", method: .post, token: nil, parameters: user) as Observable<LoginResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.showLoading.onNext(false)
            if result.status{
                if let res = result.data{
                    self.data.onNext(res)
                    if let token = result.extras {
                        Utils.saveToken(token: token)
                    }
                    
                    if let id = result.data?.id {
                        Utils.saveUserId(id: id)
                    }
                    if let email = result.data?.email {
                        Utils.saveUserEmail(email: email)
                    }
                }else{
                    self.errors.onNext(.serverMessage(result.statusMessage))
                }
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.showLoading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.showLoading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: disposeBag)
       //return result
    }
    
    
    
    func registerDevice(user:RegisterPhone){
        showLoading.onNext(true)
        let result = apiClient.send(path:"/api/login/RegisterDeviceAsync", method: .post, token: nil, parameters: user) as Observable<RegisterDevicesResponse>
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.showLoading.onNext(false)
            if result.status{
                self.registerDevice.onNext(true)
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.showLoading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.showLoading.onNext(false)
        } onDisposed: {
        }.disposed(by: disposeBag)
    }
    
    
    
    
    func registerDeviceOTP(user:DeviceOTP){
        showLoading.onNext(true)
        let result = apiClient.send(path:"/api/login/Device/verify/code", method: .post, token: nil, parameters: user) as Observable<OTPResponse>
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.showLoading.onNext(false)
            if result.status{
                self.otpSuccess.onNext(true)
//                if let _ = result.data{
//                    self.otpSuccess.onNext(true)
//                }else{
//                    self.errors.onNext(.serverMessage(result.statusMessage))
//                }
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.showLoading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.showLoading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: disposeBag)
    }
    
    
    func resendDeviceOTP(user:PasswordRecover){
        showLoading.onNext(true)
        let result = apiClient.send(path:"/api/login/resend/device/code?email=\(user.email)", method: .post, token: nil, parameters: user) as Observable<resetOTPResponse>
      
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.showLoading.onNext(false)
            if result.status{
                if let _ = result.data{
                    self.resendOTP.onNext(true)
                }else{
                    self.errors.onNext(.serverMessage(result.statusMessage))
                }
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.showLoading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.showLoading.onNext(false)
        } onDisposed: {
            
        }.disposed(by: disposeBag)
    }
 
    func login(user:Login) {
        LoginRoute.login(user: user, serviceResponse: dataMapper)
    }
    
    
    
    
}
