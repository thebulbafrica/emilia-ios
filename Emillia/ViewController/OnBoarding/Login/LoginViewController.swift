//
//  LoginViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import MHLoadingButton
import SimpleCheckbox
import SwiftKeychainWrapper
import LocalAuthentication
import RxSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var registerDevice: UIButton!
    
    @IBOutlet weak var loginButton: LoadingButton!
    var timer: Timer? = nil
    var count = 10
    
    var saveBio = false

    let child = SpinnerViewController()
    
    @IBOutlet weak var enableFingerPintBtn: Checkbox!
    @IBOutlet weak var enableFingerPrint: UIStackView!
    @IBOutlet weak var figerPrint: UIButton!
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    
    private let disposeBag = DisposeBag()

    var viewModel = LoginViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //viewModel.dataMapper.delegate = self
        loginButton.indicator = MaterialLoadingIndicator(color: .white)
        adjustView()
        let registerNewDevice = Utils().getDevice()
        let allowBiometric = Utils().getBiometric()
        
        if registerNewDevice{
            registerDevice.isHidden = true
        }else{
            registerDevice.isHidden = false
        }
        
        if allowBiometric{
            figerPrint.isHidden = false
            enableFingerPrint.isHidden = true
        }else{
            figerPrint.isHidden = true
            enableFingerPrint.isHidden = false
        }
        configureLoginServiceCallBacks()
    }
    
    private func configureLoginServiceCallBacks() {
        viewModel.showLoading.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (loading) in
            guard let this = self else {return}
            if loading{
                this.showSpinnerView(child: this.child)
            }else{
                this.removeSpinnerView(child: this.child)
            }
           
        })
        .disposed(by: disposeBag)
      
        
 
        
        viewModel.errors.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (error) in
            guard let self = self else {return}
            switch error {
            case .internetError(let message):
                self.showAlert(message: message)
            case .serverMessage(let message):
                self.showAlert(message: message)
            }
        })
        .disposed(by: disposeBag)
        
        
        viewModel.data.observe(on: MainScheduler.instance).subscribe(onNext: {[weak self] (result) in
            guard let self = self else { return }
            
            Utils().saveBiometrics(allow: self.saveBio)
            self.moveToVc(identifier: "tabViewController")
            self.userName.text = ""
            self.password.text = ""
        })
        .disposed(by: disposeBag)
    
    }
    
   
    
    func adjustView(){
        setDelegate(textField: userName,password)
        //addBoader(textField: userName,password)
        password.setRightPaddingPoints(10)
        password.setLeftPaddingPoints(10)
        userName.setLeftPaddingPoints(10)
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        password.addRightImageToTextField2(using: passwordImage)
        
        enableFingerPintBtn.uncheckedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        enableFingerPintBtn.borderStyle = .circle
        enableFingerPintBtn.checkedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        enableFingerPintBtn.checkmarkStyle = .tick
        enableFingerPintBtn.checkmarkColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        enableFingerPintBtn.addTarget(self, action: #selector(checkboxValueChanged(sender:)), for: .valueChanged)
    }

    @objc func checkboxValueChanged(sender: Checkbox) {
        if sender.isChecked{
            enableFingerPintBtn.checkboxFillColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
            enableFingerPintBtn.checkmarkColor = .white
        }else{
            enableFingerPintBtn.checkboxFillColor = .clear
        }
    }
    
    @IBAction func register(_ sender: UIButton) {
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createAccountIntroductionViewController")
        self.navigationController?.pushViewController(nextVc, animated: true)
    }
    
    
    
    @IBAction func login(_ sender: LoadingButton) {
        if validate(){
            let userEmai = userName.text!
            let userPassword = password.text!
            //if enableFingerPintBtn.isChecked{
                saveUserDetails(userEmail: userEmai, password: userPassword)
           /// }
            let userData = Login(emailAddress: userEmai, password: userPassword, deviceID: UIDevice.current.identifierForVendor!.uuidString)
            //viewModel.login(user: userData)
            viewModel.loginUser(user: userData)
        }
    }
    
    @IBAction func fingerPrintImageBtn(_ sender: UIButton) {
        authenticationWithTouchID()
    }
    

    @IBAction func enableFigerPrintFt(_ sender: Checkbox) {
        saveBio = sender.isChecked
//        if sender.isChecked{
//            figerPrint.isHidden = false
//        }else{
//            figerPrint.isHidden = true
//        }
    }
    
    func saveUserDetails(userEmail:String,password:String){
        let _: Bool = KeychainWrapper.standard.set(password, forKey: "userPassword")
        let _: Bool = KeychainWrapper.standard.set(userEmail, forKey: "userEmail")
    }
    
    func retriveData() -> Login?{
        let retrievedPassword: String? = KeychainWrapper.standard.string(forKey: "userPassword")
        let retrievedEmail: String? = KeychainWrapper.standard.string(forKey: "userEmail")
       
        
        if let userpass = retrievedPassword , let userEmailData = retrievedEmail{
            let details = Login(emailAddress: userEmailData, password: userpass, deviceID: UIDevice.current.identifierForVendor!.uuidString)
            return details
        }else{
            return nil
        }
        
    }
    
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        moveToVc(identifier: "forgotPassword")
    }
    
    @IBAction func moveToRegisterDevice(_ sender: UIButton){
        moveToVc(identifier: "registerphone")
    }
    
    func moveToVc(identifier:String){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(nextVc, animated: true)
        //tabViewController
    }
    
 
    
    func moveToOTP(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "completregistration") as? CompleteRegistrationOTPViewController
        if let registerVC = nextVc{
            registerVC.userEmail = userName.text!
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    
    func validate()-> Bool{
        var value = true
        if password.text!.isEmpty {
            setupErrorMessage(error: "Password is Empty", errorMessage: errorMessage1, isHidden: false, textField: password)
//            setupErrorMessage(error: "Password is Empty", errorMessage: errorMessage1, isHidden: false, textField: password)
            password.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if userName.text!.isEmpty {
        setupErrorMessage(error: "Username or Email is Empty", errorMessage: errorMessage2, isHidden: false, textField: userName)
//            setupErrorMessage(error: "Confirm Password is Empty", errorMessage: errorMessage2, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
        }

        
        return value
    }
}

extension LoginViewController{

//
//    func result<T>(success: Bool, data: T?) {
//        if success {
//            if let result = data as? LoginResponse{
//                if result.status{
//                    Utils().saveBiometrics(allow: saveBio)
//                    Utils.saveToken(token: result.extras!)
//                    Utils.saveUserId(id: result.data!.id)
//                    moveToVc(identifier: "tabViewController")
//                    userName.text = ""
//                    password.text = ""
//                }else{
//                    self.showAlert(message: result.statusMessage)
//                }
//
//            }
//
//        }else{
//            if let message = data as? String{
//                if message.contains("verified"){
//                    print("verified")
//                    showAlerforOTP(message: message)
//                }else{
//                    self.showAlert(message: message)
//                }
//
//            }
//        }
//    }
//
//    func loading(loading: Bool) {
//        if loading {
//            loginButton.showLoader(userInteraction: true)
//        }else{
//            loginButton.hideLoader{
//
//            }
//        }
//    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2)
        clearBorder(textField: userName,password)
    }

    func showAlerforOTP(message: String) {
          let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction!) in
            self.moveToOTP()

        })
          alertController.addAction(alertAction)
          present(alertController, animated: true, completion: nil)
      }


}


//MARK: - BIOMETRIC METHODS
extension LoginViewController {
    func authenticationWithTouchID() {
        
       
        if let loginData = retriveData(){
            //
            let localAuthenticationContext = LAContext()
            localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
            
            var authError: NSError?
            let reasonString = "To access the secure data"
            
            if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                
                localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                    
                    if success {
                        //TODO: User authenticated successfully, take appropriate action
                        DispatchQueue.main.async { [weak self] in
                            self?.userName.text = self?.retriveData()?.emailAddress
                            self?.password.text  = self?.retriveData()?.password
                            self?.viewModel.loginUser(user: loginData)
                        }
                        
                    } else {
                        //TODO: User did not authenticate successfully, look at error and take appropriate action
                        guard let error = evaluateError else {
                            return
                        }
                        
                        print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                        
                        //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                        
                    }
                }
            } else {
                
                guard let error = authError else {
                    return
                }
                //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
                print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
            }
        }else{
            
        }
        
      
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                showAlert(message: message)
                
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                showAlert(message: message)
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                showAlert(message: message)
            default:
                message = "Did not find error code on LAError object"
                showAlert(message: message)
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                showAlert(message: message)
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                showAlert(message: message)
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                showAlert(message: message)
                
            default:
                message = "Did not find error code on LAError object"
                showAlert(message: message)
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    
}
