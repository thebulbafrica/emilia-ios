//
//  LoginDataMapper.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

class LogiDataMapper: ServiceResponder {
   
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T) {
        if let result = response as? LoginResponse{
            if result.status{
                delegate?.result(success: true, data: result)
            }else{
                delegate?.result(success: false, data: result.statusMessage)
            }
        }
    }
    
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    

    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
    }
    
    
}
