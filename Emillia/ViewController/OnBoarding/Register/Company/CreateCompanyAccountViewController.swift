//
//  CreateCompanyAccountViewController.swift
//  Emillia
//
//  Created by apple on 25/08/2021.
//

import UIKit

class CreateCompanyAccountViewController: UIViewController {

    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var registrationNo: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var lastName: UITextField!
  
    
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var registrationNoLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emaiLabel: UILabel!
    
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    
    var selectedAccountType:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustView()
        addAsterix(label: companyNameLabel, text: "Company Name")
        addAsterix(label: registrationNoLabel, text: "Registration / Business Number")
        addAsterix(label: firstNameLabel, text: "Contact First Name")
        addAsterix(label: lastNameLabel, text: "Contact Last Name")
        addAsterix(label: emaiLabel, text: "Email Address")
        
        addBoader(textField: companyName,registrationNo,firstName,email,email,lastName)
    }
    
    

    @IBAction func nextBtn(_ sender: UIButton) {
        if validate(){
            let userFirstName = firstName.text!
            let userlastNmae = lastName.text!
            let userCompanyName = companyName.text!
            let userRegistrationNumber = registrationNo.text!
            let userEmail = email.text!
            let userData = Register(username: "", password: "", userType: selectedAccountType ?? 0, title: nil, firstname: userFirstName, lastname: userlastNmae, email: userEmail, phone: "", othername: nil, company: userCompanyName, rcno: userRegistrationNumber)
            moveToNextViewController(userData:userData)
        }
      
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func moveToNextViewController(userData:Register){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createAccountcompanycont") as? ContinuationCompanyCreateViewController
        if let registerVC = nextVc{
            registerVC.userdata = userData
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }

}


extension CreateCompanyAccountViewController{
    func adjustView(){
        setDelegate(textField: companyName,registrationNo, firstName,email,lastName)
      //  addBoader(textField: firstName,lastName,phoneNumber, email,userName)
        firstName.setLeftPaddingPoints(10)
        lastName.setLeftPaddingPoints(10)
        companyName.setLeftPaddingPoints(10)
        email.setLeftPaddingPoints(10)
        registrationNo.setLeftPaddingPoints(10)
    }
    
    func validate()-> Bool{
        var value = true
        if firstName.text!.isEmpty {
            setupErrorMessage(error: "First Name is Required", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            firstName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if firstName.text!.count < 3{
            setupErrorMessage(error: "Invalid First Required", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            firstName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }

        if lastName.text!.isEmpty {
            setupErrorMessage(error: "Last Name is Required", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if lastName.text!.count < 3{
            setupErrorMessage(error: "Invalid Last Name", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
       
        
        if email.text!.isEmpty {
            setupErrorMessage(error: "Email is Required", errorMessage: errorMessage4, isHidden: false, textField: email)
            email.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            do {
                let _ = try self.email.validatedText(validationType: ValidatorType.email)
                value = true
            } catch(let error) {
                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage4, isHidden: false, textField: email)
                value = false
            }
            
        }
        if companyName.text!.isEmpty {
            setupErrorMessage(error: "Company Name is Required", errorMessage: errorMessage5, isHidden: false, textField: companyName)
            companyName.layer.borderColor = UIColor.red.cgColor
           
            value = false
        }
//        else if companyName.text!.count < 2{
//            setupErrorMessage(error: "Invalid Company Name", errorMessage: errorMessage3, isHidden: false, textField: companyName)
//            companyName.layer.borderColor = UIColor.red.cgColor
//            value = false
//
//        }
        
        
        if registrationNo.text!.isEmpty {
            setupErrorMessage(error: "Registration Number is Required", errorMessage: errorMessage3, isHidden: false, textField: registrationNo)
            registrationNo.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if registrationNo.text!.count < 2{
            setupErrorMessage(error: "Invalid Registration Number", errorMessage: errorMessage3, isHidden: false, textField: registrationNo)
            registrationNo.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
    
        return value
    }
}

extension CreateCompanyAccountViewController{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:companyName,registrationNo,firstName, email,lastName)
      
    }
}
