//
//  ContinuationCompanyCreateViewController.swift
//  Emillia
//
//  Created by apple on 25/08/2021.
//

import UIKit
import FlagPhoneNumber
import SimpleCheckbox
import MHLoadingButton
import SKCountryPicker


class ContinuationCompanyCreateViewController: UIViewController {

    
    @IBOutlet weak var phoneNumber: FPNTextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var checkBox: Checkbox!
    @IBOutlet weak var signUp: LoadingButton!
    @IBOutlet weak var lblTerms: UILabel!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    
    var viewModel = RegisterViewModel()
    var userdata:Register?
    var isValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePhoneNumber()
        setDelegate()
        adjustView()
        signUp.indicator = MaterialLoadingIndicator(color: .white)
        viewModel.dataMapper.delegate = self
        
        addBoader(textField: phoneNumber,userName,password,confirmPassword)
        
        let text = "By clicking accept, you agree to the Terms & Conditions and agreement of the Debt Management Office."
         let linkTextWithColor = "Terms & Conditions"

        let range = (text as NSString).range(of: linkTextWithColor)

         let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)

        lblTerms.attributedText = attributedString
        
        
        lblTerms.isUserInteractionEnabled = true
        lblTerms.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        lblTerms.addGestureRecognizer(tapGesture)
    }

    
    func configurePhoneNumber(){
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.setFlag(countryCode: .NG)
        phoneNumber.hasPhoneNumberExample = true
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.displayMode = .list
        
        listController.setup(repository: phoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.phoneNumber.setFlag(countryCode: country.code)
        }

        phoneNumber.delegate = self
        setDelegate(textField: phoneNumber)
        
        phoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        phoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

        @objc func dismissCountries() {
            listController.dismiss(animated: true, completion: nil)
        }

    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = lblTerms.text else { return }
            let numberRange = (text as NSString).range(of: "Terms & Conditions")
        
            let emailRange = (text as NSString).range(of: "google.com")
            if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: numberRange) {
                guard let url = URL(string: "http://www.subscribedmo.herokuapp.com/terms") else {
                  return //be safe
                }

                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
               
            } else if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: emailRange) {
                print("Email tapped")
            }
    }
    

    
 
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func register(_ sender: LoadingButton) {
        if validate(){
            if !checkBox.isChecked{
                showAlert(message: "Please Accept Terms and Condition")
            }else{
                let userPassword = password.text!
                let userPhone = phoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
                userdata?.password = userPassword
                userdata?.phone = userPhone
                userdata?.username = userName.text!
                viewModel.register(user: userdata!)
            }
            
        }
    }
    
    
    @IBAction func login(_ sender: UIButton) {
        goToLoginViewController()
    }
    
    
    
    func moveToSuccess(id:Int?){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "completregistration") as? CompleteRegistrationOTPViewController
        if let registerVC = nextVc{
            registerVC.userEmail = userdata!.email
            registerVC.userId = id
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
}

extension ContinuationCompanyCreateViewController{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4)
        clearBorder(textField:password,confirmPassword,phoneNumber,userName)
      
    }
    func adjustView(){
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        password.addRightImageToTextField2(using: passwordImage)
        confirmPassword.addRightImageToTextField2(using: passwordImage)
      
        userName.setLeftPaddingPoints(10)
        password.setLeftPaddingPoints(10)
        confirmPassword.setLeftPaddingPoints(10)
        checkBox.uncheckedBorderColor = .gray
        checkBox.checkedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        checkBox.checkmarkColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
    }
    
    func setDelegate(){
        password.delegate = self
        confirmPassword.delegate = self
        userName.delegate = self
    }
}


extension ContinuationCompanyCreateViewController{
    func validate()-> Bool{
        var value = true
        
        if userName.text!.isEmpty {
            setupErrorMessage(error: "User Name is Required", errorMessage: errorMessage3, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if userName.text!.count < 2{
            setupErrorMessage(error: "Invalid User Name", errorMessage: errorMessage3, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
        if phoneNumber.text!.isEmpty {
            setupErrorMessage(error: "Phone Number is Required", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        else if !isValid{
            print("value",isValid)
            setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage4, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        if password.text!.isEmpty {
            setupErrorMessage(error: "Password is Required", errorMessage: errorMessage1, isHidden: false, textField: password)
            password.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            if password.text!.count <= 5 {
                setupErrorMessage(error: "Password must be greater than 5 character.", errorMessage: errorMessage1, isHidden: false, textField: password)
                password.layer.borderColor = UIColor.red.cgColor
                value = false
            }else{
                if !isPasswordValid(password.text!){
                    setupErrorMessage(error: "Password should contain numbers,alphabet and symbols only.", errorMessage: errorMessage1, isHidden: false, textField: password)
                    password.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
        }
        
        if confirmPassword.text!.isEmpty {
            setupErrorMessage(error: "Confirm Password is Required", errorMessage: errorMessage2, isHidden: false, textField: confirmPassword)
            confirmPassword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        if password.text! != confirmPassword.text! {
            setupErrorMessage(error: "password Mis-match", errorMessage: errorMessage2, isHidden: false, textField: confirmPassword)
            confirmPassword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        return value
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        if password.count < 6 {
            return false
        }else{
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=[^a-z]*[a-z])(?=[^0-9]*[0-9])[a-zA-Z0-9!@#$%^&*]{8,}")
            return passwordTest.evaluate(with: password)
        }
 
    }
  
}

extension ContinuationCompanyCreateViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4)
        clearBorder(textField:password,confirmPassword,phoneNumber,userName)
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}

extension ContinuationCompanyCreateViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success {
            if let response = data as? RegisterResponse{
                if response.status{
                    self.moveToSuccess(id: response.data?.id)
                }else{
                    self.showAlert(message: response.statusMessage)
                }
               
            }
            
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            signUp.showLoader(userInteraction: true)
        }else{
            signUp.hideLoader {
                
            }
        }
    }
    
   
}
