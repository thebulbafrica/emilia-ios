//
//  CreateAccountContinuationViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import SimpleCheckbox
import MHLoadingButton


class IndividualCreateAccountContinuationViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var checkBox: Checkbox!
    @IBOutlet weak var signUp: LoadingButton!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var lblTerms: UILabel!
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    
    var viewModel = RegisterViewModel()
    var userdata:Register?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegate()
        adjustView()
        signUp.indicator = MaterialLoadingIndicator(color: .white)
        viewModel.dataMapper.delegate = self
       
  
//
//        lblTerms.text = "By clicking accept, you agree to the Terms & Conditions and agreement of the Debt Management Office."

        
        let text = "By clicking accept, you agree to the Terms & Conditions and agreement of the Debt Management Office."
         let linkTextWithColor = "Terms & Conditions"

        let range = (text as NSString).range(of: linkTextWithColor)

         let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue , range: range)

        lblTerms.attributedText = attributedString
        
        
        lblTerms.isUserInteractionEnabled = true
        lblTerms.lineBreakMode = .byWordWrapping
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tappedOnLabel(_:)))
        tapGesture.numberOfTouchesRequired = 1
        lblTerms.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func tappedOnLabel(_ gesture: UITapGestureRecognizer) {
        guard let text = lblTerms.text else { return }
            let numberRange = (text as NSString).range(of: "Terms & Conditions")
        
            let emailRange = (text as NSString).range(of: "google.com")
            if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: numberRange) {
                guard let url = URL(string: "http://www.subscribedmo.herokuapp.com/terms") else {
                  return //be safe
                }

                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
               
            } else if gesture.didTapAttributedTextInLabel(label: self.lblTerms, inRange: emailRange) {
                print("Email tapped")
            }
    }
    
    func adjustView(){
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        password.addRightImageToTextField2(using: passwordImage)
        confirmPassword.addRightImageToTextField2(using: passwordImage)
        password.setLeftPaddingPoints(10)
        confirmPassword.setLeftPaddingPoints(10)
        checkBox.uncheckedBorderColor = .gray
        checkBox.checkedBorderColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
        checkBox.checkmarkColor = UIColor(red: 0.20, green: 0.51, blue: 0.27, alpha: 1.00)
    }
    
    func setDelegate(){
        password.delegate = self
        confirmPassword.delegate = self
    }
    
    func validate()-> Bool{
        var value = true
        if password.text!.isEmpty {
            setupErrorMessage(error: "Password is Required", errorMessage: errorMessage1, isHidden: false, textField: password)
            password.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            if password.text!.count <= 5 {
                setupErrorMessage(error: "Password must be greater than 5 character.", errorMessage: errorMessage1, isHidden: false, textField: password)
                password.layer.borderColor = UIColor.red.cgColor
                value = false
            }else{
                if !isPasswordValid(password.text!){
                    setupErrorMessage(error: "Password should contain numbers,alphabet and symbols only.", errorMessage: errorMessage1, isHidden: false, textField: password)
                    password.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }
        
            
        }
        
        if confirmPassword.text!.isEmpty {
            setupErrorMessage(error: "Confirm Password is Required", errorMessage: errorMessage2, isHidden: false, textField: confirmPassword)
            confirmPassword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        if password.text! != confirmPassword.text! {
            setupErrorMessage(error: "password Mis-match", errorMessage: errorMessage2, isHidden: false, textField: confirmPassword)
            confirmPassword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        
        return value
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        if password.count < 6 {
            return false
        }else{
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=[^a-z]*[a-z])(?=[^0-9]*[0-9])[a-zA-Z0-9!@#$%^&*]{8,}")
            return passwordTest.evaluate(with: password)
        }
 
    }
  
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func register(_ sender: LoadingButton) {
        if validate(){
            if !checkBox.isChecked{
                showAlert(message: "Please Accept Terms and Condition")
            }else{
                let userPassword = password.text!
                userdata?.password = userPassword
                viewModel.register(user: userdata!)
            }
            
        }
    }
    
    
    @IBAction func login(_ sender: UIButton) {
        goToLoginViewController()
    }
    
    
    func moveToSuccess(id:Int?){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "completregistration") as? CompleteRegistrationOTPViewController
        if let registerVC = nextVc{
            registerVC.userEmail = userdata!.email
            registerVC.userId = id
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
    
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
}


extension IndividualCreateAccountContinuationViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success {
            if let response = data as? RegisterResponse{
                if response.status{
                    self.moveToSuccess(id: response.data?.id)
                }else{
                    self.showAlert(message: response.statusMessage)
                }
            }
            
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        print(loading)
        if loading {
            signUp.showLoader(userInteraction: true)
        }else{
            signUp.hideLoader {
                
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2)
        clearBorder(textField: password,confirmPassword)
        
    }
}


extension UITapGestureRecognizer {
   
   func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
       guard let attributedText = label.attributedText else { return false }

       let mutableStr = NSMutableAttributedString.init(attributedString: attributedText)
       mutableStr.addAttributes([NSAttributedString.Key.font : label.font!], range: NSRange.init(location: 0, length: attributedText.length))
       
       // If the label have text alignment. Delete this code if label have a default (left) aligment. Possible to add the attribute in previous adding.
       let paragraphStyle = NSMutableParagraphStyle()
       paragraphStyle.alignment = .center
       mutableStr.addAttributes([NSAttributedString.Key.paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: attributedText.length))

       // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
       let layoutManager = NSLayoutManager()
       let textContainer = NSTextContainer(size: CGSize.zero)
       let textStorage = NSTextStorage(attributedString: mutableStr)
       
       // Configure layoutManager and textStorage
       layoutManager.addTextContainer(textContainer)
       textStorage.addLayoutManager(layoutManager)
       
       // Configure textContainer
       textContainer.lineFragmentPadding = 0.0
       textContainer.lineBreakMode = label.lineBreakMode
       textContainer.maximumNumberOfLines = label.numberOfLines
       let labelSize = label.bounds.size
       textContainer.size = labelSize
       
       // Find the tapped character location and compare it to the specified range
       let locationOfTouchInLabel = self.location(in: label)
       let textBoundingBox = layoutManager.usedRect(for: textContainer)
       let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                         y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
       let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                    y: locationOfTouchInLabel.y - textContainerOffset.y);
       let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
       return NSLocationInRange(indexOfCharacter, targetRange)
   }
   
}


