//
//  CompleteRegistrationViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import MHLoadingButton

class CompleteRegistrationOTPViewController: UIViewController {

    
    @IBOutlet weak var otp: UITextField!
    @IBOutlet weak var continueBtn: LoadingButton!
    
    private let errorMessage1 = UILabel()
    var viewModel = RegisterViewModel()
    var userEmail = ""
    var selectedData = 1
    var userId:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        otp.addBoader()
        viewModel.dataMapper.delegate = self
        viewModel.dataMapper2.delegate = self
        continueBtn.indicator = MaterialLoadingIndicator(color: .white)
        // Do any additional setup after loading the view.
    }
    
    func setDelegate(){
        otp.delegate = self
        
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func validate()-> Bool{
        var value = true
        if otp.text!.isEmpty {
            setupErrorMessage(error: "OTP is Empty", errorMessage: errorMessage1, isHidden: false, textField: otp)
            otp.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        if otp.text!.count != 4{
            setupErrorMessage(error: "Invalid OTP", errorMessage: errorMessage1, isHidden: false, textField: otp)
            otp.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }
    @IBAction func contBtn(_ sender: LoadingButton) {
        if validate(){
            let otpField = otp.text!
            let otpData = OTP(userId: userId!, oneTimePassword: otpField)
            selectedData = 0
            viewModel.confirmOTP(otp: otpData)
        }
    }
    
    
    
    @IBAction func resendOtp(_ sender: UIButton) {
        selectedData = 1
        viewModel.resendOtp(passwordRecover: PasswordRecover(email: userEmail))
    }
    
    
    func moveToSuccess(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "accountsuccess") as? RegistrationSuccesfullViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
}

extension CompleteRegistrationOTPViewController:ResponseDisplay{
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1)
        clearBorder(textField: otp)
    }
    
    func result<T>(success: Bool, data: T?) {
        if success {
            if let result = data as? OTPResponse{
                if result.status{
                    self.moveToSuccess()
                }else{
                    self.showAlert(message: result.statusMessage)
                }
            }
            else if let result = data as? resetOTPResponse{
                userId = result.data
                if result.status{
                    self.showAlert(message: result.statusMessage)
                }else{
                    self.showAlert(message: result.statusMessage)
                }
            }
            
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            continueBtn.showLoader(userInteraction: true)
        }else{
            continueBtn.hideLoader {
                
            }
        }
    }
    
  
}
