//
//  CreateAccountIntroductionViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit

class CreateAccountIntroductionViewController: UIViewController {

    @IBOutlet weak var individualSignup: UIButton!
    @IBOutlet weak var companySignup: UIButton!
    
    deinit {
        print("Introduction out of memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
       // companySignup.isHidden = true
    }
    
    @IBAction func individualSignupBtn(_ sender: UIButton) {
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "individualcreateAccount") as? IndividualCreateAccountViewController
        if let registerVC = nextVc{
            registerVC.selectedAccountType = 1
            self.navigationController?.pushViewController(registerVC, animated: true)
            //self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            //self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    @IBAction func companySignupBtn(_ sender: UIButton) {
      // registerVC.selectedAccountType = "Individual"
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "companycreateAccount") as? CreateCompanyAccountViewController
        if let registerVC = nextVc{
            registerVC.selectedAccountType = 2
            self.navigationController?.pushViewController(registerVC, animated: true)
            //self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        goToLoginViewController()
    }
    
}
