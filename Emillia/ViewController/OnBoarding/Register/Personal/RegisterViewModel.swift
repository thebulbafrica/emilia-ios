//
//  RegisterViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

public class RegisterViewModel{
    
 
    let dataMapper = RegisterDataMapper()
   
    let dataMapper2 = ResendOTPDataMapper()
    
    
    func register(user:Register) {
        RegisterRoute.register(user: user, serviceResponse: dataMapper)

    }
    
    
    func confirmOTP(otp:OTP){
        OTPRoute.otpConfirmation(user: otp, serviceResponse: dataMapper)
    }
    
    
    func resendOtp(passwordRecover:PasswordRecover){
        OTPRoute.resendOtp(user: passwordRecover, serviceResponse: dataMapper2)
    }
   
}
