//
//  CreateAccountViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import SKCountryPicker
import FlagPhoneNumber

class IndividualCreateAccountViewController: UIViewController {
    
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
   // @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var phoneNumber: FPNTextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var countryFlag: UIImageView!
    
    @IBOutlet weak var countryCode: UILabel!
    
    @IBOutlet weak var stackViewBoader: UIStackView!
    
   // @IBOutlet weak var countryCodes: UIStackView!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    private let errorMessage3 = UILabel()
    private let errorMessage4 = UILabel()
    private let errorMessage5 = UILabel()
    
    
    @IBOutlet weak var fname: UILabel!
    @IBOutlet weak var lName: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var uName: UILabel!
    @IBOutlet weak var emaiLabel: UILabel!
    
    var selectedAccountType:Int?
    
    var isValid = false
    
    var listController: FPNCountryListViewController = FPNCountryListViewController(style: .grouped)
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        adjustView()
        configurePhoneNumber()

        addAsterix(label: fname, text: "First Name")
        addAsterix(label: lName, text: "Last Name")
        addAsterix(label: uName, text: "Username")
        addAsterix(label: phoneLabel, text: "Phone")
        addAsterix(label: emaiLabel, text: "Email Address")
        
        addBoader(textField: firstName,lastName,userName,email,userName)
        stackViewBoader(stackView: stackViewBoader)
       
    }
    
    func configurePhoneNumber(){
        phoneNumber.borderStyle = .roundedRect
        phoneNumber.setFlag(countryCode: .NG)
        phoneNumber.hasPhoneNumberExample = true
        phoneNumber.placeholder = "Phone Number"
        phoneNumber.displayMode = .list
        
        listController.setup(repository: phoneNumber.countryRepository)

        listController.didSelect = { [weak self] country in
            self?.phoneNumber.setFlag(countryCode: country.code)
        }

        phoneNumber.delegate = self
        setDelegate(textField: phoneNumber)
        
        phoneNumber.flagButtonSize = CGSize(width: 35, height: 35)
        phoneNumber.flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

        @objc func dismissCountries() {
            listController.dismiss(animated: true, completion: nil)
        }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
          // handling code
          let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
              self.countryCode.text = country.dialingCode
            self.countryFlag.image = country.flag
          }
          countryController.detailColor = UIColor.red

      }
    
    
    
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextBtn(_ sender: UIButton) {
        if validate(){
            let userFirstName = firstName.text!
            let userlastNmae = lastName.text!
           // let userCountryCode = countryCode.text!
            let userEmail = email.text!
            let userPhone = phoneNumber.getFormattedPhoneNumber(format: .E164) ?? ""
            let user = userName.text!
           // let deviceuuid = UIDevice.current.identifierForVendor!.uuidString
            let userData = Register(username: user, password: "", userType: selectedAccountType ?? 0, title: nil, firstname: userFirstName, lastname: userlastNmae, email: userEmail, phone:userPhone, othername: nil, company: nil, rcno: nil)
          
            moveToNextViewController(userData:userData)
        }
      
    }
    
    
    func moveToNextViewController(userData:Register){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createAccountcontd") as? IndividualCreateAccountContinuationViewController
        if let registerVC = nextVc{
            registerVC.userdata = userData
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
    func adjustView(){
        setDelegate(textField: firstName,lastName, email,userName)
      //  addBoader(textField: firstName,lastName,phoneNumber, email,userName)
        firstName.setLeftPaddingPoints(10)
        lastName.setLeftPaddingPoints(10)
        //phoneNumber.setLeftPaddingPoints(10)
        email.setLeftPaddingPoints(10)
        userName.setLeftPaddingPoints(10)
    }
    
    func validate()-> Bool{
        var value = true
        if firstName.text!.isEmpty {
            setupErrorMessage(error: "First Name is Required", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            firstName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if firstName.text!.count < 3{
            setupErrorMessage(error: "Invalid First Required", errorMessage: errorMessage1, isHidden: false, textField: firstName)
            firstName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }

        if lastName.text!.isEmpty {
            setupErrorMessage(error: "Last Name is Required", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if lastName.text!.count < 3{
            setupErrorMessage(error: "Invalid Last Name", errorMessage: errorMessage2, isHidden: false, textField: lastName)
            lastName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
       
        
        if email.text!.isEmpty {
            setupErrorMessage(error: "Email is Required", errorMessage: errorMessage4, isHidden: false, textField: email)
            email.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            do {
                let _ = try self.email.validatedText(validationType: ValidatorType.email)
                value = true
            } catch(let error) {
                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage4, isHidden: false, textField: email)
                value = false
            }
            
        }
        if userName.text!.isEmpty {
            setupErrorMessage(error: "User Name is Required", errorMessage: errorMessage5, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
        }else if userName.text!.count < 2{
            setupErrorMessage(error: "Invalid User Name", errorMessage: errorMessage5, isHidden: false, textField: userName)
            userName.layer.borderColor = UIColor.red.cgColor
            value = false
            
        }
        
        if phoneNumber.text!.isEmpty {
            setupErrorMessage(error: "Phone Number is Required", errorMessage: errorMessage3, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        else if !isValid{
            print("value",isValid)
            setupErrorMessage(error: "Invalid Phone Number", errorMessage: errorMessage3, isHidden: false, textField: phoneNumber)
            phoneNumber.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
   

        return value
    }
    
}

extension IndividualCreateAccountViewController{
   
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:firstName,lastName,phoneNumber, email,userName)
      
    }
}

extension IndividualCreateAccountViewController: FPNTextFieldDelegate {

    func fpnDidValidatePhoneNumber(textField: FPNTextField, isValid: Bool) {
        self.isValid = isValid
        clearError(uiLabel: errorMessage1,errorMessage2,errorMessage3,errorMessage4,errorMessage5)
        clearBorder(textField:firstName,lastName,phoneNumber, email,userName)
//        print(
//            isValid,
//            textField.getFormattedPhoneNumber(format: .E164) ?? "E164: nil",
//            textField.getFormattedPhoneNumber(format: .International) ?? "International: nil",
//            textField.getFormattedPhoneNumber(format: .National) ?? "National: nil",
//            textField.getFormattedPhoneNumber(format: .RFC3966) ?? "RFC3966: nil",
//            textField.getRawPhoneNumber() ?? "Raw: nil"
//        )
    }

    func fpnDidSelectCountry(name: String, dialCode: String, code: String) {
        print(name, dialCode, code)
    }


    func fpnDisplayCountryList() {
        let navigationViewController = UINavigationController(rootViewController: listController)

        listController.title = "Countries"
        listController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(dismissCountries))

        self.present(navigationViewController, animated: true, completion: nil)
    }
    
  
}
