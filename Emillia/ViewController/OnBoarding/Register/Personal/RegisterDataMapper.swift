//
//  RegisterDataMapper.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation


class RegisterDataMapper: ServiceResponder {
   
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T) {
        delegate?.result(success: true, data: response)
    }
    
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    

    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
    }
    
    
}

class ResendOTPDataMapper: ServiceResponder {
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T?) {
        if let result = response as? resetOTPResponse{
            if result.status{
                delegate?.result(success: true, data: result)
            }else{
                delegate?.result(success: false, data: result.statusMessage)
            }
        }else if let result = response as? OTPResponse{
            if result.status{
                delegate?.result(success: true, data: result)
            }else{
                delegate?.result(success: false, data: result.statusMessage)
            }
        }
    }
    
    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
        
    }
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    
   
    
   
    
    
    
    
}
