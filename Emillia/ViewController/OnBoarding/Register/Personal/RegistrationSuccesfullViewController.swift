//
//  RegistrationSuccesfullViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import UIKit

class RegistrationSuccesfullViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        removeAllViewControllerExpectLast()
        // Do any additional setup after loading the view.
    }
    

 
    @IBAction func goToLogin(_ sender: UIButton) {
        goToLoginViewController()
    }
    
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            //self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
}
