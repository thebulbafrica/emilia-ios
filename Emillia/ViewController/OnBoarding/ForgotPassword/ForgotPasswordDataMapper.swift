//
//  ForgotPasswordDataMapper.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
class ForgotPasswordDataMapper: ServiceResponder {
   
    
    weak var delegate: ResponseDisplay?
    
    func success<T>(response: T) {
        if let response = response as? ForgotPasswordResponse{
            guard let status = response.status else { return }
            if status{
                delegate?.result(success: true, data: response)
            }else{
                print(response.statusCode)
                delegate?.result(success: false, data: response.statusMessage)
            }
        }
        
    }
    
    
    func loading(loading: Bool) {
        delegate?.loading(loading: loading)
    }
    

    func failed(error: ServiceError) {
        delegate?.result(success: false, data: "Network Error...Please try Again")
        
    }
    
    
}
