//
//  ForgotPasswordViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import MHLoadingButton


class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var loginButton: LoadingButton!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    let viewModel = ForgotPasswordViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        //email.addBoader()
        loginButton.indicator = MaterialLoadingIndicator(color: .white)
    }
    


    @IBAction func contBtn(_ sender: UIButton) {
        if validate(){
            viewModel.enterEmail(password: PasswordRecover(email: email.text!))
        }
    }
    
    func moveToVc(id:Int?){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "forgotPasswordcontd") as? OTPForgotPasswordViewController
        if let registerVC = nextVc{
            registerVC.id = id
            self.navigationController?.pushViewController(registerVC, animated: true)

        }
    }
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setDelegate(){
        email.delegate = self
        
    }
    
    func validate()-> Bool{
        var value = true
        if email.text!.isEmpty {
            setupErrorMessage(error: "Email is Empty", errorMessage: errorMessage1, isHidden: false, textField: email)
            email.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            do {
                let _ = try self.email.validatedText(validationType: ValidatorType.email)
                value = true
            } catch(let error) {
                setupErrorMessage(error: (error as! ValidationError).message, errorMessage: errorMessage1, isHidden: false, textField: email)
                value = false
            }
            
        }
        return value
    }
    
}

extension ForgotPasswordViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success {
            if let result = data as? ForgotPasswordResponse{
                moveToVc(id: result.data)
                Utils.saveUserId(id: result.data!)
            }
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            loginButton.showLoader(userInteraction: true)
        }else{
            loginButton.hideLoader{
                
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1)
        clearBorder(textField: email)
    }
}
