//
//  EnterNewPasswordViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import MHLoadingButton

class EnterNewPasswordViewController: UIViewController {
    
    
    @IBOutlet weak var newPassword: UITextField!
    
    @IBOutlet weak var confirmPasssword: UITextField!
    
    @IBOutlet weak var loginButton: LoadingButton!
    
    private let errorMessage1 = UILabel()
    private let errorMessage2 = UILabel()
    
    let viewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
       // newPassword.addBoader()
        //confirmPasssword.addBoader()
        guard let passwordImage = UIImage(named: "eyeiconclose") else{
            fatalError("Password image not found")
        }
        setDelegate()
        newPassword.addRightImageToTextField2(using: passwordImage)
        confirmPasssword.addRightImageToTextField2(using: passwordImage)
        loginButton.indicator = MaterialLoadingIndicator(color: .white)
    }
    
    
    @IBAction func resetPassword(_ sender: UIButton) {
        let id = Utils.getUserId()
        if validate(){
            viewModel.resetPssword(passwordRecover: NewPassword(userId: id, password: confirmPasssword.text!))
        }
    }
    
    
    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setDelegate(){
        newPassword.delegate = self
        confirmPasssword.delegate = self
    }
    
    func validate()-> Bool{
        var value = true
        if newPassword.text!.isEmpty {
            setupErrorMessage(error: "Password is Required", errorMessage: errorMessage1, isHidden: false, textField: newPassword)
            newPassword.layer.borderColor = UIColor.red.cgColor
            value = false
        }else{
            if newPassword.text!.count <= 5 {
                setupErrorMessage(error: "Password must be greater than 5 character.", errorMessage: errorMessage1, isHidden: false, textField: newPassword)
                newPassword.layer.borderColor = UIColor.red.cgColor
                value = false
            }else{
                if isPasswordValid(newPassword.text!){
                   value = true
                }else{
                    setupErrorMessage(error: "Password should contain numbers,alphabet and symbols only.", errorMessage: errorMessage1, isHidden: false, textField: newPassword)
                    newPassword.layer.borderColor = UIColor.red.cgColor
                    value = false
                }
            }

        }
            
        
        
        if confirmPasssword.text!.isEmpty {
            setupErrorMessage(error: "Confirm Password is Required", errorMessage: errorMessage2, isHidden: false, textField: confirmPasssword)
            confirmPasssword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        if newPassword.text! != confirmPasssword.text! {
            setupErrorMessage(error: "password Mis-match", errorMessage: errorMessage2, isHidden: false, textField: confirmPasssword)
            confirmPasssword.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
        return value
    }

    func isPasswordValid(_ password : String) -> Bool{
        if password.count < 6 {
            return false
        }else{
            
            let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=[^a-z]*[a-z])(?=[^0-9]*[0-9])[a-zA-Z0-9!@#$%^&*]{8,}")
            return passwordTest.evaluate(with: password)
        }
    }
    func moveToVc(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "passwordsuccess") as? PasswordVerifiedViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }
    
}

extension EnterNewPasswordViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success {
            moveToVc()
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            loginButton.showLoader(userInteraction: true)
        }else{
            loginButton.hideLoader{}
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1,errorMessage2)
        clearBorder(textField: newPassword,confirmPasssword)
        
    }
}
