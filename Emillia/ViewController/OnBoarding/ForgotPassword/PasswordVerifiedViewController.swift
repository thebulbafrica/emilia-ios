//
//  PasswordVerifiedViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit

class PasswordVerifiedViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       removeAllViewControllerExpectLast()
    }
    @IBAction func goToLogin(_ sender: UIButton) {
        goToLoginViewController()
    }
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            
        }
    }

}
