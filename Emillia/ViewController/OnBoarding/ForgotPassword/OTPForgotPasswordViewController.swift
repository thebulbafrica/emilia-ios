//
//  OTPForgotPasswordViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import UIKit
import MHLoadingButton

class OTPForgotPasswordViewController: UIViewController {

    @IBOutlet weak var otpField: UITextField!
    
    @IBOutlet weak var loginButton: LoadingButton!
    
    private let errorMessage1 = UILabel()
    
    var email:String = ""
    var id:Int?
    
    let viewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.dataMapper.delegate = self
        otpField.addBoader()
        loginButton.indicator = MaterialLoadingIndicator(color: .white)
    }
    

    @IBAction func ctnBtn(_ sender: UIButton) {
        if validate(){
            viewModel.enterOTP(otp: OTP(userId: id!, oneTimePassword: otpField.text!))
        }
    }
    

    @IBAction func backBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setDelegate(){
        otpField.delegate = self
       
    }
    
    func validate()-> Bool{
        var value = true
        if otpField.text!.isEmpty {
            setupErrorMessage(error: "OTP is Empty", errorMessage: errorMessage1, isHidden: false, textField: otpField)
            otpField.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        if otpField.text!.count != 4{
            setupErrorMessage(error: "Invalid OTP", errorMessage: errorMessage1, isHidden: false, textField: otpField)
            otpField.layer.borderColor = UIColor.red.cgColor
            value = false
        }
        
      
        
        return value
    }
    func moveToVc(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "forgotPasswordfinish") as? EnterNewPasswordViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)

        }
    }
}

extension OTPForgotPasswordViewController:ResponseDisplay{
    
    
    func result<T>(success: Bool, data: T?) {
        if success {
            moveToVc()
        }else{
            if let message = data as? String{
                self.showAlert(message: message)
            }
        }
    }
    
    func loading(loading: Bool) {
        if loading {
            loginButton.showLoader(userInteraction: true)
        }else{
            loginButton.hideLoader{
                
            }
        }
    }
 
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearError(uiLabel: errorMessage1)
        clearBorder(textField: otpField)
    
    }
}
