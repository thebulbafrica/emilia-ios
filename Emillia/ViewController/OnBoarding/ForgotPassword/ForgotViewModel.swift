//
//  ForgotViewModel.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation

public class ForgotPasswordViewModel{
    
 
    let dataMapper = ForgotPasswordDataMapper()
 
    func enterEmail(password:PasswordRecover) {
        PasswordRecoverRoute.enterEmail(user: password, serviceResponse: dataMapper)

    }
    
    
    func enterOTP(otp:OTP){
        PasswordRecoverRoute.enterOTP(user: otp, serviceResponse: dataMapper)
    }
    
    
    func resetPssword(passwordRecover:NewPassword){
        PasswordRecoverRoute.resetPssword(user: passwordRecover, serviceResponse: dataMapper)
    }
   
}
