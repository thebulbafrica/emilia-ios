//
//  Protocol.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import Foundation

protocol ServiceResponder {
    func success<T>(response:T?)
    func failed(error:ServiceError)
    func loading(loading:Bool)
}


enum ServiceError: Error {
  case failedRequest
  case invalidResponse
  case noData
  case invalidData
  //case tokenExpiration
}

protocol ResponseDisplay:AnyObject {
    func result<T>(success:Bool,data:T?)
    func loading(loading:Bool)
}

