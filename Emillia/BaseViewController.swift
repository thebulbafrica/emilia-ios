//
//  BaseViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 26/03/2021.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
    }
    
    
    @objc func appMovedToForeground() {
        startTimer()
    }
    
    @objc func appMovedToBackground() {
        Utils.saveTime(time: startTimer2())
    }
    
    
    
    func goToLoginViewController(){
        let nextVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login") as? LoginViewController
        if let registerVC = nextVc{
            self.navigationController?.pushViewController(registerVC, animated: true)
            self.navigationController?.removeViewController(OnBoardingInstructionViewController.self)
        }
    }
    
    
    private func startTimer() {
        let startTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let time = formatter.string(from: startTime)
        let saveTime = Utils.getTime()
        let diff = findDateDiff(time1Str: saveTime, time2Str: time)
        if diff > 2 {
            goToLoginViewController()
        }
    }
    
    
    
    private func startTimer2()-> String {
        let startTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter.string(from: startTime)
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> Double {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        guard let time1 = timeformatter.date(from: time1Str),
              let time2 = timeformatter.date(from: time2Str) else { return 0 }
        let interval = time2.timeIntervalSince(time1)
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        
        return minute
    }
    
}
