//
//  UserDefault.swift
//  Emillia
//
//  Created by Anthony Odu on 23/03/2021.
//

import Foundation

extension UserDefaults {
    static func clear() {
        guard let domain = Bundle.main.bundleIdentifier else { return }
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
