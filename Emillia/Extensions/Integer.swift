//
//  Integer.swift
//  Emillia
//
//  Created by Anthony Odu on 11/03/2021.
//

import Foundation

public extension Int {
  var asWord: String? {
    let numberValue = NSNumber(value: self)
    let formatter = NumberFormatter()
    formatter.numberStyle = .spellOut
    return formatter.string(from: numberValue)
  }
}
