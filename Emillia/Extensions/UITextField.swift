//
//  UITextField.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import Foundation
import UIKit
//import SKCountryPicker
private var kAssociationKeyMaxLength: Int = 0
extension UITextField {
    
    @IBInspectable var maxLength: Int {
           get {
               if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                   return length
               } else {
                   return Int.max
               }
           }
           set {
               objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
               addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
           }
       }
       
       @objc func checkMaxLength(textField: UITextField) {
           guard let prospectiveText = self.text,
               prospectiveText.count > maxLength
               else {
                   return
           }
           
           let selection = selectedTextRange
           
           let indexEndOfText = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
           let substring = prospectiveText[..<indexEndOfText]
           text = String(substring)
           
           selectedTextRange = selection
       }

    
    
    func setLeftPaddingPoints(_ amount:CGFloat){
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.leftView = paddingView
           self.leftViewMode = .always
       }
    
       func setRightPaddingPoints(_ amount:CGFloat) {
           let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
           self.rightView = paddingView
           self.rightViewMode = .always
       }
    
    
    
    func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self.text!)
    }
    
    
    func setBottomBorder(){
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        
        
    }
    
    func setIcon(_ image: UIImage) {
       let iconView = UIImageView(frame:
                      CGRect(x: 10, y: 5, width: 20, height: 20))
       iconView.image = image
       let iconContainerView: UIView = UIView(frame:
                      CGRect(x: 20, y: 0, width: 30, height: 30))
       iconContainerView.addSubview(iconView)
       rightView = iconContainerView
       rightViewMode = .always
    }
    
    func setupErrorMessage(error:String,errorMessage:UILabel,isHidden:Bool,textField:UITextField,view:UIView) {
        let margins = view.layoutMarginsGuide
        errorMessage.translatesAutoresizingMaskIntoConstraints = false
        errorMessage.text = error
        errorMessage.textColor = .red
        errorMessage.isHidden = isHidden
        errorMessage.font = .systemFont(ofSize: 10)
        view.addSubview(errorMessage)
        errorMessage.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 5).isActive = true
        errorMessage.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0).isActive = true
    }
    
    func addBoader(){
        let myColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00)
        self.layer.borderColor = myColor.cgColor
        self.layer.borderColor = myColor.cgColor
        self.layer.borderWidth = 1.0
        self.layer.borderWidth = 1.0
    }
    
    func addRightImageToTextField(using image:UIImage){
        let rightImageView = UIImageView()
        
        rightImageView.translatesAutoresizingMaskIntoConstraints = false
        rightImageView.heightAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
        rightImageView.widthAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
        

        
        rightImageView.image = image
        self.rightView = rightImageView
        self.rightViewMode = .always
        self.initiateTapGesture(imageView: rightImageView)
    }
    func addRightImageToTextField2(using image:UIImage){
        
        let checkImageView = UIImageView(image: image)
        checkImageView.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        checkImageView.contentMode = .scaleAspectFit
        let padding: CGFloat = 6

        // create the view that would act as the padding
        let rightView = UIView(frame: CGRect(
            x: 0, y: 0, // keep this as 0, 0
            width: checkImageView.frame.width + padding, // add the padding
            height: checkImageView.frame.height))
        rightView.addSubview(checkImageView)

        self.rightView = rightView
        self.rightViewMode = .always
        self.initiateTapGesture(imageView: checkImageView)
    }
    
    func initiateTapGesture(imageView : UIImageView){
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
    }
    
    @objc func tapDetected(){
     
        self.isSecureTextEntry = !self.isSecureTextEntry
        if self.isSecureTextEntry {
            guard let passwordImage = UIImage(named: "eyeiconclose") else{
                fatalError("Password image not found")
            }
            self.addRightImageToTextField2(using: passwordImage)
        }else{
            guard let passwordImage = UIImage(named: "eyeiconopen") else{
                fatalError("Password image not found")
            }
            self.addRightImageToTextField2(using: passwordImage)
        }
        
    }
    
    
//    func createLeftView(image:UIImage,firstText:String){
//        let myView = UIView()
//        //Text Label
//        let textLabel = UILabel()
//        textLabel.textColor = .black
//        textLabel.font = .boldSystemFont(ofSize: 14)
//        textLabel.translatesAutoresizingMaskIntoConstraints = false
//        textLabel.heightAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
//        textLabel.widthAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
//        textLabel.text  = firstText
//        textLabel.textAlignment = .center
//        //UI IMAGE
//        let imageView = UIImageView()
//        imageView.image = image
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.heightAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
//        imageView.widthAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
//        
//        let divider = UIView()
//        divider.translatesAutoresizingMaskIntoConstraints = false
//        divider.heightAnchor.constraint(equalToConstant: CGFloat(20)).isActive = true
//        divider.widthAnchor.constraint(equalToConstant: CGFloat(1)).isActive = true
//        
//        //Stack View
//        let stackView   = UIStackView()
//        stackView.axis  = NSLayoutConstraint.Axis.horizontal
//        stackView.distribution  = UIStackView.Distribution.equalSpacing
//        stackView.alignment = UIStackView.Alignment.center
//        stackView.spacing   = 5.0
//        
//        
//        stackView.addArrangedSubview(textLabel)
//        stackView.addArrangedSubview(imageView)
//        stackView.addArrangedSubview(divider)
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        myView.addSubview(stackView)
//
//        
//        //Constraints
//        stackView.centerXAnchor.constraint(equalTo: myView.centerXAnchor).isActive = true
//        stackView.centerYAnchor.constraint(equalTo: myView.centerYAnchor).isActive = true
//        
//        
//        self.leftView = myView
//        self.leftViewMode = .always
//        self.initiateCountryGesture(myView: myView)
//    }
//    
//    func initiateCountryGesture(myView:UIView){
//        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected2))
//        myView.isUserInteractionEnabled = true
//        myView.addGestureRecognizer(singleTap)
//    }
//    
//    @objc func tapDetected2(){
//     
//        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { (country) in
//            self.contryCodes.text = country.dialingCode
//        }
//     
//        countryController.detailColor = UIColor.red
//        
//    }
    
    
    func setMaxLength(){
        self.addTarget(self, action: #selector(editingChanged(sender:)), for: .editingChanged)
    }
    
    @objc private func editingChanged(sender: UITextField) {
        
        if let text = sender.text, text.count >= 1 {
            sender.text = String(text.dropLast(text.count - 1))
            return
        }
    }
    
    
    
}

