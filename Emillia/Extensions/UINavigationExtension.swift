//
//  UINavigationExtension.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import Foundation
import UIKit

extension UINavigationController {
    func removeViewController(_ controller: UIViewController.Type) {
        if let viewController = viewControllers.first(where: { $0.isKind(of: controller.self) }) {
            viewController.removeFromParent()
        }
    }
}
