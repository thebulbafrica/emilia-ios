//
//  UIViewController.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import Foundation
import UIKit

extension UIViewController:UITextFieldDelegate {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showAlert(message: String, completion: ((UIAlertAction) -> Void)? = nil) {
        let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: completion)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertMessageWithImage(message: String,image:String) {
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
           //Add imageview to alert
            let imgViewTitle = UIImageView(frame: CGRect(x: 0, y: 10, width: 30, height: 30))
            imgViewTitle.image = UIImage(named:image)
            alertController.view.addSubview(imgViewTitle)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    
    func showAlerAndCloseViewController(message: String) {
          let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
            
        })
          alertController.addAction(alertAction)
          present(alertController, animated: true, completion: nil)
      }

    func createImage(tableView:UITableView,height:CGFloat,width:CGFloat,firstText:String,secondText:String){
       //Text Label
       let textLabel = UILabel()
       textLabel.textColor = .black
       textLabel.font = .boldSystemFont(ofSize: 16)
       textLabel.widthAnchor.constraint(equalToConstant: tableView.frame.width).isActive = true
       textLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
       textLabel.text  = firstText
       textLabel.textAlignment = .center
       
       
       //Text Label
       let textLabel2 = UILabel()
       textLabel2.widthAnchor.constraint(equalToConstant: tableView.frame.width).isActive = true
       textLabel2.heightAnchor.constraint(equalToConstant: 40.0).isActive = true
       textLabel2.numberOfLines = 0
       textLabel2.text  = secondText
       textLabel2.textAlignment = .center
       textLabel2.font = .systemFont(ofSize: 12)
       
       let imageName = "Group 115"
       let image = UIImage(named: imageName)
       let imageView = UIImageView()
       imageView.image = image
       imageView.widthAnchor.constraint(equalToConstant: width).isActive = true
       imageView.heightAnchor.constraint(equalToConstant: height).isActive = true
       //Stack View
       let stackView   = UIStackView()
       stackView.axis  = NSLayoutConstraint.Axis.vertical
       stackView.distribution  = UIStackView.Distribution.equalSpacing
       stackView.alignment = UIStackView.Alignment.center
       stackView.spacing   = 0
       
       
       stackView.addArrangedSubview(imageView)
       stackView.addArrangedSubview(textLabel)
       stackView.addArrangedSubview(textLabel2)
       stackView.translatesAutoresizingMaskIntoConstraints = false
       tableView.addSubview(stackView)

       
       //Constraints
       stackView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor).isActive = true
       stackView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor).isActive = true
        stackView.tag = 100
     
   }
    
    func removeAllViewControllerExpectLast(){
       guard let navigationController = navigationController else { return }
       var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
       let temp = navigationArray.last
       navigationArray.removeAll()
       navigationArray.append(temp!) //To remove all previous UIViewController except the last one
       navigationController.viewControllers = navigationArray
      
   }
    
    func setupErrorMessage(error:String,errorMessage:UILabel,isHidden:Bool,textField:UITextField) {
        let margins = view.layoutMarginsGuide
        errorMessage.translatesAutoresizingMaskIntoConstraints = false
        errorMessage.text = error
        errorMessage.textColor = .red
        errorMessage.isHidden = isHidden
        errorMessage.font = .systemFont(ofSize: 10)
        view.addSubview(errorMessage)
        errorMessage.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: 5).isActive = true
        errorMessage.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0).isActive = true
        
    }
    
    func showToast(message : String, font: UIFont) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func setDelegate(textField:UITextField...){
        for field in textField{
            field.delegate = self
        }
    }
    
    func clearError(uiLabel:UILabel...) {
        for label in uiLabel{
            label.isHidden = true
        }
    }
    
    func clearBorder(textField:UITextField...) {
        for field in textField{
            field.layer.borderColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00).cgColor
        }
    }
    
    func addBoader(textField:UITextField...){
        let myColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00)
        for field in textField{
            field.layer.borderColor = myColor.cgColor
            field.layer.borderColor = myColor.cgColor
            field.layer.borderWidth = 1.0
            field.layer.borderWidth = 1.0
        }
    }
    
    func stackViewBoader(stackView:UIStackView){
        let myColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00)
        stackView.layer.borderColor = myColor.cgColor
        stackView.layer.borderColor = myColor.cgColor
        stackView.layer.borderWidth = 1.0
        stackView.layer.borderWidth = 1.0
    }
    
    
    func addAsterix(label:UILabel,text:String){
        let asterix = NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red])
        let attriburedString = NSMutableAttributedString(string: text)
        attriburedString.append(asterix)
        label.attributedText = attriburedString
      }
    
    
    func showAlerAndPopCloseViewController(message: String) {
          let alertController = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: {(action:UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
            
        })
          alertController.addAction(alertAction)
          present(alertController, animated: true, completion: nil)
      }
    
    func loadingIndicator(load: Bool,btnTitle:String,btn:UIButton) {
        let tag = 808404
        if load {
            btn.isEnabled = false
            btn.alpha = 0.5
            btn.setTitle("")
            let indicator = UIActivityIndicatorView()
            let buttonHeight = btn.bounds.size.height
            let buttonWidth = btn.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            btn.addSubview(indicator)
            indicator.startAnimating()
        } else {
            btn.isEnabled = true
            btn.alpha = 1.0
            btn.setTitle(btnTitle)
            if let indicator = btn.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
    
    func showSpinnerView(child:SpinnerViewController) {
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func removeSpinnerView(child:SpinnerViewController){
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
}
