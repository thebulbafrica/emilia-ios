//
//  Reactive.swift
//  Emillia
//
//  Created by apple on 10/09/2021.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

protocol loadingViewable {
    func startAnimating()
    func stopAnimating()
}


extension UIViewController: loadingViewable {
}

extension Reactive where Base: UIViewController {
    /// Bindable sink for `startAnimating()`, `stopAnimating()` methods.
    public var isAnimating: Binder<Bool> {
        return Binder(self.base, binding: { (vc, active) in
            if active {
                vc.startAnimating()
            } else {
                vc.stopAnimating()
            }
        })
    }
}



extension loadingViewable where Self : UIViewController {
    func startAnimating(){
        let spinner = UIActivityIndicatorView(style: .large)
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        spinner.color = UIColor(red: 0.23, green: 0.71, blue: 0.29, alpha: 1.00)
        view.addSubview(spinner)
        view.tag = 12
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func stopAnimating() {
        for item in view.subviews
        where item.tag == 12 {
                UIView.animate(withDuration: 0.3, animations: {
                    item.alpha = 0
                }) { (_) in
                    item.removeFromSuperview()
                }
        }
    }

}

