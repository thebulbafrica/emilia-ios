//
//  CountryDatas.swift
//  Emillia
//
//  Created by Anthony Odu on 09/03/2021.
//
import Alamofire
import RxSwift
import Foundation

class AllCountriesAndState {
    
    private static let baseUrl = Utils.baseUrl
    
   static func allcountries()-> [Country]?{
        if let path = Bundle.main.path(forResource: "countries", ofType: "json")  {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(CountryDataResponse.self, from: data)
               return  jsonData.countries
            }
            catch {
                // handle error
            }
        }
        return nil
    }
    
    static func getAllCountry(token:String,serviceResponse:ServiceResponder)->Observable<allcountry> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(baseUrl)/api/location/countries",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: allcountry.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        print(subscription)
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    static func getAllState(token:String,countryId:Int,serviceResponse:ServiceResponder)->Observable<allState> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(baseUrl)/api/location/states/\(countryId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: allState.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        print(subscription)
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    static func getAllCity(token:String,stateId:Int,serviceResponse:ServiceResponder)->Observable<allCity> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(baseUrl)/api/location/cities/\(stateId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: allCity.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        print(subscription)
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
}
