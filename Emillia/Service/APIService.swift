//
//  APIService.swift
//  Emillia
//
//  Created by apple on 06/09/2021.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire
import SwiftyJSON

public enum CustomError {
       case internetError(String)
       case serverMessage(String)
   }

class APIClient {
    
    
    
    typealias parameters = [String:Any]
    
    enum RequestError: Error {
        case serverError
        case connectionError
        case authorizationError
    }

    private let baseURL =  Utils.baseUrl
    

    func send<U: Codable,T: Codable>(path: String,method: HTTPMethod,token:String?,parameters:U?) -> Observable<T> {
        
        return Observable.create{observer -> Disposable in
            var request:DataRequest? = nil
            
            if token != nil{
                var generatedToken:String{
                    if !token!.contains("Bearer"){
                        return "Bearer \(token!)"
                    }else{
                        return token!
                    }
                }
                let headers: HTTPHeaders? = [
                    .authorization(generatedToken),
                    .accept("application/json")
                ]

                request = AF.request("\(self.baseURL)\(path)",
                             method: method,
                             parameters: parameters,
                             encoder: JSONParameterEncoder.default,headers: headers)
                
               
                
            }else{
                request = AF.request("\(self.baseURL)\(path)",
                             method: method,
                             parameters: parameters,
                             encoder: JSONParameterEncoder.default)
                
              
             
            }

            if let body = request?.request?.httpBody {
                let jsonBodyString = String(data: body, encoding: .utf8)
                print("BODY PAYLOAD: ", jsonBodyString!)
            }
            
            request?.responseJSON(completionHandler: { response in
                
                print(response)
                print(response.request?.url?.absoluteString)
                print(response.response)
                print(response.request)
                
                if let _ = response.error {
                    observer.onError(RequestError.connectionError)
                }
               
               
                if let data = response.data ,let responseCode = response.response?.statusCode {
                   
                    do {
                        let jsonBodyString = String(data: data, encoding: .utf8)
                        print("JSON String : ", jsonBodyString!)
                       // let responseJson = try JSON(data: data)
    //                    print("responseCode : \(responseCode.statusCode)")
                       // print("responseJSON : \(responseJson)")
                        switch responseCode {
                        case 200...201:
                            let decoder = JSONDecoder()
                            let results = try decoder.decode(T.self, from: data)
                            print(results)
                            observer.onNext(results)
                        case 400...499:
                           
                            observer.onError(RequestError.authorizationError)
                        case 500...599:
                            observer.onError(RequestError.serverError)
                        default:
                            observer.onError(RequestError.connectionError)
                            break
                        }
                    }
                    catch let parseJSONError {
                        observer.onError(RequestError.connectionError)
                        print("error on parsing request to JSON : \(parseJSONError)")
                    }
                }
            
            })
            
            return Disposables.create {
            }
        }
    }
}

