//
//  SubscriptionRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
import RxSwift
import Alamofire

class SubscriptionRoute{
    
    private static let baseUrl = Utils.baseUrl
    
    static func getInstrument(token:String,serviceResponse:ServiceResponder)->Observable<InstrumentResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/subscription/offers",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: InstrumentResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print("error: ",error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(instrument):
                        observer.onNext(instrument)
                        serviceResponse.success(response: instrument)
                        serviceResponse.loading(loading: false)
                        
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    static func getSubscription(token:String,userId:Int,serviceResponse:ServiceResponder)->Observable<AllSubDataResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/subscription/\(userId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: AllSubDataResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    static func validateCHN(token:String,user:ValidateChN,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        guard let url = URL(string:"\(baseUrl)/api/cscs/verify/chn") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(ValidateChNResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    static func valideAccountNumber(token:String,user:AccountData,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        guard let url = URL(string:"\(baseUrl)/api/cscs/verify/account") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(String.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    static func validateBVN(token:String,user:BVNData,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        
        guard let url = URL(string:"\(baseUrl)/api/cscs/validate/bvn/record") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(BVNValidateResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    static func getBrokers(token:String,serviceResponse:ServiceResponder)->Observable<BrokersResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]

        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            
            let task = AF.request("\(baseUrl)/api/subscription/brokers",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: BrokersResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        print(subscription)
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
//    static func getBanks(token:String,serviceResponse:ServiceResponder)->Observable<BankResponse> {
//        var generatedToken:String{
//            if !token.contains("Bearer"){
//                return "Bearer \(token)"
//            }else{
//                return token
//            }
//        }
//        let headers: HTTPHeaders = [
//            .authorization(generatedToken),
//            .accept("application/json")
//        ]
//        serviceResponse.loading(loading: true)
//        return Observable.create{observer -> Disposable in
//            let task = AF.request("http://52.90.16.8/banks",headers: headers)
//            task.validate(statusCode: 200..<600)
//                .responseDecodable(of: BankResponse.self){ response in
//                    switch response.result {
//                    case let .failure(error):
//                        print(error)
//                        serviceResponse.failed(error: .failedRequest)
//                        serviceResponse.loading(loading: false)
//                        observer.onError(error)
//                    case let .success(subscription):
//                        print(subscription)
//                        serviceResponse.success(response: subscription)
//                        serviceResponse.loading(loading: false)
//                        observer.onNext(subscription)
//                    }
//                }
//            return Disposables.create {
//            }
//        }
//
//    }
//
    static func getBanks()-> [Bank]?{
         if let path = Bundle.main.path(forResource: "Banks", ofType: "json")  {
            print(path)
             do {
                 let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                 let decoder = JSONDecoder()
                 let jsonData = try decoder.decode(BankResponse.self, from: data)
                return  jsonData
             }
             catch {
                print(error)
                 // handle error
             }
         }
         return nil
     }
//    static func submitSubscribtion(token:String,userId:Int,data:SubscriptionSubmissionData,serviceResponse:ServiceResponder){
//        serviceResponse.loading(loading: true)
//        var generatedToken:String{
//            if !token.contains("Bearer"){
//                return "Bearer \(token)"
//            }else{
//                return token
//            }
//        }
//        print(userId)
//
//        guard let url = URL(string:"\(baseUrl)/api/subscription/subscribe/\(userId)") else {return}
//        print(url)
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue(token, forHTTPHeaderField: "Authorization")
//        do {
//            let jsonBody = try JSONEncoder().encode(data)
//            request.httpBody = jsonBody
//            print("jsonBody:",jsonBody)
//            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
//            print("JSON String : ", jsonBodyString!)
//        } catch let err  {
//            print("jsonBody Error: ",err)
//        }
//        let session = URLSession.shared
//        let task = session.dataTask(with: request){ (data,response,err) in
//
//            guard let data = data else {return}
//
//            do{
//                let results = try JSONDecoder().decode(SubscriptionSubmissionDataResponse.self, from: data)
//
//                DispatchQueue.main.async {
//                    print("json::\(json)")
//                    serviceResponse.success(response: results)
//                    serviceResponse.loading(loading: false)
//                }
//                print("DATA:\(data)")
//                print("DATA:\(results)")
//            }catch let err{
//                DispatchQueue.main.async {
//                    serviceResponse.failed(error: .failedRequest)
//                    serviceResponse.loading(loading: false)
//                }
//
//                print("Session Error: ",err)
//            }
//        }
//        task.resume()
//    }
//
    
    
    
    static func filter(token:String,filter:SearchList,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        guard let url = URL(string:"\(baseUrl)/api/subscription/search/offers") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
        do {
            let jsonBody = try JSONEncoder().encode(filter)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(InstrumentResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    
    
    
    static func submitSubscribtion(token:String,userId:Int,data:SubscriptionSubmissionData,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        serviceResponse.loading(loading: true)
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        AF.request("\(baseUrl)/api/subscription/subscribe/\(userId)",
                   method: .post,
                   parameters: data,
                   encoder: JSONParameterEncoder.default,headers: headers).response{ response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else { return }
                        do {
                            let decoder = JSONDecoder()
                            let results = try decoder.decode(SubscriptionSubmissionDataResponse.self, from: data)
                            print(results)
                            serviceResponse.success(response: results)
                            serviceResponse.loading(loading: false)
                            
                        } catch let error {
                            serviceResponse.failed(error: .invalidData)
                            serviceResponse.loading(loading: false)
                            print(error)
                        }
                    case .failure(let error):
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        print(error)
                    }
                   }
        
    }
    
    
    static func getExistingSub(token:String,userId:Int,serviceResponse:ServiceResponder)->Observable<GetExistingSubResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(baseUrl)/api/subscription/data/\(userId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: GetExistingSubResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                       
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    
    
    static func reSubscribe(token:String,subID:Int,serviceResponse:ServiceResponder)->Observable<ResubscribeResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            
            let task = AF.request("\(baseUrl)/api/subscription/ReSubscrib?subId=\(subID)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: ResubscribeResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                       
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
 
    static func getPayment(token:String,transRef:String,serviceResponse:ServiceResponder)->Observable<GetPaymentResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(baseUrl)/api/transactions/verify/\(transRef)",headers: headers)
            task.validate(statusCode: 200..<500)
                .responseDecodable(of: GetPaymentResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print(error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(subscription):
                        print(subscription)
                        serviceResponse.success(response: subscription)
                        serviceResponse.loading(loading: false)
                        observer.onNext(subscription)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    
    
    static func filterSubscription(token:String,filter:SubFilter,serviceResponse:ServiceResponder)->Observable<AllSubDataResponse>{
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        
     
        
        return Observable.create{observer -> Disposable in
            
            AF.request("\(self.baseUrl)/api/subscription/filter/GetSubHistoryByQuery",
                       method: .post,
                       parameters: filter,
                       encoder: JSONParameterEncoder.default,headers: headers).response{ response in
                        switch response.result {
                        case .success:
                            guard let data = response.data else { return }
                            do {
                                let decoder = JSONDecoder()
                                let results = try decoder.decode(AllSubDataResponse.self, from: data)
                                print(results)
                                serviceResponse.success(response: results)
                                serviceResponse.loading(loading: false)
                                observer.onNext(results)
                                
                            } catch let error {
                                serviceResponse.failed(error: .invalidData)
                                serviceResponse.loading(loading: false)
                                print(error)
                                observer.onError(error)
                            }
                        case .failure(let error):
                            serviceResponse.failed(error: .failedRequest)
                            serviceResponse.loading(loading: false)
                            observer.onError(error)
                            print(error)
                        }
                       }
            return Disposables.create {
            }
        }
        
    }
    
}

