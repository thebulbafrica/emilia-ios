//
//  Profile.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//


import Foundation
import RxSwift
import Alamofire

class ProfileRoute{
    
    private static let baseUrl = Utils.baseUrl
    
//
//    static func updateProfile(token:String,userId:Int,data:UpdateProfile,serviceResponse:ServiceResponder){
//        serviceResponse.loading(loading: true)
//        var generatedToken:String{
//            if !token.contains("Bearer"){
//                return "Bearer \(token)"
//            }else{
//                return token
//            }
//        }
//        guard let url = URL(string:"\(baseUrl)/api/account/update/\(userId)") else {return}
//        var request = URLRequest(url: url)
//        request.httpMethod = "PUT"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
//        do {
//            let jsonBody = try JSONEncoder().encode(data)
//            request.httpBody = jsonBody
//            print("jsonBody:",jsonBody)
//            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
//            print("JSON String : ", jsonBodyString!)
//        } catch let err  {
//            print("jsonBody Error: ",err)
//        }
//        let session = URLSession.shared
//        let task = session.dataTask(with: request){ (data,response,err) in
//
//            guard let data = data else {return}
//
//            do{
//                let results = try JSONDecoder().decode(UpdateProfileResponse.self, from: data)
//                DispatchQueue.main.async {
//                    serviceResponse.success(response: results)
//                    serviceResponse.loading(loading: false)
//                }
//                print("DATA:\(data)")
//                print("DATA:\(results)")
//            }catch let err{
//                DispatchQueue.main.async {
//                    serviceResponse.failed(error: .failedRequest)
//                    serviceResponse.loading(loading: false)
//                }
//
//                print("Session Error: ",err)
//            }
//        }
//        task.resume()
//    }
//
//
    
    static func updateProfile(token:String,userId:Int,data:UpdateProfile,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        serviceResponse.loading(loading: true)
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        AF.request("\(baseUrl)/api/account/update/\(userId)",
                   method: .put,
                   parameters: data,
                   encoder: JSONParameterEncoder.default,headers: headers).response{ response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else { return }
                        do {
                            let decoder = JSONDecoder()
                            let results = try decoder.decode(UpdateProfileResponse.self, from: data)
                            print(results)
                            serviceResponse.success(response: results)
                            serviceResponse.loading(loading: false)
                            
                        } catch let error {
                            serviceResponse.failed(error: .invalidData)
                            serviceResponse.loading(loading: false)
                            print(error)
                        }
                    case .failure(let error):
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        print(error)
                    }
                   }
        
    }
    
    
    
    
    static func getUserProfile(token:String,userId:Int,serviceResponse:ServiceResponder)->Observable<UserProfileResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/account/profile/\(userId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: UserProfileResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print("error: ",error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(instrument):
                        observer.onNext(instrument)
                        serviceResponse.success(response: instrument)
                        serviceResponse.loading(loading: false)
                        
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    
    
    static func getallSettings (token:String)->Observable<SettingsResponse> {
       
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/Others/LinksAndContacts",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: SettingsResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print("error: ",error)
                        observer.onError(error)
                    case let .success(instrument):
                        observer.onNext(instrument)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    
}
