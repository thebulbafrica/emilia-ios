//
//  NotificationRoute.swift
//  Emillia
//
//  Created by apple on 01/09/2021.
//

import Foundation
import Alamofire
import RxSwift

class NotificationRoute {
    
    let loading: PublishSubject<Bool> = PublishSubject()
   // let error : PublishSubject<ServiceError> = PublishSubject()
   // let success: PublishSubject<Bool> = PublishSubject()
    let allNotification:PublishSubject<[Notifications]> = PublishSubject()
    let errors : PublishSubject<CustomError> = PublishSubject()
    let logOutDevice:PublishSubject<Bool> = PublishSubject()
    private let apiClient = APIClient()
    private let disposeBag = DisposeBag()
    
    //private let baseUrl = Utils.baseUrl
    
    func getNotification(token:String,unUsed:UnKnown?){
        
        
        self.loading.onNext(true)

        
        
        
      let result =  apiClient.send(path: "/api/Others/GetAllNotification", method: .get, token: token, parameters: unUsed) as Observable<NotificationResponse>
        
      
        
        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            if result.status{
                if let data = result.data{
                    self.allNotification.onNext(data)
                }else{
                    self.errors.onNext(.serverMessage(result.statusMessage))
                }
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {

        }.disposed(by: disposeBag)
        
    }
    
    func logoutUser(userID:Int,token:String,user:UnKnown?){
      
        loading.onNext(true)

        let result = apiClient.send(path: "/api/login/Logout/\(userID)", method: .get, token: token,parameters: user) as Observable<LogOutResponse>


        result.subscribe {[weak self] result in
            guard let self = self else { return }
            self.loading.onNext(false)
            if result.status{
                self.logOutDevice.onNext(true)
            }else{
                self.errors.onNext(.serverMessage(result.statusMessage))
            }
        } onError: { error in
            self.loading.onNext(false)
            switch error as! APIClient.RequestError{
            case .serverError:
                self.errors.onNext(.serverMessage("Server Error"))
            case .connectionError:
                self.errors.onNext(.internetError("Connection Error"))
            case .authorizationError:
                self.errors.onNext(.serverMessage("Unable to Complete this request"))
            }
        } onCompleted: {
            self.loading.onNext(false)
        } onDisposed: {

        }.disposed(by: disposeBag)
    }
}



