//
//  DashBoardRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation

import Foundation
import RxSwift
import Alamofire


class DashBoardRoute{
    
    private static let baseUrl = Utils.baseUrl
    
    static func getUserDashBoard(token:String,id:Int,serviceResponse:ServiceResponder)->Observable<HomeResponse> {
        
        var generatedToken:String{
                   if !token.contains("Bearer"){
                       return "Bearer \(token)"
                   }else{
                       return token
                   }
               }
         let headers: HTTPHeaders = [
             .authorization(generatedToken),
             .accept("application/json")
         ]
        
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/user/summaries/\(id)",headers: headers)
            task.validate(statusCode: 200..<400)
                .responseDecodable(of: HomeResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                    case let .success(dashBoard):
                        serviceResponse.success(response: dashBoard)
                        serviceResponse.loading(loading: false)
                        observer.onNext(dashBoard)
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
}
