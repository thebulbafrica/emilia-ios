//
//  TransactionRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation
import RxSwift
import Alamofire


enum ApiResult {
    case success(Data)
    case failure(RequestError)
}

enum RequestError: Error {
    case unknownError
    case connectionError
    case invalidRequest(String)
    case notFound
    case invalidResponse
    
    case serverUnavailable
}

class TransactionRoute{
    
    private static let baseUrl = Utils.baseUrl
    
    static func getallTransaction(token:String,userId:Int,serviceResponse:ServiceResponder)->Observable<TransacationResponse> {
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        serviceResponse.loading(loading: true)
        return Observable.create{observer -> Disposable in
            let task = AF.request("\(self.baseUrl)/api/transactions/\(userId)",headers: headers)
            task.validate(statusCode: 200..<600)
                .responseDecodable(of: TransacationResponse.self){ response in
                    switch response.result {
                    case let .failure(error):
                        print("error: ",error)
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        observer.onError(error)
                        
                    case let .success(instrument):
                        observer.onNext(instrument)
                        serviceResponse.success(response: instrument)
                        serviceResponse.loading(loading: false)
                        
                    }
                }
            return Disposables.create {
            }
        }
        
    }
    
    static func filterTransaction(token:String,filter:Filter,serviceResponse:ServiceResponder)->Observable<TransacationResponse>{
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        let headers: HTTPHeaders = [
            .authorization(generatedToken),
            .accept("application/json")
        ]
        
        print(filter)
   
        
        return Observable.create{observer -> Disposable in
            
            AF.request("\(self.baseUrl)/api/transactions/GetTransactions_v2",
                       method: .post,
                       parameters: filter,
                       encoder: JSONParameterEncoder.default,headers: headers).response{ response in
                        switch response.result {
                        case .success:
                            guard let data = response.data else { return }
                            do {
                                let decoder = JSONDecoder()
                                let results = try decoder.decode(TransacationResponse.self, from: data)
                                print(results)
                                serviceResponse.success(response: results)
                                serviceResponse.loading(loading: false)
                                observer.onNext(results)
                                
                            } catch let error {
                                serviceResponse.failed(error: .invalidData)
                                serviceResponse.loading(loading: false)
                                print(error)
                                observer.onError(error)
                            }
                        case .failure(let error):
                            serviceResponse.failed(error: .failedRequest)
                            serviceResponse.loading(loading: false)
                            observer.onError(error)
                            print(error)
                        }
                       }
            return Disposables.create {
            }
        }
        
    }
}


struct Filter: Codable {
    let endDate, startDate: String?
    let userID: Int
    let status: String?
    let sendToMail: Int
}
