//
//  OTPRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

class OTPRoute{
    
    private static let baseUrl = Utils.baseUrl

    
    static func otpConfirmation(user:OTP,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        guard let url = URL(string:"\(baseUrl)/api/account/verify") else {return}
           var request = URLRequest(url: url)
           request.httpMethod = "POST"
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           do {
               let jsonBody = try JSONEncoder().encode(user)
               request.httpBody = jsonBody
               print("jsonBody:",jsonBody)
               let jsonBodyString = String(data: jsonBody, encoding: .utf8)
               print("JSON String : ", jsonBodyString!)
           } catch let err  {
               print("jsonBody Error: ",err)
           }
           let session = URLSession.shared
           let task = session.dataTask(with: request){ (data,response,err) in

               guard let data = data else {return}

               do{
                   let results = try JSONDecoder().decode(OTPResponse.self, from: data)
                DispatchQueue.main.async {
                serviceResponse.success(response: results)
                serviceResponse.loading(loading: false)
                }
                   print("DATA:\(data)")
                print("DATA:\(results)")
               }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
              
                   print("Session Error: ",err)
               }
           }
           task.resume()
       }
    
    
    
    static func resendOtp(user:PasswordRecover,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        guard let url = URL(string:"\(baseUrl)/api/account/resend/registration/code?email=\(user.email)") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
           let session = URLSession.shared
           let task = session.dataTask(with: request){ (data,response,err) in
               guard let data = data else {return}

               do{
                   let results = try JSONDecoder().decode(resetOTPResponse.self, from: data)
                DispatchQueue.main.async {
                serviceResponse.success(response: results)
                serviceResponse.loading(loading: false)
                }
                   print("DATA:\(data)")
                print("DATA:\(results)")
               }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
              
                   print("Session Error: ",err)
               }
           }
           task.resume()
       }
}
