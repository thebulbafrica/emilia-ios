//
//  LoginRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import Foundation
import RxSwift
import Alamofire

class LoginRoute {
 
    private static let baseUrl = Utils.baseUrl

  
    
    static func login(user:Login,serviceResponse:ServiceResponder) {
        print(user)
        serviceResponse.loading(loading: true)
        AF.request("\(baseUrl)/api/login",
                   method: .post,
                   parameters: user,
                   encoder: JSONParameterEncoder.default).response{ response in
                    switch response.result {
                    
                    case .success:
                        guard let data = response.data else { return }
                        do {
                            let decoder = JSONDecoder()
                            let results = try decoder.decode(LoginResponse.self, from: data)
                            print(results)
                            serviceResponse.success(response: results)
                            serviceResponse.loading(loading: false)
                            
                        } catch let error {
                            serviceResponse.failed(error: .invalidData)
                            serviceResponse.loading(loading: false)
                            print(error)
                           
                        }
                    case .failure(let error):
                      
                        serviceResponse.failed(error: .failedRequest)
                        serviceResponse.loading(loading: false)
                        print(error)
                    }
                   }
        
    }
    

    
}
