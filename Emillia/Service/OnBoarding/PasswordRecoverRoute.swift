//
//  PasswordRecoverRoute.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation


class PasswordRecoverRoute{
    
    private static let baseUrl = Utils.baseUrl
    
    
    static func enterEmail(user:PasswordRecover,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        guard let url = URL(string:"\(baseUrl)/api/login/forgotpassword/code?request=\(user.email)") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(ForgotPasswordResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    
    static func enterOTP(user:OTP,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        guard let url = URL(string:"\(baseUrl)/api/login/forgotpassword/verify/code") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(ForgotPasswordResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    
    static func resetPssword(user:NewPassword,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        guard let url = URL(string:"\(baseUrl)/api/login/forgotpassword") else {return}
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(ForgotPasswordResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
    
    
    
    
    static func changePassword(token:String,user:ChangePassword,id:Int,serviceResponse:ServiceResponder){
        serviceResponse.loading(loading: true)
        var generatedToken:String{
            if !token.contains("Bearer"){
                return "Bearer \(token)"
            }else{
                return token
            }
        }
        guard let url = URL(string:"\(baseUrl)/api/login/changepassword/\(id)") else {return}
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(generatedToken, forHTTPHeaderField: "Authorization")
        do {
            let jsonBody = try JSONEncoder().encode(user)
            request.httpBody = jsonBody
            print("jsonBody:",jsonBody)
            let jsonBodyString = String(data: jsonBody, encoding: .utf8)
            print("JSON String : ", jsonBodyString!)
        } catch let err  {
            print("jsonBody Error: ",err)
        }
        let session = URLSession.shared
        let task = session.dataTask(with: request){ (data,response,err) in
            guard let data = data else {return}
            
            do{
                let results = try JSONDecoder().decode(ForgotPasswordResponse.self, from: data)
                DispatchQueue.main.async {
                    serviceResponse.success(response: results)
                    serviceResponse.loading(loading: false)
                }
                print("DATA:\(data)")
                print("DATA:\(results)")
            }catch let err{
                DispatchQueue.main.async {
                    serviceResponse.failed(error: .failedRequest)
                    serviceResponse.loading(loading: false)
                }
                
                print("Session Error: ",err)
            }
        }
        task.resume()
    }
}
