//
//  CardVaidator.swift
//  Emillia
//
//  Created by Anthony Odu on 26/05/2021.
//

import Foundation
import UIKit

enum CardType: CaseIterable {
    case AmericanExpress,DinersClub,Discover,JCB,Visa,VERVE,MasterCard,Unknown
    
    var regex: String {
        switch self {
        case .Visa:
            return "^4[0-9]{12}(?:[0-9]{3})?$"
        case .MasterCard:
            return "^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$"
        case .DinersClub:
            return "^3(?:0[0-5]|[68][0-9])[0-9]{11}$"
        case .Discover:
            return "^6(?:011|5[0-9]{2})[0-9]{12}$"
        case .JCB:
            return "^(?:2131|1800|35[0-9]{3})[0-9]{11}$"
        case .AmericanExpress:
            return "^3[47][0-9]{13}$"
        case .VERVE:
            return "^((506(0|1))|(507(8|9))|(6500))[0-9]{12,15}$"
        default:
            return ""
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .MasterCard:
            return UIImage(named: "master")
        case .Visa:
            return UIImage(named: "visa")
        case .VERVE:
            return UIImage(named: "verve")
        default:
            return nil
        }
    }
}

struct CardValidator {
    
    fileprivate static func matchesRegex(regex: String, text: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text, range: NSRange(text.startIndex..., in: text))
            return results.map {
                String(text[Range($0.range, in: text)!])
            }.count > 0
        } catch {
            return false
        }
    }
    
    static func getCardType(number: String) -> CardType {
        for card in CardType.allCases {
            if (matchesRegex(regex: card.regex, text: number)) {
                return card
            }
        }
        return .Unknown
    }
    
    static func luhnCheck(_ cardNumber: String) -> Bool {

        guard let _ = Int64(cardNumber) else {
            //if string is not convertible to int, return false
            return false
        }
        let numberOfChars = cardNumber.count
        let numberToCheck = numberOfChars % 2 == 0 ? cardNumber : "0" + cardNumber
        
        let digits = numberToCheck.map { Int(String($0)) }
        
        let sum = digits.enumerated().reduce(0) { (sum, val: (offset: Int, element: Int?)) in
            if (val.offset + 1) % 2 == 1 {
                let element = val.element!
                return sum + (element == 9 ? 9 : (element * 2) % 9)
            }
            // else
            return sum + val.element!
        }
        let validates = sum % 10 == 0
        
        return validates
    }

}
