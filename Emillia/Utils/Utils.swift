//
//  Utils.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class Utils {
    //C4047594IS
    static let baseUrl = "http://thebulbafricaemilliauiapi-dev.us-east-1.elasticbeanstalk.com"
       // "https://dmomobile.cscs.ng/Middleware"
    //static let baseUrl2 = "thebulbafricaemilliauiapi-test.us-east-1.elasticbeanstalk.com"
    
    static func timeZoneForWallet(date:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
        if let userDate = formatter.date(from: date) {
            formatter.dateFormat = "dd-MMM-yyyy hh:mm a"
            //formatter.dateFormat = "MMM dd,yyyy"
            let formattedDate = formatter.string(from: userDate)
            return formattedDate
        }
        return ""
    }
    
    static func timeZone(date:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
        if let userDate = formatter.date(from: date) {
            formatter.dateFormat = "dd-MMM-yyyy"
            //formatter.dateFormat = "MMM dd,yyyy"
            let formattedDate = formatter.string(from: userDate)
            return formattedDate
        }
        return ""
    }
    
    static func justDate(date:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
        if let userDate = formatter.date(from: date) {
            let today = Calendar.current.isDateInToday(userDate)
            let yesterday = Calendar.current.isDateInYesterday(userDate)
            if today{
                return "Today"
            }else if yesterday{
                return "Yesterday"
            }else{
                formatter.dateFormat = "MMM dd"
                //formatter.dateFormat = "MMM dd,yyyy"
                let formattedDate = formatter.string(from: userDate)
                return formattedDate
            }
           
        }
        return ""
    }
    
    static func saveToken(token:String) {
        UserDefaults.standard.set(token, forKey: "token")
    }
    
    static func getToken() -> String {
        var result = ""
        if let token =  UserDefaults.standard.string(forKey: "token"){
            result = token
        }
        return result
    }
    
    static func saveUserId(id:Int) {
        UserDefaults.standard.set(id, forKey: "id")
    }
    
    static func getUserId() -> Int {
        let id =  UserDefaults.standard.integer(forKey: "id")
        return id
    }
    
    static func saveUserPhone(phone:Int) {
        UserDefaults.standard.set(phone, forKey: "phone")
    }
    
    static func getUserPhone() -> Int {
        let id =  UserDefaults.standard.integer(forKey: "phone")
        return id
    }
    
    static func saveUserEmail(email:String) {
        UserDefaults.standard.set(email, forKey: "email")
    }
    
    static func getUserEmail() -> String {
        return UserDefaults.standard.string(forKey: "email") ?? ""
    }
    
    static func saveLogin(allowlogin:Bool){
        UserDefaults.standard.set(allowlogin, forKey: "allowlogin")
    }
    
    
    static func saveUserDetails(user:UserDetails) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey:"user")
        //UserDefaults.standard.set(user, forKey: "user")
    }
    
    func saveBiometrics(allow:Bool) {
        UserDefaults.standard.set(allow, forKey: "allowBiometric")
    }
    
    
    func getBiometric() -> Bool {
        var result = false
        let allowlogin = UserDefaults.standard.bool(forKey: "allowBiometric")
        result = allowlogin
        return result
    }
    
    func saveDevice(allow:Bool) {
        UserDefaults.standard.set(allow, forKey: "saveDevice")
    }
    
    
    func getDevice() -> Bool {
        var result = false
        let allowlogin = UserDefaults.standard.bool(forKey: "saveDevice")
        result = allowlogin
        return result
    }
    
    static func getUserUserdetails() -> UserDetails? {
        if let data = UserDefaults.standard.value(forKey:"user") as? Data {
            let songs2 = try? PropertyListDecoder().decode(UserDetails.self, from: data)
            
            return songs2
        }else{
            return nil
        }
    }
    
    
    static func saveSubscriptionDetails(user:SubscriptionSubmissionData) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey:"subscribe")
        //UserDefaults.standard.set(user, forKey: "user")
    }
    
    static func getSubscriptionDetails() -> SubscriptionSubmissionData? {
        if let data = UserDefaults.standard.value(forKey:"subscribe") as? Data {
            let subscription = try? PropertyListDecoder().decode(SubscriptionSubmissionData.self, from: data)
            return subscription
        }else{
            return nil
        }
    }
    
    static func saveLastSubscriptionDetails(user:SubscriptionData2) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey:"lastSubscription")
        //UserDefaults.standard.set(user, forKey: "user")
    }
    
    static func getLastSubscriptionDetails() -> SubscriptionData2? {
        if let data = UserDefaults.standard.value(forKey:"lastSubscription") as? Data {
            let subscription = try? PropertyListDecoder().decode(SubscriptionData2.self, from: data)
            return subscription
        }else{
            return nil
        }
    }
    
    
    
    static func saveUserChnn(chn:String) {
        UserDefaults.standard.set(chn, forKey: "chn")
    }
    
    static func getUserChn() -> String {
        var result = ""
        if let chn =  UserDefaults.standard.string(forKey: "chn"){
            result = chn
        }
        return result
    }
    static func getAccountName() -> String{
        var result = ""
        if let accountName =  UserDefaults.standard.string(forKey: "accountName"){
            result = accountName
        }
        return result
    }
    static func getBVNName() -> String{
        var result = ""
        if let bvnName =  UserDefaults.standard.string(forKey: "bvnName"){
            result = bvnName
        }
        return result
    }
    static func saveBVNName(bvnName:String){
        UserDefaults.standard.set(bvnName, forKey: "bvnName")
    }
    static func saveAccountName(accountName:String){
        UserDefaults.standard.set(accountName, forKey: "accountName")
    }
    
    static func saveUserCscs(cscs:String) {
        UserDefaults.standard.set(cscs, forKey: "cscs")
    }
    
    static func getUserCscs() -> String {
        var result = ""
        if let cscs =  UserDefaults.standard.string(forKey: "cscs"){
            result = cscs
        }
        return result
    }
    
    static func saveTime(time:String) {
           UserDefaults.standard.set(time, forKey: "time")
           //keychain.set(token, forKey: "token")
       }
       
    static func saveTime2(time:String) {
              UserDefaults.standard.set(time, forKey: "time2")
              //keychain.set(token, forKey: "token")
          }
       
    static func getTime() -> String {
          var result = ""
              if let time =  UserDefaults.standard.string(forKey: "time"){
                  result = time
              }
              return result
       }
       
    static func getTime2() -> String {
             var result = ""
                 if let time =  UserDefaults.standard.string(forKey: "time2"){
                     result = time
                 }
                 return result
          }
    
    
    static func timeZoneForWallet2(date:String) -> String {
        let formatter = DateFormatter()
        let formatedDate = date.split(separator: ".")[0]
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //.SSSSSSS'+'HH:mm"
        if let userDate = formatter.date(from: String(formatedDate)) {
            formatter.dateFormat = "dd/MM/yyyy"
            //formatter.dateFormat = "MMM dd,yyyy"
            let formattedDate = formatter.string(from: userDate)
            return formattedDate
        }
        return ""
    }
    
    static func timezoneMonthDayYear(date:String) -> String {
        let formatter = DateFormatter()
        let formatedDate = date.split(separator: ".")[0]
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //.SSSSSSS'+'HH:mm"
        if let userDate = formatter.date(from: String(formatedDate)) {
            formatter.dateFormat = "MMM dd,yyyy"
            //formatter.dateFormat = "MMM dd,yyyy"
            let formattedDate = formatter.string(from: userDate)
            return formattedDate
        }
        return ""
    }
  
    static private let tensNames = [
                "",
                " ten",
                " twenty",
                " thirty",
                " forty",
                " fifty",
                " sixty",
                " seventy",
                " eighty",
                " ninety"
        ]
        static private let numNames = [
                "",
                " one",
                " two",
                " three",
                " four",
                " five",
                " six",
                " seven",
                " eight",
                " nine",
                " ten",
                " eleven",
                " twelve",
                " thirteen",
                " fourteen",
                " fifteen",
                " sixteen",
                " seventeen",
                " eighteen",
                " nineteen"
        ]
         static private func convertLessThanOneThousand(_number: Int)-> String {
            var number = _number
            var soFar: String
            if (number % 100 < 20) {
                soFar = numNames[number % 100]
                number /= 100
            } else {
                soFar = numNames[number % 10]
                number /= 10
                soFar = tensNames[number % 10] + soFar
                number /= 10
            }

            if number == 0{
                return soFar
            }else if soFar.isEmpty{
              return  numNames[number] + " hundred"
            }else{
               return numNames[number] + " hundred" + " and" + soFar
            }

        }


        static func convert(number: NSNumber)-> String {
            
            if (number == 0) {
                return "zero"
            }
            let snumber: String
            let numberFormatter = NumberFormatter()
            numberFormatter.positiveFormat = "000000000000"

            snumber = numberFormatter.string(from: number)!

            var billions = ""
            var millions = ""
            var hundredThousands = ""
            var thousands = ""

            for i in String(snumber) {
                
                if billions.count < 3 {
                    billions += String(i)
                }else if millions.count < 3 {
                    millions += String(i)
                }else if hundredThousands.count < 3 {
                    hundredThousands += String(i)
                }else if thousands.count < 3 {
                    thousands += String(i)
                }
            }
            
            var tradBillions: String
            
            if Int(billions) == 0{
                tradBillions = ""
            }else if Int(billions) == 1{
                tradBillions = convertLessThanOneThousand(_number: Int(billions)!) + " billion"
            }else{
                tradBillions = convertLessThanOneThousand(_number: Int(billions)!) + " billion"
            }
            
            var result = tradBillions
            
            
            var tradMillions: String
            
            if Int(millions) == 0{
                tradMillions = ""
            }else if Int(millions) == 1{
                tradMillions = convertLessThanOneThousand(_number: Int(millions)!) + " million"
            }else{
                tradMillions =  convertLessThanOneThousand(_number: Int(millions)!)  + " million"
            }
            
            result += tradMillions
            var tradHundredThousands: String
            
            
            if Int(hundredThousands) == 0{
                tradHundredThousands = ""
            }else if Int(tradMillions) == 1{
                tradHundredThousands = "one thousand "
            }else{
                tradHundredThousands = convertLessThanOneThousand(_number: Int(hundredThousands)!)  + " thousand"
            }
            result += tradHundredThousands
            var tradThousand: String
            
            tradThousand = convertLessThanOneThousand(_number: Int(thousands)!)
            
            result += tradThousand
            // remove extra spaces!
            return result + " Naira"
            //return result.replace("^\\s+".toRegex(), "").replace("\\b\\s{2,}\\b".toRegex(), " ").capitalize()
        }

    func clearAllSavedData(){
        UserDefaults.standard.removeObject(forKey: "allowBiometric")
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: "userPassword")
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: "userPassword")
    }
}
