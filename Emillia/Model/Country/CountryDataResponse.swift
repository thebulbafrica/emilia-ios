//
//  CountryDataResponse.swift
//  Emillia
//
//  Created by Anthony Odu on 09/03/2021.
//

import Foundation


struct CountryDataResponse: Codable {
    let countries: [Country]

    enum CodingKeys: String, CodingKey {
        case countries = "Countries"
    }
}

// MARK: - Country
struct Country: Codable {
    let states: [State]
    let countryName: String

    enum CodingKeys: String, CodingKey {
        case states = "States"
        case countryName = "CountryName"
    }
}

// MARK: - State
struct State: Codable {
    let cities: [String]
    let stateName: String

    enum CodingKeys: String, CodingKey {
        case cities = "Cities"
        case stateName = "StateName"
    }
}

struct AllCountry: Codable {
    let id: Int
    let name: String
}

typealias allcountry = [AllCountry]

struct AllState: Codable {
    let id, countryid: Int
    let name: String
}

typealias allState = [AllState]


struct AllCity: Codable {
    let id, stateid: Int
    let name: String
}

typealias allCity = [AllCity]
