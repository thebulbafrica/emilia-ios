//
//  UserProfile.swift
//  Emillia
//
//  Created by Anthony Odu on 14/03/2021.
//

import Foundation

struct UserProfileResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: UserData
    let extras: String?
}

// MARK: - DataClass
struct UserData: Codable {
    let id: Int
    let title, firstname, lastname, othername: String?
    let email, phone, address, address2: String?
    let city, state, country: Int?
    let company, rcno, createdate: String?
    let uid: Int
}


struct UpdateProfile: Codable {
    let title, firstName, lastName, otherName: String?
    let address, address2: String?
    let city, state, country: Int?
    let company, rcno: String?
}

struct UpdateProfileResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: UpdateProfileData?
    let extras: String?
}

// MARK: - DataClass
struct UpdateProfileData: Codable {
    let id: Int
    let username: String?
}



struct SettingsResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: AllSettings?
    let extras: String?
}

// MARK: - DataClass
struct AllSettings: Codable {
    let cookie, tutorialVideo, faq, privacy, termandCondition: String?
    let contacts: Contacts
}

// MARK: - Contacts
struct Contacts: Codable {
    let emials, phone, address: String?
}
