//
//  GetCallbackResponse.swift
//  Emillia
//
//  Created by MAC on 27/07/2022.
//

import Foundation

struct GetCallbackResponse: Codable {
    var statusMessage: String?
    var statusCode: Int?
    var status: Bool?
    var data: GetCallbackResponseData?
    var extras: String?
}

// MARK: - GetCallbackResponseData
struct GetCallbackResponseData: Codable {
    var status: Bool?
    var message: String?
    var data: ResponseData?
}

// MARK: - DataData
struct ResponseData: Codable {
    var id: Int?
    var txRef, flwRef, deviceFingerprint, amount: String?
    var currency, chargedAmount, appFee, merchantFee: String?
    var status, paymentType, createdAt, accountID: String?
    var amountSettled, customer, paymentCode: String?
    var authorizationURL: String?
    var accessCode, reference: String?

    enum CodingKeys: String, CodingKey {
        case id
        case txRef = "tx_Ref"
        case flwRef = "flw_Ref"
        case deviceFingerprint = "device_Fingerprint"
        case amount, currency
        case chargedAmount = "charged_Amount"
        case appFee = "app_Fee"
        case merchantFee = "merchant_Fee"
        case status
        case paymentType = "payment_Type"
        case createdAt = "created_At"
        case accountID = "account_Id"
        case amountSettled = "amount_Settled"
        case customer
        case paymentCode = "payment_Code"
        case authorizationURL = "authorization_url"
        case accessCode = "access_code"
        case reference
    }
}
