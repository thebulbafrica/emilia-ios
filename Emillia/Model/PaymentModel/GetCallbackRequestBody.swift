//
//  GetCallbackRequestBody.swift
//  Emillia
//
//  Created by MAC on 27/07/2022.
//

import Foundation

struct GetCallbackBody: Codable {
    let email, amount, currency, reference: String
    let callbackURL: String

    enum CodingKeys: String, CodingKey {
        case email, amount, currency, reference
        case callbackURL = "callback_url"
    }
}
