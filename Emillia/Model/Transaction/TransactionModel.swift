//
//  TransactionModel.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import Foundation


// MARK: - Welcome
struct TransacationResponse: Codable {
    let statusMessage: String?
    let statusCode: Int?
    let status: Bool
    let data: [TransacationData]
    let extras: String?
}

// MARK: - Datum
struct TransacationData: Codable {
    let id, amount, status: Int
    let transdate, description, code, name: String

}
