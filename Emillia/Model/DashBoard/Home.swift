//
//  Home.swift
//  Emillia
//
//  Created by Anthony Odu on 08/03/2021.
//

import Foundation



struct HomeResponse: Codable {
    let statusMessage: String?
    let statusCode: Int?
    let status: Bool?
    let data: HomeData
    let extras: String?
}

// MARK: - DataClass
struct HomeData: Codable {
    let userDetails: UserDetails
    let totalAmount, totalSubscriptions, totalSuccessfulTransactions, totalFailedTransactions: Int
    let transactions: [HomeTransaction]
}


// MARK: - Transaction
struct HomeTransaction: Codable {
    let id, amount, status: Int
    let transdate, description, code, name: String
}

// MARK: - UserDetails
struct UserDetails: Codable {
    let id: Int?
    let username, firstname, lastname, email: String?
    let phone:String?
    let title: String?
    let status, usertype: Int?
    let companyname: String?
    let rcno, contactfirstName, contactLastName: String?
}
