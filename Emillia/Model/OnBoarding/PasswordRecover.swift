//
//  PasswordRecover.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

struct PasswordRecover:Codable {
    let email:String
}

struct ForgotPasswordResponse: Codable {
    let statusMessage: String?
    let statusCode: Int?
    let status: Bool?
    let data: Int?
    let extras: String?
    
}




struct NewPassword:Codable {
    let userId:Int
    let password:String
}


struct ChangePassword: Codable {
    let oldPassword, newPassword: String
}
