//
//  OnBoardingModel.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import Foundation


struct OnBoardImageAndText {
    let imageName:String
    let text:String
}
