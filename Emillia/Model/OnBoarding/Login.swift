//
//  Login.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

struct Login: Codable {
    let emailAddress, password, deviceID: String
}


struct LoginResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: LoginData?
    let extras: String?
}

// MARK: - DataClass
struct LoginData: Codable {
    let id: Int
    let username: String
    let usertype, status, isphoneverified: Int
    let email: String
}

struct RegisterDevicesResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let extras: String?
}

struct LogOutResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data, extras: String?
}

struct UnKnown: Codable{
    
}
