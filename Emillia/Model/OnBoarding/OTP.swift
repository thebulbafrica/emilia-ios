//
//  OTP.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

struct OTP:Codable {
    let userId: Int
    let oneTimePassword: String
}


// MARK: - Welcome
struct OTPResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: String?
    let extras: String?
    
}


struct resetOTPResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: Int?
}

struct RegisterPhone:Codable {
    let emailAddress:String
    let password:String
    let deviceID:String
}
