//
//  Register.swift
//  Emillia
//
//  Created by Anthony Odu on 07/03/2021.
//

import Foundation

struct Register: Codable {
    var username: String
    var password:String
    var userType: Int
    var title:String?
    var firstname, lastname: String
    var email, phone: String
    var othername, company, rcno:String?
}


struct RegisterResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: RegisterData?
}

// MARK: - DataClass
struct RegisterData: Codable {
    let id: Int
    let username: String
}
