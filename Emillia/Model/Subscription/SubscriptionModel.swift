//
//  SubscriptionModel.swift
//  Emillia
//
//  Created by Anthony Odu on 05/03/2021.
//

import Foundation

struct Holder{
    let id: String?
    var tenor: String?
    let name: String?
    let date: String?
    let status: String?
    
}

struct InstrumentResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: [AllInstrument]
    let extras: String?
}

// MARK: - Datum
struct AllInstrument: Codable {
    let offer: Offer
    let instrument: Instrument
}

// MARK: - Instrument
struct Instrument: Codable {
    let id: Int
    let name, code, issuer: String
}

// MARK: - Offer
struct Offer: Codable {
    let id: Int
    let offercode: String
    let tenor: Int
    let offerdate: String
    let multiplethereof:Int?
    let minval, maxval, status: Int
    let instrumentid: Int
    let startdate, enddate, currency: String
}



















//
//struct InstrumentResponse: Codable {
//    let statusMessage: String
//    let statusCode: Int
//    let status: Bool
//    let data: [Instrument]
//    let extras: String?
//}
//
//// MARK: - Datum
//struct Instrument: Codable {
//    let id, accountType, brokerName, firstName: String
//    let lastName, emailAddress, phoneNumber, address: String
//    let alternativeAddress, city, state, postCode: String
//    let country, bankName, accountName, accountNumber: String
//    let instrumentDueDate, chn, cscsNumber: String
//    let status: Bool
//    let bvn, bvnName, instrumentName, instrumentCode: String
//    let unit, amount: Int
//}









// MARK: - Welcome
struct SubscribtionResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: [Subscription]
}

// MARK: - Datum
struct Subscription: Codable {
    let id, accountType, brokerName, firstName: String
    let lastName, emailAddress, phoneNumber, address: String
    let alternativeAddress, city, state, postCode: String
    let country, bankName, accountName, accountNumber: String
    let bvn, bvnName, instrumentName: String
    let unit, amount: Int
    let transactions: [Transaction]
}

// MARK: - Transaction
struct Transaction: Codable {
    let createdDate, lastModifiedDate, deletedDate, id: String
    let amount: Int
    let transactionDate, valueDate, systemDate, transactionStatus: String
    let transactionChannel, paymentRef, ipAddress, currency: String
}

struct BrokersResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: [BrokersData]
    let extras: String?
}

// MARK: - Datum
struct BrokersData: Codable {
    let id: Int
    let name, code: String
}


struct NewBrokersData: Codable {
    let id: Int
    let name, code: String
    var check:Bool? = false
}





struct SubscriptionSubmissionData: Codable {
    var offerid, amount, subamount, totalamount: Int?
    var firstname, lastname, title, occupation: String?
    var phone, nextkinfname, nextkinlname, address: String?
    var address2, passportno, dob, mmaidenname: String?
    var email, bankname, bankacctno, bvnno: String?
    var companyname: String?
    var businesstype: Int?
    var rcno, contactperson, cscsno, chnno: String?
    var brokername, gender, nextkinphone, nextkinemail: String?
    var nextkinrelation, city, state, postcode: String?
    var country, brokercode, nextkinothername: String?
    var bankOpeningDate,contactPersonPhone,taxId,userId: String?
    var isChn:Bool?
    
    
    
}


struct SubmitSubscriptionData: Codable {
    let offerid, amount, subamount, totalamount: Int?
    let firstname, lastname, title, occupation: String?
    let phone, phoneUseForSub, nextkinfname, nextkinlname: String?
    let address, address2, passportno, dob: String?
    let mmaidenname, email, bankname, bankacctno: String?
    let bvnno, companyname: String?
    let businesstype: Int?
    let rcno, contactperson, cscsno, chnno: String?
    let brokername, gender, nextkinphone, nextkinemail: String?
    let nextkinrelation, city, state, postcode: String?
    let country, brokercode, nextkinothername: String?
}


struct SubscriptionSubmissionDataResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data:String?
    let extras: String?
}


struct ValidateChN:Codable {
    let chn:String
}

//struct ValidateChNResponse: Codable {
//    let chn, cscsNo, accountName, responseCode: String
//    let responseMessage: String
//}

struct ValidateChNResponse: Codable {
    let data: [ValidateCHNData]
}



struct ValidateCHNData: Codable {
    let chn: String?
    let response_code: Int?
    let response_message, broker_code, broker_name, cscs: String?
    let bvn, dob, full_name, username: String?
    let webtoken, mobileNo, status, email: String?
    let streetaddress: String?

}



struct SearchList: Codable {
    let status: Int
}




struct BVNData: Codable {
    let bvn, firstName, lastName, middleName: String
    let accountNumber, bankCode: String
}

struct AccountData: Codable {
    let accountNumber, bankCode: String
}

struct BVNValidateResponse: Codable {
    let responseCode, bvn, firstName, middleName: String?
    let lastName, bankCode: String?
}

//struct BVNValidateResponse: Codable {
//    let firstName, bvn, lastName, middleName: String
//    let bankCode, accountNumber: String
//}
// MARK: - Welcome
//struct BankResponse: Codable {
//    let banks: [Bank]
//}
//
//// MARK: - DataClass
//struct BankData: Codable {
//    let banks: [Bank]
//}
//
//
//// MARK: - Bank
//struct Bank: Codable {
//    let id: Int
//    let code, name: String
//}
//struct WelcomeElement: Codable {
//    let id: Int
//    let code, name: String
//}

typealias BankResponse = [Bank]



struct Bank: Codable {
    let id: Int
    let code, name: String
}







struct AllSubDataResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: [AllSubData]?
    let extras: String?
}

// MARK: - Datum
struct AllSubData: Codable {
    let id, status, amount: Int
    let applicationid, issuer, code, name: String
    let cscsno:String?
    let chnno, subdate: String
}

struct GetPaymentResponse: Codable {
    let statusMessage: String?
    let statusCode: Int?
    let status: Bool
    let data, extras: String?
}

struct ResubscribeResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: ResubscribeData?
    let extras: String?
}



struct GetExistingSubResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: SubscriptionData2?
    let extras: String?
}


struct ResubscribeData: Codable {
    let subscriptionData: SubscriptionData2
    let offer: AllInstrument
}


// MARK: - SubscriptionData
struct SubscriptionData2: Codable {
    let id, uid, sid, iscompany: Int?
    let isjoint: Int?
    let firstname, lastname, title, occupation: String?
    let phone, nextkinfname, nextkinlname, address: String?
    let address2, passportno, dob, mmaidenname: String?
    let email, bankname, bankacctno, bvnno: String?
    let companyname: String?
    let businesstype: Int?
    let rcno, contactperson, subdate, cscsno: String?
    let chnno, brokername, gender, nextkinphone: String?
    let nextkinemail, nextkinrelation, city, state: String?
    let postcode, country, brokercode, nextkinothername: String?
}


struct SubFilter: Codable {
    let chnno:String
    let endDate, startDate: String?
    let userID:Int
    let filterWithChn: Int?
    let status, brokerName, instrument: String?
    let sendToMail: Int?

    enum CodingKeys: String, CodingKey {
        case chnno, endDate, startDate
        case userID = "userId"
        case filterWithChn, status, brokerName, instrument, sendToMail
    }
}


// MARK: - Welcome
struct NoTradingResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: String?
    let extras: String?
}

struct NonTradingSendData: Codable {
    let bankName, bankAccNo, bopDate, nxPhone: String
    let gender, madianName, cpName, cpPhone: String
    let bvn, state, taxID: String
    let userID: Int

    enum CodingKeys: String, CodingKey {
        case bankName, bankAccNo, bopDate, nxPhone, gender, madianName, cpName, cpPhone, bvn, state
        case taxID = "taxId"
        case userID = "userId"
    }
}
