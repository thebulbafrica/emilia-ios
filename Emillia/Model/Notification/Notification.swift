//
//  Notification.swift
//  Emillia
//
//  Created by apple on 01/09/2021.
//

import Foundation

import Foundation

struct NotificationResponse: Codable {
    let statusMessage: String
    let statusCode: Int
    let status: Bool
    let data: [Notifications]?
    let extras: String?
}

struct Notifications: Codable {
    let id:Int?
    let title:String?
    let msg:String?
    let createddate:String?
    let firebasetopic:String?
    let userdevice:String?
}

    
