//
//  Validator.swift
//  Emillia
//
//  Created by Anthony Odu on 01/03/2021.
//

import Foundation
class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) throws -> String
}


enum ValidatorType {
    case email
    case password
    case fullname
    case requiredField(field: String)
    case phone
    case codeValidator
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
        case .fullname: return NameValidator()
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        case .phone: return PhoneNumberValidator()
        case .codeValidator: return CodeValidator()
        }
    }
}


struct CodeValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
           guard value != "" else {throw ValidationError("Code is Required")}
          guard value.count == 4 else {throw ValidationError("Invalid Code!")}
          return value
      }
}


class PhoneNumberValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        print(value.count)
         guard value != "" else {throw ValidationError("PhoneNumber is Required")}
        
        if value.count > 11{
            throw ValidationError("Invalid phone number!")
        }else if value.count < 10{
             throw ValidationError("Invalid phone number!")
        }else{
            return value
        }
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String) throws -> String {
        guard !value.isEmpty else {
            throw ValidationError("Required field " + fieldName)
        }
        return value
    }
}

struct NameValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value.count >= 5 else {
            throw ValidationError("Name must contain more than three characters" )
        }
        guard value != "" else {
                  throw ValidationError("Full Name Required" )
              }
        return value
    }
}


struct PasswordValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Password is Required")}
        guard value.count >= 6 else { throw ValidationError("Password must have at least 6 characters") }
        return value
    }
   
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String) throws -> String {
        guard value != "" else {throw ValidationError("Email is Required")}
        do {
            if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid e-mail Address")
            }
        } catch {
            throw ValidationError("Invalid e-mail Address")
        }
        return value
    }
}
