//
//  Pager.swift
//  Emillia
//
//  Created by Anthony Odu on 04/03/2021.
//

import PagingMenuController


struct PagingMenuOptions: PagingMenuControllerCustomizable {
    
    private let viewController1 = InstrumentListViewController.instantiateFromStoryboard()
    private let viewController2 = SubHistroyViewController.instantiateFromStoryboard()
    private let viewController3 = StandingOrderViewController.instantiateFromStoryboard()
    
    var show = false
    
    var componentType: ComponentType {
        return .all(menuOptions: MenuOptions(), pagingControllers: [viewController1,
                                                                    viewController2,
                                                                    viewController3
        ])
    }
    
    
    fileprivate struct MenuOptions: MenuViewCustomizable {
        
        var displayMode: MenuDisplayMode {
            return .segmentedControl
        }
        var focusMode: MenuFocusMode {
            return .roundRect(radius: 12, horizontalPadding: 8, verticalPadding: 8, selectedColor: .white)
        }
        
        var itemsOptions: [MenuItemViewCustomizable] {
            return [MenuItem1(), MenuItem2(),MenuItem3()
            ]
        }
        
        var backgroundColor: UIColor = UIColor(red: 0.74, green: 0.74, blue: 0.74, alpha: 1.00)
        
    }
    
    fileprivate struct MenuItem1: MenuItemViewCustomizable {
    
        var displayMode: MenuItemDisplayMode {
            let text = MenuItemText(text: "Instrument List",
                                    color:.black,
                                    font:.boldSystemFont(ofSize: 12),
                                    selectedFont:.boldSystemFont(ofSize: 10))
            return .text(title: text)
        }
        
    }
    
    fileprivate struct MenuItem2: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            let text = MenuItemText(text: "Subs. History",
                                    color:.black,
                                    font:.boldSystemFont(ofSize: 12),
                                    selectedFont:.boldSystemFont(ofSize: 10))
            return .text(title: text)
        }
        
    }
    
    fileprivate struct MenuItem3: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            
            let text = MenuItemText(text: "Standing Order",
                                    color:.black,
                                    font:.boldSystemFont(ofSize: 12),
                                    selectedFont:.boldSystemFont(ofSize: 10))
            return .text(title: text)
        }
    }
    
    
}

