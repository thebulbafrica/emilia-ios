//
//  InvoiceComposer.swift
//  Emillia
//
//  Created by apple on 02/09/2021.
//

import Foundation
import UIKit
import WebKit

class TransactionComposer: NSObject {

    let pathToTransactionHistroyHTMLTemplate = Bundle.main.path(forResource: "TransactionHistory", ofType: "html")
    
    let pathToSingleItemHTMLTemplate = Bundle.main.path(forResource: "single_item", ofType: "html")
    
    let pathToLastItemHTMLTemplate = Bundle.main.path(forResource: "last_item", ofType: "html")

    var invoiceNumber: String!
    
    func renderInvoice(invoiceNumber: String,name: String, start: String, end: String, items: [[String: String]]) -> String! {
   
        self.invoiceNumber = invoiceNumber
        let image = UIImage(named:"logo") // Your Image
        let imageData = image!.pngData() ?? nil
        let base64String = imageData?.base64EncodedString()
        
        let imageInstagram = UIImage(named:"instagram") // Your Image
        let imageDataInstagram = imageInstagram!.pngData() ?? nil
        let base64StringInstagram = imageDataInstagram?.base64EncodedString()
        
        
        let imageTwitter = UIImage(named:"twitter") // Your Image
        let imageDataTwitter = imageTwitter!.pngData() ?? nil
        let base64StringTwitter = imageDataTwitter?.base64EncodedString()
        
        
        let imageLinken = UIImage(named:"linkedln") // Your Image
        let imageDataLinken = imageLinken!.pngData() ?? nil
        let base64StringLinken = imageDataLinken?.base64EncodedString()
       

        do {
            // Load the invoice HTML template code into a String variable.
            var HTMLContent = try String(contentsOfFile: pathToTransactionHistroyHTMLTemplate!)

            // Replace all the placeholders with real values except for the items.
            // The logo image.
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#USER_NAME#", with: name)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#START_DATE#", with: start)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#END_DATE#", with: end)
            
            HTMLContent = HTMLContent.replacingOccurrences(of: "#LOGO_IMAGE#", with: "data:image/png;base64,\(base64String!)")
            HTMLContent = HTMLContent.replacingOccurrences(of: "#LINKEDLN_IMAGE#", with: "data:image/png;base64,\(base64StringLinken!)")
            HTMLContent = HTMLContent.replacingOccurrences(of: "#INSTAGRAM_IMAGE#", with: "data:image/png;base64,\(base64StringInstagram!)")
            HTMLContent = HTMLContent.replacingOccurrences(of: "#TWITTER_IMAGE#", with: "data:image/png;base64,\(base64StringTwitter!)")
  
            
            var allItems = ""
            for i in 0..<items.count {
                var itemHTMLContent: String!
                
                if i != items.count - 1 {
                    itemHTMLContent = try String(contentsOfFile: pathToSingleItemHTMLTemplate!)
                }
                else {
                    itemHTMLContent = try String(contentsOfFile: pathToLastItemHTMLTemplate!)
                }
                
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_DATE#", with: items[i]["transactionDate"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_NAME#", with: items[i]["transactionName"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_CODE#", with: items[i]["transactionCode"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_DESCRIPTION#", with: items[i]["transactionDesc"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_AMOUNT#", with: items[i]["transactionAmount"]!)
                itemHTMLContent = itemHTMLContent.replacingOccurrences(of: "#TRANSACTION_STATUS#", with: items[i]["transactionStatus"]!)
                
                // Add the item's HTML code to the general items string.
                allItems += itemHTMLContent
            }
            
            // Set the items.
            HTMLContent = HTMLContent.replacingOccurrences(of: "#ITEMS#", with: allItems)
           
     
            // The HTML code is ready.
            return HTMLContent
            
            
        }
        catch {
            print("Unable to open and use HTML template files.")
        }

        return nil
    }

}
