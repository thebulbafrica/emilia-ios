class DB {
  static stationData = [
    { station: "Abuja", type: "Natural", orbit: "Earth"},
    { station: "Spock", type: "Natural", orbit: "Mars"},
    { station: "International Space Station", type: "Manmade", orbit: "Earth"},
    { station: "Moon", type: "Natural", orbit: "Earth"}
  ]


  static getStation(station) {
    return DB.stationData.find((value) => {
      return value.station === station
    })
  }

}

class RocketTravel {


  wallet = 0
  loadBTC (amount) {
    this.wallet += amount
    console.log("your new balance is ", this.wallet)
  }


  trip(rocketType, from, to) {

      const costOfTrip = this.checkOrbit(from, to) * this.checkRocket(rocketType);
      const costOfLanding = this.checkLandingStationType(to);
  
      const value = costOfTrip + costOfLanding;
  
      this.debit(value)

  }

  checkOrbit(from, to) {
    const f = DB.getStation(from);
    const t = DB.getStation(to);
    return f.orbit === t.orbit? 50 : 250;
  }

  checkRocket(rocketType) {
    return rocketType === "falcon9"? 2: 1;
  }

  checkLandingStationType (to) {
    const t = DB.getStation(to);
    return t.type === "Natural"? 0: 200;

  }

  debit (cost) {
    if (cost > this.wallet) {
      console.log('Insufficient fund load more btc') 
      return;
    }

    this.wallet -= cost;
    console.log("Your balance is ", this.wallet)
  }

}


let rockt = new RocketTravel()

rockt.loadBTC(100);

rockt.trip("falcon9", "Abuja", "Moon")
rockt.trip("falcon1", "Moon", "Spock")
rockt.trip("falcon9", "Spock", "International Space Station")